function [BWCh,y,output]=MMT_z(Channel1,boxes_z)
%Function to perform local multi-thresholding using different boxe sizes
%Input:
%               - Channel1: input image
%               - boxes_z: box sized to be used along the z dimension. If
%               provided empty, the the possible box sizes are calculated
%               depending on the size of the image along z dimension
%Output
%               - BWCh: image containing the segmentation mask
%               - y: image with the candidate segmentations
%               - output: 4D image with the last dimension containing the 
%               result for each box size along z that was used 
%
%Author: Xenia Gkontra

%Boxes along z-dimension
if isempty(boxes_z)
    p=floor(log(size(Channel1,3))/log(2));
    for i=4:p
        boxes_z=[boxes_z 2^i];
    end
    if (2^p)<size(Channel1,3)
        boxes_z=[boxes_z size(Channel1,3)];
    end
end


y=zeros(size(Channel1,1),size(Channel1,2),size(Channel1,3));
for i=1:length(boxes_z)
    output(:,:,:,i)=multiLocalThresholding(Channel1,boxes_z(i));
    y=y+output(:,:,:,i);
end

y=y/(length(boxes_z));
BWCh=y>0.5;
