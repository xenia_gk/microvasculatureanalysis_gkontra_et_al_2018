function [BWCh3,BWCh2]=SegmentationSMA(mainDir, filename, folderRaw, raw)
%Function for loading segmenting information from aSMA channel
%Information and saving results
%
%Input:             - mainDir : input directory
%                   - filename: name of image under processing
%                   - folderRaw: directory for saving results
%                   - raw: whether to save images to .mha format
%
%Output:            - BWCh3: Segmentation of SMA coverage
%                   - BWCh2: Segmentation of the microvasculature
%                   - All results are saved in folderRaw
%
%Hint! Please note that you can change the intensity levels used in the 
%segmentation with MMT (multiLocalThresholding.m, line 85 e.g take only the 
%highest instead of the two highest as done here) or even change the
%permitted boxes sizes (call to MMT_z.m and first lines of 
%MMT_z.m and multiLocalThresholding.m)
%
%
%Author: Xenia Gkontra, CNIC, 2015

%--------------------------------------------------------------------------
%           PART 1: Load data information for SMA (Channel3)
%                       & Segment
%-------------------------------------------------------------------------

%Load Denoised volume
load([mainDir,filename,'\extras\NL_',filename])
%Segmentation using MMT
[BWCh3]=MMT_z(Channel3,10);

%--------------------------------------------------------------------------
%                       PART 2: Save Results
%--------------------------------------------------------------------------
%Load information for the vasculature
load([mainDir,filename,'\extras\SegmMMT_',filename,'.mat']);
%Save images in raw and save .mat
%SCIMAT = scimat_im2scimat(double(BWCh3+2*BWCh2));
%scirunnrrd = scimat_save([folderRaw,'\BWSMA_',filename,'.mha'],SCIMAT);
save([mainDir,filename,'\extras\SegmSMA_',filename,'.mat'],'BWCh3');
if raw==1
    SCIMAT = scimat_im2scimat(double(BWCh3));
    scirunnrrd = scimat_save([folderRaw,'\BWSMA_',filename,'.mha'],SCIMAT);
end