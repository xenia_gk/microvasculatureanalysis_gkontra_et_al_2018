%--------------------------------------------------------------------------
%Main function for segmentating the SMA+ cells channel and
%extacting possible arterioles/venules
%
%Input:
%               - .mat file with denoised channels (particularly Channel 3
%               corresponding to SMA channel is needed(NL_imageName.mat)
%               - .mat file with the segmentation of the VE-Cadherin
%               (SegmMMT_imageName.mat)
%
%Output:
%               - .mat files with and image that has denoted the regions of
%               possible arterioles/venules (regions where voxel value is
%               greater than 0) to be saved in Results/Arterioles/
%               (Arterioles_imageName.mat)
%               - .mat files with the segmented SMA channel saved in
%               ../Denoised/Results (SegmSMA_imageName.mat)
%
%Hint! Please note that you can change the intensity levels used in the 
%segmentation with MMT (multiLocalThresholding.m, line 85 e.g take only the 
%highest instead of the two highest as done here) or even change the
%permitted boxes sizes (call to MMT_z.m and first lines of 
%MMT_z.m and multiLocalThresholding.m)
%
%Author: Xenia Gkontra, CNIC
%--------------------------------------------------------------------------

close all;
clear;
clc;


%--------------------------------------------------------------------------
%                       Directories
%--------------------------------------------------------------------------
%Call function that saves the directories to the struct "directories"
directories=set_directories(GetFullPath('..'));

%Input directory where the denoised 3d image channels and the segmentation
%of the VE-Cadherin channel can be found
mainDir=directories.mainDir;

%Load time-points after MI to be stadied. Please note first two folders are
%'.' and '..' so we ignore them
daysMI_All=dir(mainDir);

%Outputfolder for possible arterioles/venules position
folderOut='Arterioles/';
%Check if folder exist
if ~exist(folderOut)
    mkdir(folderOut);
end


%--------------------------------------------------------------------------
%                       Parameters
%--------------------------------------------------------------------------
%Indicator to save (raw set to 0) or not (raw set to 1) .mha images
raw=0;

%--------------------------------------------------------------------------
%           SMA segmentation & Arterioles extraction
%--------------------------------------------------------------------------

for j=3:length(daysMI_All)
    
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    %Input directory
    directoryIn=[mainDir,daysMI,'/'];
    %Find images in the directory
    filenames=dir(directoryIn);
    %Output directory
    dirOut=[folderOut,daysMI,'/'];
    
    %Process the images sequentially
    for i=3:length(filenames)
        %Tissue condition
        tissue_cat=define_tissueCat(filenames(i).name);
        %Voxel Size
        [voxelSize,Voxel_volume]=define_voxelSize(filenames(i).name);
        %Output directory
        folderRaw=[dirOut,tissue_cat,'/',filenames(i).name];
        %Check whether the folder exist
        if ~exist(folderRaw)
            mkdir(folderRaw)
        end
        %Segment SMA+ channel and load VE-Cadherin segmentation refined by Nuclei
        if ~exist([directoryIn, filenames(i).name,'/extras/SegmSMA_',filenames(i).name,'.mat'])
            [BWCh3,BWCh2]=SegmentationSMA(directoryIn, filenames(i).name, folderRaw, raw);
        else
            load([directoryIn, filenames(i).name,'/extras/SegmMMT_',filenames(i).name,'.mat']);
            load([directoryIn, filenames(i).name,'/extras/SegmSMA_',filenames(i).name,'.mat']);
        end
        %Apply frangi filter on the segmentation of the SMA channel
        options.BlackWhite=false;
        %In voxels 
        options.FrangiScaleRange=[7.3 18.95]/min(voxelSize);
        [Vfiltered,whatscale,Voutx,Vouty,Voutz]=FrangiFilter3D(BWCh3,options);
              
        %Save .mha image
        if raw==1
            SCIMAT = scimat_im2scimat(BWCh2.*(Vfiltered>0)+BWCh2);
            scirunnrrd = scimat_save([folderRaw,'/Guidance_',filenames(i).name,'.mha'],SCIMAT);
        end
        save([folderRaw,'/Arterioles_',filenames(i).name,'.mat'],'Vfiltered','whatscale','Voutx','Vouty','Voutz');
    end
end

