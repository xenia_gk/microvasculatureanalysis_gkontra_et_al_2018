function [] = plot_properties_stacked(folderOut,prop_infarct,prop_remote,ylabelPlot,p,labels,prop_infarct2,prop_remote2,prop_infarct3,prop_remote3,prop_basal)
%Function to create stacked bar histogram comparing 7 different tissue
%condition for each of which there exist 3 values (please see fig 5a of
%Gkontra et al., 2018)
%
%Input:         - folderOut: folder where the plots should be saved during
%               the call prop_properties function
%               - prop_infarct & prop_remote: matrices of size
%               (number of samples)x3 1st (imarcted 1 days post MI) &
%               4th (remote 1 day post MI) tissue condition under investigation.
%               Every row corresponds to a volume and column 1-3 to the
%               three different values for the plot
%               - ylabelPlot:  y axis label to be used for the plot
%               - p: p-values. By setting
%                   category(1)=Infarct1MI, category(2)=Infarct3MI, category(3)=Infarct7MI,
%                   category(4)=Remote1MI, category(5)=Remote3MI, category(6)=Remote7MI, category(7)=Basal
%                   The correspondance between each row and the pair under
%                   comparison to which the pvalue refers is as follows
%                   p(2): [categories(1),categories(4)]: Infarct1MI vs Remote1MI
%                   p(3): [categories(2),categories(5)]: Infarct3MI vs Remote3MI
%                   p(4): [categories(3),categories(6)]: Infarct7MI vs Remote7MI
%                   p(5): [categories(1),categories(2)]: Infarct1MI vs Infarct3MI
%                   p(6): [categories(4),categories(5)]: Remote1MI vs Remote3MI
%                   p(7): [categories(1),categories(3)]: Infarct1MI vs Infarct7MI
%                   p(8): [categories(4),categories(6)]: Remote1MI vs Remote7MI
%                   p(9): [categories(2),categories(3)]: Infarct3MI vs Infarct7MI
%                   p(10): [categories(5),categories(6)]: Remote3MI vs Remote7MI
%                   p(11): [categories(1),categories(7)]: Infarct1MI vs Basal
%                   p(12): [categories(4),categories(7)]: Remote1MI vs Basal
%                   p(13): [categories(2),categories(7)]: Infarct3MI vs Basal
%                   p(14): [categories(5),categories(7)]: Remote3MI vs Basal
%                   p(15): [categories(3),categories(7)]: Infract7MI vs Basal
%                   p(16): [categories(6),categories(7)]: Remote7MI vs Basal
%               - labels: Categories of different tissue under comparison.
%               The labels will be used for naming the x-axis points
%               - prop_infarct2 & prop_remote2: matrices of size
%               (number of samples)x3 for the 2st (infacted 3 days post MI) &
%               5th (remote 3 days post MI) tissue condition under investigation.
%               Every row corresponds to a volume and  column 1-3 to the
%               three different categories of the metric for which the
%               stacked plot is going to be created
%               - prop_infarct3 & prop_remote3: matrices of size
%               (number of samples)x3 for the 3st (infacted 7 days post MI) &
%               6th (remote 7 days post MI) tissue condition under investigation.
%               Every row corresponds to a volume and column 1-3 to the
%               three different categories of the metric for which the
%               stacked plot is going to be created
%               - prop_basal: matrices of size
%               (number of samples)x3 for the 2th (basal) tissue condition
%               under investigation.Every row corresponds to a volume and column 1-3 to the
%               three different categories of the metric for which the
%               stacked plot is going to be created
%
%Output:
%               - The function does not return anything but during the call
%               the created stacked bar plot VesselPercentageRadius is 
%               saved in .png, .fig and .eps format in folder folderOut
%
% Uses sigstar
%
%Author:Xenia Gkontra


if length(labels)==7
    %----------------------------------------------------------------------
    %           Create bar plot
    %----------------------------------------------------------------------
    %Handle of figure
    figure(9);
    %Colors for the bar plots
    col(1,1:3)=[43 29 12]/255;
    col(2,1:3)=[213 94 0]/255;
    col(3,1:3)=[240 228 66]/255;
    col(4,1:3)=col(1,1:3);
    col(5,1:3)=col(2,1:3);
    col(6,1:3)=col(3,1:3);
    col(7,1:3)=[0.5 0.5 0.5];
    
    %Mean values
    properties=[mean(prop_infarct(:,1)) mean(prop_infarct2(:,1)) mean(prop_infarct3(:,1)) mean(prop_remote(:,1)) mean(prop_remote2(:,1)) mean(prop_remote3(:,1)) mean(prop_basal(:,1));
        mean(prop_infarct(:,2)) mean(prop_infarct2(:,2)) mean(prop_infarct3(:,2)) mean(prop_remote(:,2)) mean(prop_remote2(:,2)) mean(prop_remote3(:,2)) mean(prop_basal(:,2));
        mean(prop_infarct(:,3)) mean(prop_infarct2(:,3)) mean(prop_infarct3(:,3)) mean(prop_remote(:,3)) mean(prop_remote2(:,3)) mean(prop_remote3(:,3)) mean(prop_basal(:,3))]';
    categories=[1,4,7,13,16,19,25];  % 1st category=Infarct1MI, 2nd category=Infarct3MI, 3rd category=Infarct7MI, 4rth category=Remote1MI, 5th category=Remote3MI, 6th category=Remote7MI
    %Create bar plot
    H=bar(categories,properties,0.5,'stacked');
    for k=1:3
        set(H(k),'facecolor',col(k,:))
    end
    %----------------------------------------------------------------------
    %           Add stars of significance to the bar plots
    %----------------------------------------------------------------------
    groups={[categories(1),categories(4)],[categories(2),categories(5)],[categories(3),categories(6)],[categories(1),categories(2)],[categories(4),categories(5)],[categories(1),categories(3)],[categories(4),categories(6)],[categories(2),categories(3)],[categories(5),categories(6)],[categories(1),categories(7)],[categories(4),categories(7)],[categories(2),categories(7)],[categories(5),categories(7)],[categories(3),categories(7)],[categories(6),categories(7)]};
    group_or={[(1),(4)],[(2),(5)],[(3),(6)],[(1),(2)],[(4),(5)],[(1),(3)],[(4),(6)],[(2),(3)],[(5),(6)],[(1),(7)],[(4),(7)],[(2),(7)],[(5),(7)],[(3),(7)],[(6),(7)]};
    idx=find(p(2:end)<=0.05);
    group2=groups(idx);
    categories2=group_or(idx);
    p2=p(1+idx);
    %sigstar(group2,p2);
    
    if p2<=1E-3
        stars='***';
    elseif p2<=1E-2
        stars='**';
    elseif p2<=0.05
        stars='*';
        %elseif (p<=0.1)&&(p>0.05)
        %	stars='*';
    elseif isnan(p2)
        stars='n.s.';
    else
        stars='';
    end
    yl=ylim;
    yd=yl(2)*0.015; %Ticks are 1% of the y axis range
    for ii=1:length(group2)
        yd=yd+yd;
        y(1)=max(sum(properties(categories2{ii}(1),:)),sum(properties(categories2{ii}(2),:)))+yd;
        y(2)=y(1);
        x=group2{ii};
        % x=
        hold on
        plot(x,y,'-k','LineWidth',2);
        plot([x(1) x(1)],[y(1) y(1)-y(1)*0.03],'-k','LineWidth',2);
        plot([x(2) x(2)],[y(1) y(2)-y(2)*0.03],'-k','LineWidth',2);
        %Increase offset between line and text if we will print "n.s."
        %instead of a star.
        if ~isnan(p)
            offset=0.005;
        else
            offset=0.02;
        end
        
        text(mean(x(:)),mean(y)+yl(2)*offset,stars,...
            'HorizontalAlignment','Center',...
            'BackGroundColor','none','FontSize',32,'FontWeight','bold');
        hold off
    end
    
    set(gcf, 'Position', [1   1   1024 768]); % Maximize figure.
    %----------------------------------------------------------------------
    %       Formulate the plot so as to be adequate for publication
    %----------------------------------------------------------------------
    %Tips from http://blogs.mathworks.com/loren/2007/12/11/making-pretty-graphs/
    
    yl = ylim;
    xl=xlim;
    ylim([yl(1) yl(2)+0.4*yl(2)])
    
    set(gca,'XTick',[categories]);
    set(gca,'XTickLabel',{'I1','I3','I7','R1','R3','R7','Basal'},'FontSize',30,'FontName', 'Helvetica')
    
    legend({'Vessels of diameter \leq 6.9 \mum','Vessels of diameter between 6.9 and 8.2 \mum','Vessels of diameter > 8.2 \mum'},'Location','NorthEast','Fontsize',20);
    ylabel(ylabelPlot,'FontName', 'Helvetica','FontSize',30)
    
    set(gca, ...
        'Box'         , 'off'     , ...
        'TickDir'     , 'out'     , ...
        'TickLength'  , [.02 .02] , ...
        'XMinorTick'  , 'on'      , ...
        'YMinorTick'  , 'on'      , ...
        'YGrid'       , 'on'      , ...
        'XColor'      , [.3 .3 .3], ...
        'YColor'      , [.3 .3 .3], ...
        'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
        'LineWidth'   , 1 );
    %----------------------------------------------------------------------
    %          Save plot
    %----------------------------------------------------------------------
    %Save the figure
    saveas(gca,[folderOut,'VesselPercentageRadius.png'])
    saveas(gca,[folderOut,'VesselPercentageRadius.fig'])
    print(fullfile([folderOut,'VesselPercentageRadius.eps']), '-depsc');
else
    error('Tissue conditions under comparison are expected to be 7')
end

