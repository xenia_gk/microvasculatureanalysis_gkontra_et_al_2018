function [titlePlot]=create_titlePlot_m2()
%Function to define titles for the plots of the 


titlePlot{1}=' '; %1
titlePlot{size(titlePlot,2)+1}=' '; %2
titlePlot{size(titlePlot,2)+1}=' '; %3
titlePlot{size(titlePlot,2)+1}=' '; %4
titlePlot{size(titlePlot,2)+1}=' '; %5
%Max
titlePlot{size(titlePlot,2)+1}=' '; %6
titlePlot{size(titlePlot,2)+1}=' '; %7
titlePlot{size(titlePlot,2)+1}=' '; %8
titlePlot{size(titlePlot,2)+1}=' '; %9
titlePlot{size(titlePlot,2)+1}=' '; %10
titlePlot{size(titlePlot,2)+1}=' '; %11
titlePlot{size(titlePlot,2)+1}=' '; %12
titlePlot{size(titlePlot,2)+1}=' '; %13
titlePlot{size(titlePlot,2)+1}=' '; %14
titlePlot{size(titlePlot,2)+1}=' '; %15
titlePlot{size(titlePlot,2)+1}=' '; %16
titlePlot{size(titlePlot,2)+1}=' '; %17
titlePlot{size(titlePlot,2)+1}=' '; %18
titlePlot{size(titlePlot,2)+1}=' '; %19
titlePlot{size(titlePlot,2)+1}=' '; %20
titlePlot{size(titlePlot,2)+1}=' '; %21
titlePlot{size(titlePlot,2)+1}=' '; %22
titlePlot{size(titlePlot,2)+1}=' '; %23
titlePlot{size(titlePlot,2)+1}=' '; %24
titlePlot{size(titlePlot,2)+1}=' '; %25
titlePlot{size(titlePlot,2)+1}='Capillaries of diameter \leq 8.2 \mum'; %26
titlePlot{size(titlePlot,2)+1}='Vessels of diameter > 8.2 \mum'; %27
titlePlot{size(titlePlot,2)+1}='Capillaries of diameter \leq 8.2 \mum'; %28
titlePlot{size(titlePlot,2)+1}='Vessels of diameter > 8.2 \mum'; %29
titlePlot{size(titlePlot,2)+1}=' '; %30
titlePlot{size(titlePlot,2)+1}=' '; %31
titlePlot{size(titlePlot,2)+1}=' '; %32
%Vessels covered with sma
titlePlot{size(titlePlot,2)+1}='VCS'; %33
titlePlot{size(titlePlot,2)+1}='VCS'; %34
titlePlot{size(titlePlot,2)+1}='VCS'; %35
titlePlot{size(titlePlot,2)+1}='VCS'; %36
titlePlot{size(titlePlot,2)+1}='VCS'; %37
titlePlot{size(titlePlot,2)+1}='VCS'; %38
titlePlot{size(titlePlot,2)+1}='VCS'; %39
titlePlot{size(titlePlot,2)+1}='VCS'; %40
titlePlot{size(titlePlot,2)+1}='VCS'; %41
titlePlot{size(titlePlot,2)+1}='VCS'; %42
titlePlot{size(titlePlot,2)+1}='VCS - Mean radius'; %43
%Max
titlePlot{size(titlePlot,2)+1}='VCS'; %44
titlePlot{size(titlePlot,2)+1}='VCS'; %45
%Vessels not covered with SMA
titlePlot{size(titlePlot,2)+1}='VNCS'; %46
titlePlot{size(titlePlot,2)+1}='VNCS '; %47
titlePlot{size(titlePlot,2)+1}='VNCS'; %48
titlePlot{size(titlePlot,2)+1}='VNCS'; %49
titlePlot{size(titlePlot,2)+1}='VNCS'; %50
titlePlot{size(titlePlot,2)+1}='VNCS'; %51
titlePlot{size(titlePlot,2)+1}='VNCS'; %52
titlePlot{size(titlePlot,2)+1}='VNCS'; %53
titlePlot{size(titlePlot,2)+1}='VNCS';% 54
titlePlot{size(titlePlot,2)+1}='VNCS'; %55
titlePlot{size(titlePlot,2)+1}='VNCS';%56
%Max
titlePlot{size(titlePlot,2)+1}='VNCS'; %57
titlePlot{size(titlePlot,2)+1}='VNCS'; %58
%Vessels small, medium, large
titlePlot{size(titlePlot,2)+1}=' '; %59
titlePlot{size(titlePlot,2)+1}=' '; %60
titlePlot{size(titlePlot,2)+1}=' '; %61
titlePlot{size(titlePlot,2)+1}=' '; %62
titlePlot{size(titlePlot,2)+1}=' '; %63
titlePlot{size(titlePlot,2)+1}=' '; %64
titlePlot{size(titlePlot,2)+1}=' '; %65
titlePlot{size(titlePlot,2)+1}=' '; %66
titlePlot{size(titlePlot,2)+1}=' '; %67
%Cell numbers 
titlePlot{size(titlePlot,2)+1}=' '; %68
titlePlot{size(titlePlot,2)+1}=' '; %69
titlePlot{size(titlePlot,2)+1}=' '; %70
%SMA Thickness
titlePlot{size(titlePlot,2)+1}='Vessels of diameter \leq 6.9 \mum'; %71
titlePlot{size(titlePlot,2)+1}='Vessels of diameter between 6.9 and 8.2 \mum'; %72
titlePlot{size(titlePlot,2)+1}='Vessels of diameter > 8.2 \mum'; %73

%--------------------------------------------------------------------------
%               Metrics per vascular length
%--------------------------------------------------------------------------
titlePlot{size(titlePlot,2)+1}=' '; %74
titlePlot{size(titlePlot,2)+1}=' '; %75
titlePlot{size(titlePlot,2)+1}=' '; %76
titlePlot{size(titlePlot,2)+1}=' '; %77
titlePlot{size(titlePlot,2)+1}=' '; %78
titlePlot{size(titlePlot,2)+1}=' '; %79
%--------------------------------------------------------------------------
%               Metrics per vascular volume
%--------------------------------------------------------------------------
titlePlot{size(titlePlot,2)+1}=' '; %80
titlePlot{size(titlePlot,2)+1}=' '; %81
titlePlot{size(titlePlot,2)+1}=' '; %82
titlePlot{size(titlePlot,2)+1}=' '; %83
titlePlot{size(titlePlot,2)+1}=' '; %84
titlePlot{size(titlePlot,2)+1}=' ' ; %85
   %----------------------------------------------------------------------
    %          SDF
    %----------------------------------------------------------------------
titlePlot{size(titlePlot,2)+1}=' '; %86
titlePlot{size(titlePlot,2)+1}=' '; %87
titlePlot{size(titlePlot,2)+1}=' '; %88
titlePlot{size(titlePlot,2)+1}=' ' ; %89