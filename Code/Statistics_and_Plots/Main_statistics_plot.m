function []=Main_statistics_plot(type_metrics)
%--------------------------------------------------------------------------
%Calculate statistics and plot metrics as in Gkontra et al., 2018
%
%Input:
%               - type_metrics: Define whether you would like to plot
%               metrics studying changes at stucture level, or all the rest
%               of measures (graph-based, sma, oxygen) by using
%               variable type_metrics=1 for the former or
%               type_metrics=2 for the latter
%
%Output:
%               - Comparative plots for every metric with p-values
%               annotated are saved in .eps, .png and .fig format (in
%               folder folderOut, i.e. Results or Results_structure
%               depending on which class of metrics was chosen)
%               - pvalue.mat is saved in folder folderOut. This MAT-
%               file   contains the pvalues for all comparisons between the
%               different tissue categories performed before and after the
%               application of multiple comparisons correction
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@cnic.es
%--------------------------------------------------------------------------

%--------------------------------------------------------------------------
%                       Directories & Initialize
%--------------------------------------------------------------------------
%Get full Directory
curDir   = pwd;
%Call function to load the directories
directories=set_directories(GetFullPath('..'));
if type_metrics==1
    mainDir_Metrics=directories.mainDir_Structure_Results;
    folderOut=[curDir,'/Results_structure/'];
else
    mainDir_Metrics=directories.mainDir_Segment_Results;
    folderOut=[curDir,'/Results/'];
end

%Check if folder exist
if ~exist(folderOut)
    mkdir(folderOut);
end

%--------------------------------------------------------------------------
%             Load calculated metrics
%--------------------------------------------------------------------------
load([mainDir_Metrics,'/Res3.mat'])

%--------------------------------------------------------------------------
%              Calculate  Statistical significance
%--------------------------------------------------------------------------
%Wilcoxon rank-sum

%Initialize
pvalue=zeros(16,9);
hyp=zeros(16,9);

%Row 1: Pvalues for statistical significant difference between remote and
%infract (independently of the time-point)
for i=1:size(prop_infarct,2)
    [pvalue(1,i),hyp(1,i)]=ranksum(prop_infarct(:,i),prop_remote(:,i));
end

%Remote vs Infarct at each time-point (rows 2,3,4)
%--------------------------------------------------
%Row 2: Pvalues for statistical significant difference between
%Infract 1MI and Remote 1MI
for i=1:size(prop_infarct1MI,2)
    [pvalue(2,i),hyp(2,i)]=ranksum(prop_infarct1MI(:,i),prop_remote1MI(:,i));
end

%Row 3: Pvalues for statistical significant difference between
%Infract 3MI and Remote 3MI
for i=1:size(prop_infarct3MI,2)
    [pvalue(3,i),hyp(3,i)]=ranksum(prop_infarct3MI(:,i),prop_remote3MI(:,i));
end

%Row 4: Pvalues for statistical significant difference between
%Infract 7MI and Remote 7MI
for i=1:size(prop_infarct7MI,2)
    [pvalue(4,i),hyp(4,i)]=ranksum(prop_infarct7MI(:,i),prop_remote7MI(:,i));
end

%Infarcted areas over time (rows 5,7,9) & Remote over time (6,8,10)
%-----------------------------------------------------------------
%Row 5: Pvalues for statistical significant difference between
%Infract 1MI and Infarct 3MI
for i=1:size(prop_infarct1MI,2)
    [pvalue(5,i),hyp(5,i)]=ranksum(prop_infarct1MI(:,i),prop_infarct3MI(:,i));
end

%Row 6: Pvalues for statistical significant difference between
%Remote 1MI and Remote 3MI
for i=1:size(prop_remote1MI,2)
    [pvalue(6,i),hyp(6,i)]=ranksum(prop_remote1MI(:,i),prop_remote3MI(:,i));
end

%Row 7: Pvalues for statistical significant difference between
%Infarct 1MI and Infarct 7MI
for i=1:size(prop_infarct1MI,2)
    [pvalue(7,i),hyp(7,i)]=ranksum(prop_infarct1MI(:,i),prop_infarct7MI(:,i));
end

%Row 8: Pvalues for statistical significant difference between
%Remote 1MI and Remote 7MI
for i=1:size(prop_remote1MI,2)
    [pvalue(8,i),hyp(8,i)]=ranksum(prop_remote1MI(:,i),prop_remote7MI(:,i));
end

%Row 9: Pvalues for statistical significant difference between
%Infarct 3MI and Infarct 7MI
for i=1:size(prop_infarct3MI,2)
    [pvalue(9,i),hyp(9,i)]=ranksum(prop_infarct3MI(:,i),prop_infarct7MI(:,i));
end

%Row 10: Pvalues for statistical significant difference between
%Remote 3MI and Remote 7MI
for i=1:size(prop_remote3MI,2)
    [pvalue(10,i),hyp(10,i)]=ranksum(prop_remote3MI(:,i),prop_remote7MI(:,i));
end

%Infarcted vs Basal (rows 11,12,13) & Remote vs Basal (rows 14, 15, 16)
%----------------------------------------------------------------------
%Row 11: Pvalues for statistical significant difference between
%Infarct 1MI and Basal
for i=1:size(prop_infarct1MI,2)
    [pvalue(11,i),hyp(11,i)]=ranksum(prop_infarct1MI(:,i),prop_basal(:,i));
end

%Row 12: Pvalues for statistical significant difference between
%Remote 1MI and Basal
for i=1:size(prop_remote1MI,2)
    [pvalue(12,i),hyp(12,i)]=ranksum(prop_remote1MI(:,i),prop_basal(:,i));
end

%Row 13: Pvalues for statistical significant difference between
%Infarct 3MI and Basal
for i=1:size(prop_infarct3MI,2)
    [pvalue(13,i),hyp(13,i)]=ranksum(prop_infarct3MI(:,i),prop_basal(:,i));
end

%Row 14: Pvalues for statistical significant difference between
%Remote 3MI and Basal
for i=1:size(prop_remote3MI,2)
    [pvalue(14,i),hyp(14,i)]=ranksum(prop_remote3MI(:,i),prop_basal(:,i));
end

%Row 15: Pvalues for statistical significant difference between
%Infarc 7MI and Basal
for i=1:size(prop_infarct7MI,2)
    [pvalue(15,i),hyp(15,i)]=ranksum(prop_infarct7MI(:,i),prop_basal(:,i));
end

%Row 15: Pvalues for statistical significant difference between
%Remote 7MI and Basal
for i=1:size(prop_remote7MI,2)
    [pvalue(16,i),hyp(16,i)]=ranksum(prop_remote7MI(:,i),prop_basal(:,i));
end

adjusted_pvalues(1,:)=pvalue(1,:);
%Correction for multiple comparisons
numComp=size(pvalue,1);
for i=1:size(pvalue,2)
    adjusted_pvalues(2:numComp,i)= mafdr(pvalue(2:end,i),'BHFDR', true) ;
end

%--------------------------------------------------------------------------
%                  Plotting
%--------------------------------------------------------------------------
if type_metrics==1
    %Create titles for the figures
    [titlePlot]=create_titlePlot_m1();
    %Labels for y-axis
    [ylabelPlot]=create_ylabelPlot_m1();
    %Create plots
    plotting2(folderOut,prop_infarct1MI,prop_remote1MI,adjusted_pvalues,ylabelPlot,ylabelPlot,titlePlot,{'I1MI','I3MI','I7MI','R1MI','R3MI','R7MI','Basal'},prop_infarct3MI,prop_remote3MI,prop_infarct7MI,prop_remote7MI,prop_basal)
    %Save pvalues to the directory where the metrics were saved
    save([mainDir_Metrics,'/pvalue.mat'],'pvalue','adjusted_pvalues')
else
    %Create titles for the figures
    [titlePlot]=create_titlePlot_m2();
    %Labels for y-axis
    [ylabelPlot]=create_ylabelPlot_m2();
    %Filename for saving the plots
    [savePlot]=create_savePlot_m2();
    %Create plots
    plotting2(folderOut,prop_infarct1MI,prop_remote1MI,adjusted_pvalues,ylabelPlot,savePlot,titlePlot,{'I1MI','I3MI','I7MI','R1MI','R3MI','R7MI','Basal'},prop_infarct3MI,prop_remote3MI,prop_infarct7MI,prop_remote7MI,prop_basal)
    %Create stacked diagram
    plot_properties_stacked(folderOut,prop_infarct1MI(:,65:67),prop_remote1MI(:,65:67),{'Vascular segments',;'per mm^3 of tissue'},adjusted_pvalues(:,2),{'I1MI','I3MI','I7MI','R1MI','R3MI','R7MI','Basal'},prop_infarct3MI(:,65:67),prop_remote3MI(:,65:67),prop_infarct7MI(:,65:67),prop_remote7MI(:,65:67),prop_basal(:,65:67))
    %Save pvalues to the directory where the metrics were saved
    save([mainDir_Metrics,'/pvalue.mat'],'pvalue','adjusted_pvalues')
    
end
