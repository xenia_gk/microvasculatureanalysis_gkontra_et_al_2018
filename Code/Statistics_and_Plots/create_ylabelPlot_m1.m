function [titlePlot]=create_ylabelPlot_m1()
%Function that contains the names for the fractal- & minkowksi based
%metrics that were calculated earlier. 
%
%Output: 
%                   -titlePlot: cell array with the name of the metrics
%                   that have been calculated. Cell i corresponds to column
%                   i of matrices(e.g. prop_remote,prp_infarct,prop_basal)
%                   were the metrics are saved
%
%Author: Xenia Gkontra

titlePlot{1}='Fractal Dimension';
titlePlot{2}='Succolarity';
titlePlot{3}='Succolarity (Max)';
titlePlot{4}='Succolarity (Median)';
titlePlot{5}='Lacunarity';
titlePlot{6}='Vascular Volume Density (%)';
titlePlot{7}='Surface Area';
titlePlot{8}='Surface Area Density (\mum^2/\mum^3)';
titlePlot{9}='Breadth Density (\mum/\mum^3)';
titlePlot{10}='Euler characteristic Density';
titlePlot{11}='Fractal Dimension with cut-offs';
titlePlot{12}='Cut Indicator';
titlePlot{13}='Cut -off 1';
titlePlot{14}='Cut -off 2';
titlePlot{15}={'Capillary Volume Density';' (%)'};
titlePlot{16}='Capillary Surface Area';
titlePlot{17}={'Capillary Surface Area Density';'(\mum^2/\mum^3)'};
titlePlot{18}='Capillary Breadth';
titlePlot{19}='Capillary Euler';
