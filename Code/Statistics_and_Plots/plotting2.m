function [] = plotting2(folderOut,prop_infarct,prop_remote,pvalue,ylabelPlot,saveName,titlePlot,labels,prop_infarct2,prop_remote2,prop_infarct3,prop_remote3,prop_basal)
%Function to call plot_properties_v2 in order to create comparative bar plot
%with significance indications for each parameter in the matrices prop_infarct,
%prop_infarct1, prop_infarct2 prop_remote, prop_remote1, prop_remote3,
%prop_basal
%
%Input:         - folderOut: folder where the plots should be saved during
%               the call prop_properties function 
%               - prop_infarct & prop_remote: matrices of size 
%               (number of samples)x(number of parameters). They contain
%               the the parameters calculated for every volume at infarcted
%               and remote areas corresponding at 1 day post MI (1st & 
%               4th tissue condition under investigation). Every row 
%               corresponds to a volume and every column to a parameter
%               - pvalue: p-values
%               - ylabelPlot: cell array that contains in every cell the y
%               axis label to be used. Cell i corresponds to parameter in
%               column i of the matrices prop_infarct, prop_remote etc
%               - saveName: cell array that contains in every cell the
%               filename to be used for saving the plot
%               - titlePlot: cell array with titles for the plots. Cell i 
%               corresponds to parameter in column i of the matrices
%               prop_infarct, prop_remote etc
%               - labels: Categories of different tissue under comparison.
%               The labels will be used for naming the x-axis points 
%               - prop_infarct2 & prop_remote2: matrices of size 
%               (number of samples)x(number of parameters). They contain
%               the the parameters calculated for every volume at infarcted
%               and remote areas correspondingly at 3 days post MI (2nd & 
%               5th tissue condition under investigation). Every row 
%               corresponds to a volume and every column to a parameter
%               - prop_infarct3 & prop_remote3: matrices of size 
%               (number of samples)x(number of parameters). They contain
%               the the parameters calculated for every volume at infarcted
%               and remote areas correspondingly at 7 days post MI (3rd & 
%               6th tissue condition under investigation). Every row 
%               corresponds to a volume and every column to a parameter
%               - prop_basal: matrix of size (number of volumes)x(number
%               of parameters) that contains the parameters calculated for
%               every volume at infarcted areas at basal conditions (7th
%               tissue condition under investigation). 
%
%Output:
%               - The function does not return anything but during the call
%               to plot_properties_v2 the created plots are saved
%
%Note! The function expect to compare 7 different tissue conditions: remote
%and infracted from 3 different time-points and basal. Pvalues are
%expected to refer to comparisons between infracted and remote, infarcted 
%among them, remote among them, infarcted vs basal and remote vs basal. For
%more details see plot_properties_v2.
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com

%In case no folder was provided save them to the default "Results"
if isempty(folderOut)
    folderOut='Results\';
end

for i=1:size(prop_infarct2,2)
     plot_properties(folderOut,prop_infarct(:,i),prop_remote(:,i),ylabelPlot{i},saveName{i},titlePlot{i},pvalue(:,i),labels,prop_infarct2(:,i),prop_remote2(:,i),prop_infarct3(:,i),prop_remote3(:,i),prop_basal(:,i));
end

end

