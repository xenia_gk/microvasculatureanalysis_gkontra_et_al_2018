function [] = plot_properties(folderOut,prop_infarct,prop_remote,ylabelPlot,namePlot,titlePlot,p,labels,prop_infarct2,prop_remote2,prop_infarct3,prop_remote3,prop_basal)
%Create bar plot comparing mean parameter values & sd between infarct, basal and remote
% zone using statistical significance as well.
%
%Input:         - folderOut: folder where the plots should be saved during
%               the call prop_properties function
%               - prop_infarct & prop_remote: matrices of size
%               (number of samples)x(number of parameters). They contain
%               the the parameters calculated for every volume at infarcted
%               and remote areas correspondingly at 1 day post MI (1st &
%               4th tissue condition under investigation). Every row
%               corresponds to a volume and every column to a parameter
%               - p: p-values. By setting
%                   category(1)=Infarct1MI, category(2)=Infarct3MI, category(3)=Infarct7MI,
%                   category(4)=Remote1MI, category(5)=Remote3MI, category(6)=Remote7MI, category(7)=Basal
%                   The correspondance between each row and the pair under
%                   comparison to which the pvalue refers is as follows
%                   p(2): [categories(1),categories(4)]: Infarct1MI vs Remote1MI
%                   p(3): [categories(2),categories(5)]: Infarct3MI vs Remote3MI
%                   p(4): [categories(3),categories(6)]: Infarct7MI vs Remote7MI
%                   p(5): [categories(1),categories(2)]: Infarct1MI vs Infarct3MI
%                   p(6): [categories(4),categories(5)]: Remote1MI vs Remote3MI
%                   p(7): [categories(1),categories(3)]: Infarct1MI vs Infarct7MI
%                   p(8): [categories(4),categories(6)]: Remote1MI vs Remote7MI
%                   p(9): [categories(2),categories(3)]: Infarct3MI vs Infarct7MI
%                   p(10): [categories(5),categories(6)]: Remote3MI vs Remote7MI
%                   p(11): [categories(1),categories(7)]: Infarct1MI vs Basal
%                   p(12): [categories(4),categories(7)]: Remote1MI vs Basal
%                   p(13): [categories(2),categories(7)]: Infarct3MI vs Basal
%                   p(14): [categories(5),categories(7)]: Remote3MI vs Basal
%                   p(15): [categories(3),categories(7)]: Infract7MI vs Basal
%                   p(16): [categories(6),categories(7)]: Remote7MI vs Basal
%               - ylabelPlot:  y axis label to be used for the plot
%               - saveName: name to be used for saving the plot
%               - titlePlot: title to be used for the plot
%               - labels: Categories of different tissue under comparison.
%               The labels will be used for naming the x-axis points
%               - prop_infarct2 & prop_remote2: matrices of size
%               (number of samples)x(number of parameters). They contain
%               the the parameters calculated for every volume at infarcted
%               and remote areas correspondingly at 3 days post MI (2nd &
%               5th tissue condition under investigation). Every row
%               corresponds to a volume and every column to a parameter
%               - prop_infarct3 & prop_remote3: matrices of size
%               (number of samples)x(number of parameters). They contain
%               the the parameters calculated for every volume at infarcted
%               and remote areas correspondingly at 7 days post MI (3rd &
%               6th tissue condition under investigation). Every row
%               corresponds to a volume and every column to a parameter
%               - prop_basal: matrix of size (number of volumes)x(number
%               of parameters) that contains the parameters calculated for
%               every volume at infarcted areas at basal conditions (7th
%               tissue condition under investigation).
%
%Output:
%               - The function does not return anything but during the call
%               to plot_properties_v2 the created plots are saved
%
% Uses sigstar
%
%Author:Xenia Gkontra


if length(labels)==7
    %----------------------------------------------------------------------
    %           Create bar plot
    %----------------------------------------------------------------------
    %Handle of figure
    figure(9);
    %Colors for the bar plots
    col(1,1:3)=[230 159 0]/255;
    col(2,1:3)=[86 180 233]/255;
    col(3,1:3)=[0 158 115]/255;
    col(4,1:3)=col(1,1:3);
    col(5,1:3)=col(2,1:3);
    col(6,1:3)=col(3,1:3);
    col(7,1:3)=[0.5 0.5 0.5];
    
    %Mean values
    properties=[mean(prop_infarct(:)) mean(prop_infarct2(:)) mean(prop_infarct3(:)) mean(prop_remote(:)) mean(prop_remote2(:)) mean(prop_remote3(:)) mean(prop_basal(:))];
    categories=[1,4,7,13,16,19,25]; % 1st category=Infarct1MI, 2nd category=Infarct3MI, 3rd category=Infarct7MI, 4rth category=Remote1MI, 5th category=Remote3MI, 6th category=Remote7MI
    
    %For every tissue category plot a bar
    for i=1:length(labels)
        H=bar(categories(i),properties(:,i),1.5);
        set(H,'FaceColor',col(i,1:3))
        hold on
    end
    %----------------------------------------------------------------------
    %           Add standard deviation to the bar plots
    %----------------------------------------------------------------------
    %Standard devieations
    sd=[std(prop_infarct),std(prop_infarct2),std(prop_infarct3),std(prop_remote),std(prop_remote2),std(prop_remote3),std(prop_basal)];
    %Standard deviation: errorbar(Y,E) plots Y and draws an error bar at each element of Y. The error bar is a distance of E(i) above and below the curve so that each bar is symmetric and 2*E(i) long.
    %Find negative means in order to create an errobar that is on the
    %reverse direction
    negValues=find([mean(prop_infarct),mean(prop_infarct2),mean(prop_infarct3),mean(prop_remote),mean(prop_remote2),mean(prop_remote3),mean(prop_basal)]<0);
    if any(negValues)
        stPos=sd;
        stPos(negValues)=0;
        stNeg=zeros(size(stPos));
        stNeg(negValues)=sd(negValues);
        errorbar([categories(1),categories(2),categories(3),categories(4),categories(5),categories(6),categories(7)],[mean(prop_infarct),mean(prop_infarct2),mean(prop_infarct3),mean(prop_remote),mean(prop_remote2),mean(prop_remote3),mean(prop_basal)],stNeg,stPos,'x','Color','black','LineWidth',2)
    else
        errorbar([categories(1),categories(2),categories(3),categories(4),categories(5),categories(6),categories(7)],[mean(prop_infarct),mean(prop_infarct2),mean(prop_infarct3),mean(prop_remote),mean(prop_remote2),mean(prop_remote3),mean(prop_basal)],zeros(length(sd),1),sd,'x','Color','black','LineWidth',2)
    end
    hold off
    %----------------------------------------------------------------------
    %               Add significance
    %----------------------------------------------------------------------
    ymax=max([mean(prop_infarct)+sd(1),mean(prop_remote)+sd(2),mean(prop_infarct2)+sd(3),mean(prop_remote2)+sd(4),mean(prop_infarct3)+sd(5),mean(prop_remote3)+sd(6),mean(prop_basal)+sd(7)]);
    %Comparison pairs for plotting a straight line between the two categories
    %of each pair
    groups={[categories(1),categories(4)],[categories(2),categories(5)],[categories(3),categories(6)],[categories(1),categories(2)],[categories(4),categories(5)],[categories(1),categories(3)],[categories(4),categories(6)],[categories(2),categories(3)],[categories(5),categories(6)],[categories(1),categories(7)],[categories(4),categories(7)],[categories(2),categories(7)],[categories(5),categories(7)],[categories(3),categories(7)],[categories(6),categories(7)]};
    idx=find(p(2:end)<=0.05);
    group2=groups(idx);
    p2=p(1+idx);
    sigstar(group2,p2);
    
    %----------------------------------------------------------------------
    %       Formulate the plot so as to be adequate for publication
    %----------------------------------------------------------------------
    %Tips from http://blogs.mathworks.com/loren/2007/12/11/making-pretty-graphs/
    
    set(gca,'XTick',[categories(2) categories(5) categories(7)]);
    set(gca,'XTickLabel',{'Infarcted','Remote','Basal'},'FontSize',26,'FontName', 'Helvetica')
    %Limits along y-axis
    yl = ylim;
    %{
    if yl(2)>1
        yl=ceil(yl);
    end
    %}
    ylim([yl(1) yl(2)+0.25*yl(2)]);
    
    ylabel(ylabelPlot,'FontName', 'Helvetica','FontSize',26);
    htitle=title(titlePlot,'FontSize',26,'FontName', 'Helvetica');
    set(gca, ...
        'Box'         , 'off'     , ...
        'TickDir'     , 'out'     , ...
        'TickLength'  , [.02 .02] , ...
        'XMinorTick'  , 'on'      , ...
        'YMinorTick'  , 'on'      , ...
        'YGrid'       , 'on'      , ...
        'XColor'      , [.3 .3 .3], ...
        'YColor'      , [.3 .3 .3], ...
        'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
        'LineWidth'   , 1 );
    legend({'1 day post MI','3 days post MI','7 days post MI'},'Location','NorthEast','Fontsize',16);
    %Work-around for digits adapted from https://uk.mathworks.com/matlabcentral/answers/158707-force-scientific-notation-in-axes
    yl=ylim;
    yt=get(gca,'ytick');
    if max(yt(:))<0.01
        % get order of magnitude
        e=log10(yl(2));
        e=sign(e)*floor(abs(e));
        % get and rescale yticks
        yt=get(gca,'ytick')/10^e;
        % create tick labels
        ytl=cell(size(yt));
        for j=1:length(yt)
            % the space after the percent gives the same size to positive and
            % negative numbers. The number of decimal digits can be changed.
            ytl{j}=sprintf('% 1.2f',yt(j));
        end
        % set tick labels
        set(gca,'yticklabel',ytl);
        % place order of magnitude
        fs = get(gca,'fontsize');
        set(gca,'units','normalized');
        xl = xlim;
        text(xl(1),yl(2),sprintf('\\times10^{%d}',e),...
            'fontsize',fs,'VerticalAlignment','bottom');
    elseif max(yt(:))>1&max(yt(:))<10
        % get and rescale yticks
        yt=get(gca,'ytick');
        % create tick labels
        ytl=cell(size(yt));
        for j=1:length(yt)
            % the space after the percent gives the same size to positive and
            % negative numbers. The number of decimal digits can be changed.
            ytl{j}=sprintf('% 1.2f',yt(j));
        end
        % set tick labels
        set(gca,'yticklabel',ytl);
        % place order of magnitude
        fs = get(gca,'fontsize');
        set(gca,'units','normalized');
    elseif max(yt(:))>=10
        % get and rescale yticks
        yt=get(gca,'ytick');
        % create tick labels
        ytl=cell(size(yt));
        for j=1:length(yt)
            % the space after the percent gives the same size to positive and
            % negative numbers. The number of decimal digits can be changed.
            ytl{j}=sprintf('% 1.0f',yt(j));
        end
        % set tick labels
        set(gca,'yticklabel',ytl);
        % place order of magnitude
        fs = get(gca,'fontsize');
        set(gca,'units','normalized');
    end
    set(gcf, 'Position', get(0,'Screensize')); % Maximize figure.
    %----------------------------------------------------------------------
    %           Name to use for saving the plot
    %----------------------------------------------------------------------
    %Eliminate delimeters that cannot be used for filenames like %, \,/,
    %>,<
    temp=[];
    if length(namePlot)>1
        for ll=1:length(namePlot)
            temp=[temp char(namePlot(ll))];
        end
        namePlot=temp;
    end
    
    str=strsplit(namePlot,'(\');
    if length(str)>1
        tit=str{1};
    else
        tit=namePlot;
    end
    temp=strsplit(tit,' ');
    temp2='';
    for i=1:size(temp,2)
        temp2=strcat(temp2,temp{i});
    end
    temp=strsplit(temp2,'(%)');
    tit2=temp{1};
    
    %Eliminate less than or equal
    temp=findstr(tit2,'\leq');
    if ~isempty(temp)
        tit2=[tit2(1:(temp-1)),'smallerthan',tit2((temp+1):end)];
    end
    
    %Eliminate \
    temp=strsplit(tit2,'\');
    tit2=temp{1};
    
    %Eliminate /
    temp=strsplit(tit2,'/');
    tit2=temp{1};
    
    %Eliminate greater than
    temp=strsplit(tit2,'>');
    if length(temp)>1
        tit2=[temp{1},temp{2}];
    end
    
    %Title
    saveName=tit2;
    %Save the figure
    saveas(gca,[folderOut,saveName,'.png'])
    saveas(gca,[folderOut,saveName,'.fig'])
    print(fullfile([folderOut,saveName,'.eps']), '-depsc');
else
    error('Tissue conditions under comparison are expected to be 7')
end

