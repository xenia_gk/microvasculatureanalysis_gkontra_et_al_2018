function [properties] = calc_properties(im,voxelSize)
%Calculate geomtric caracteristics (minkowski-based parameters) of 3D
%volumes
%
%Input:         
%               - im: 3D volume for zhich the minkowski-based properties are
%               going to be calculated
%               - voxelSize: image resolution               
%Output:        
%               - properties: vector containing the following geometric
%               properties in each column; 1. Volume, 2. Density, 3. Surface area, 4.
%               Surface area density, 5. Mean breadth density,
%               7. Euler number density 
%Calls functions from http://www.mathworks.com/matlabcentral/fileexchange/48515-areal--volumetric-and-textural-parameters-from-2d-and-3d-images/content/Biofilm%20analysis/imMeanBreadth.m
%
%Author: Xenia Gkontra, CNIC


im=double(im);
%Volume Density
properties(1,1)=imVolumeDensity(im)*100;
%Surface area
properties(1,2)=imSurface(im,voxelSize);
%Surface area density
properties(1,3)=imSurfaceDensity(im,voxelSize);
%Mean breadth density
refVol=prod(size(im)-1)*prod(voxelSize);
properties(1,4)=imMeanBreadth(im,voxelSize)/refVol;
%Euler number density
vol=numel(im) * prod(voxelSize);
properties(1,5)=imEuler3d(im,26)/vol;
%Euler number density
%properties(7)=imEuler3dDensity(im);

end

