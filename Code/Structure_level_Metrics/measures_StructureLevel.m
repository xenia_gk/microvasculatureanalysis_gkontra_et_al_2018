function [prop_remote,prop_infarct,prop_remoteXMI,prop_infarctXMI,subj_remote,subj_infarct,subj_remoteXMI,subj_infarctXMI,prop_basal,subj_basal] = measures_StructureLevel(mainDir,filenames,prop_remote,prop_infarct,prop_basal,Subjects,subj_remote,subj_infarct,subj_basal,mainDir_Art,mainDir_BW)
%Function to process all volumes of a particular time-point so as to calculate the
%fractal and minkowski-based parameters and return updated matrices with
%the parameters calculated for each volume processed.
%
%Input:
%               - mainDir: directory where the fractal propeerties in
%               structure Params_2_Complete_imageName.mat are saved
%               - filenames: names of images to be processed
%               - prop_remote: matrix where the parameters for every volume
%               are saved if the tissue from which they were acquired
%               belongs to remote areas independently of the time point.
%               Evey row correspond to a different volume and every column
%               to a particular metric
%               - prop_infarct: matrix where the parameters for every volume
%               are saved if the tissue from which they were acquired
%               belongs to infarcted areas independently of the time point.
%               Evey row correspond to a different volume and every column
%               to a particular metric.
%               - prop_basal: matrix where the parameters for every volume
%               are saved if the tissue from which they were acquired
%               belongs to basal areas.Evey row correspond to a different
%               volume and every column to a particular metric.
%               - Subjects: matrix of size (number of images)x2. The second
%               column corresponds to the image name and the first column
%               to the subject's name/number from which the image was taken
%               - subj_remote: matrix to save the subject's name for every
%               volume from remote area being processed. Each row
%               of subj_remote refers to the same row at prop_remote
%               - subj_infarct: matrix to save the subject's name for every
%               volume from remote area being processed. Each row
%               of subj_remote refers to the same row at prop_remote
%               - subj_basal: matrix to save the subject's name for every
%               volume from basal area being processed. Each row
%               of subj_remote refers to the same row at prop_remote
%               - mainDir_Art: directory where the volume indicating
%               possible arterioles or venules regions are saved
%               - mainDir_BW: directory where the reconstructed
%               microvasculature is saved (BW)
%
%
%Output:
%               - prop_remote,prop_infarct, subj_remote,subj_infarct,
%               prop_basal,subj_basal: Input matrixes updated by the values
%               for the volumes being processed (ie the ones in filenames)
%               during the call of the function
%               -prop_remoteXMI,prop_infarctXMI: matrix of size (number of
%               volumes)x(number of parameters). If volumes belonging to a
%               time-point after MI were processed during the call of the
%               function then the parameters for each volume are saved
%               on prop_remoteXMI,prop_infarctXMI depending on whether the
%               volume belongs to a remote or infarcted area. Please not
%               that each row refers to a volume and each column to a
%               parameter. If the volumes being processed where from
%               infarcted areas, those matrices will be empty.
%               - subj_remoteXMI: array of size (number of
%               volumes). If volumes belonging to a
%               time-point after MI were processed during the call of the
%               function then each row i contains the subject's name/number
%               to which the volume whose paraemeters are saved at the row i
%               of prop_remoteXMI
%               - subj_infarctXMI: similar to subj_remoteXMI but for
%               infarcted volumes
%
%Author: Xenia Gkontra, CNIC


%Initialize
prop_remoteXMI=[];
prop_infarctXMI=[];
subj_remoteXMI=[];
subj_infarctXMI=[];

for i=3:length(filenames)
    
    %Image name
    name=filenames(i).name;
    %Tissue category
    [tissueCat]=define_tissueCat(name);
    %Input directory
    directoryIn=[mainDir,tissueCat,'/',name,'/'];
    
    %Define voxel size and volume
    [voxelSize,volume_voxel]=define_voxelSize(name);
    %Load microvascular binary volume under investigation
    load([mainDir_BW,'/',tissueCat,'/',name,'/BW_',name,'.mat']);
    BWCh2=double(BW);
    %Fractal and geometric properties
    x=load_properties_Complete(BWCh2,name,directoryIn,voxelSize);
    %Load arterioles related information for calcualting geometric metrics
    %only in the area of cappilaries
    dataArt=load([mainDir_Art,'/',tissueCat,'/',name,'/Arterioles_',name,'.mat']);
    Vfiltered=dataArt.Vfiltered>0;
    Cap=(1-Vfiltered).*BW;
    x_cap= calc_properties(Cap,voxelSize);
    %Saving the results in the corresponding matrix depending on whether
    %the tissue catgeory is remote, infarcted or basal
    if ~isempty(find(strcmp(tissueCat,'Remote'),1))
        prop_remote=[prop_remote;x x_cap];
        prop_remoteXMI=[prop_remoteXMI;x x_cap];
        %Find row corresponding to this particular image so as to get the
        %subject
        indx=find(ismember(Subjects(:,1),name));
        %Save the subject number
        subj_remoteXMI=[subj_remoteXMI;Subjects{indx,2}];
        subj_remote=[subj_remote;Subjects{indx,2}];
    elseif ~isempty(find(strcmp(tissueCat,'Infarct'),1))
        prop_infarct =[prop_infarct;x x_cap];
        prop_infarctXMI=[prop_infarctXMI;x x_cap];
        %Find row corresponding to this particular image so as to get the
        %subject
        indx=find(ismember(Subjects(:,1),name));
        %Save the subject number
        subj_infarctXMI=[subj_infarctXMI;Subjects{indx,2}];
        subj_infarct=[subj_infarct;Subjects{indx,2}];
    else
        prop_basal =[prop_basal;x x_cap];
        %Find row corresponding to this particular image so as to get the
        %subject
        indx=find(ismember(Subjects(:,1),name));
        %Save the subject number
        subj_basal=[subj_basal;Subjects{indx,2}];
    end
end
end

