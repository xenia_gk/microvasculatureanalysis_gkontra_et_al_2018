function [FDc,CutOff,x1,y1] = fractal_dimension(n1,r1,type,orderPol)
%Plot fractal dimension, fiting line and residuals. Calculate cut-offs for
%FD calculation based on statistical significance of residuals plot
%curvilinearity (berntson criteria: Berntson and Stoll, Proc. R. Soc. Lond.
%B (1997))
%
%Input:
%               - n1: number of boxes needed to cover the object at each
%               scale r1
%               - r1: number of boxes, ie scales, used 
%               - type: name of image being processed, used for creating
%               plot titles & names for saving the plots
%               - orderPol: polynomial order to be used 
%
%Output:        
%               - FDc: fractal dimension after the application of cut-offs,
%               ie fractal dimension is being calculated only over the
%               range of scales that the objects can be considered as
%               self-similar
%               - CutOff: If CutOff=1, cut-off could be calculated, 
%                 else cut-off could not be calculated
%               - x1: smallest box size, ie scale, that the object is
%               self-smilar
%               - y1: largest box size, ie scale, that the object is
%               self-smilar
%
%Based on Berntson & Stoll, 2011, and 
%http://www.mathworks.es/es/help/curvefit/examples/polynomial-curve-fitting.html
%http://www.mathworks.es/es/help/stats/f-statistic-and-t-statistic.html
%
%Author: Xenia Gkontra, CNIC

%--------------------------------------------------------------------------
%                   Fractal Dimension Plot
%--------------------------------------------------------------------------

%Log values
x_init=log(r1);
y_init=log(n1);
siz=length(x_init);

%--------------------------------------------------------------------------
%Way A: Elimining one box size per time starting from the biggest
%--------------------------------------------------------------------------
%Eliminate until at least three scales are left
pvalue1=zeros(siz-4+1,3);
f_pvalue1=zeros(siz-4+1);
for i=1:siz
    %When at least at 3 scales are left 
    if (siz-i+1)>=(orderPol+1)
        %Call function to create residual plot and calcualate statistical
        %significance for curvilinearity
        [~,~,resnew]=fractal_cut_off2(x_init,y_init,1,siz-i+1,type,0,orderPol);
        %Smallest box size taken into account
        pvalue1(i,1)=1;
        %Varying largest box size taken into account
        pvalue1(i,2)=siz-i+1;
        %P-value demonstrating statistal significance of the second order polynomials
        pvalue1(i,3)=resnew.tstat.pval(orderPol);
        f_pvalue1(i)=resnew.fstat.pval;
    end
end
%Find the boxes for which the object did not presented curvilinearity, ie
%scale for which it can be considered self-similar
indx=find(pvalue1(:,3)>0.1);
if ~isempty(indx)
    %keep the largest range of boxes that satisfy self-similarity
    [m1,indx2]=max((pvalue1(indx,2)-pvalue1(indx,1)+1));
    indx3=indx(indx2);
    pval1=pvalue1(indx3,:);
else
    pval1=0;
    m1=0;
end

%--------------------------------------------------------------------------
%Way B: Elimining one box size per time starting from the smallest
%--------------------------------------------------------------------------
%Eliminate until at least three scales are left
pvalue2=zeros(siz-4+1,3);
f_pvalue2=zeros(siz-4+1);
for i=1:siz
    if (siz-i+1)>=(orderPol+1)
        %Call function to create residual plot and calcualate statistical
        %significance for curvilinearity
        [~,~,resnew]=fractal_cut_off2(x_init,y_init,i,siz,type,0,orderPol);
        %Varying smallest box size taken into account
        pvalue2(i,1)=i;
        %Largest box size taken into account
        pvalue2(i,2)=siz;
        %P-value demonstrating statistal significance of the second order polynomials
        %for the above smallest and largest box size
        pvalue2(i,3)=resnew.tstat.pval(3);
        f_pvalue2(i)=resnew.fstat.pval;
    end
end
clear indx indx2 indx3
%Find&Keep the boxes for which the object did not presented curvilinearity, ie
%scale for which it can be considered self-similar:  p value < 0.1 is taken
%as evidence that the object is not self-similar at that particular range
indx=find(pvalue2(:,3)>0.1);
if ~isempty(indx)
    %keep the largest range of boxes that satisfy self-similarity
    [m2,indx2]=max((pvalue2(indx,2)-pvalue2(indx,1)+1));
    indx3=indx(indx2);
    pval2=pvalue2(indx3,:);
else
    pval2=0;
    m2=0;
end

%--------------------------------------------------------------------------
%Way C: Elimining two box sizes per time, biggest and smallest
%--------------------------------------------------------------------------
%Eliminate until at least three scales are left
pvalue3=zeros(siz-4+1,3);
f_pvalue3=zeros(siz-4+1);
for i=1:siz
    if (siz-2*i+2)>=(orderPol+1)
        %Call function to create residual plot and calcualate statistical
        %significance for curvilinearity
        [~,~,resnew]=fractal_cut_off2(x_init,y_init,i,siz-i+1,type,0,orderPol);
        %Varying smallest box size taken into account
        pvalue3(i,1)=i;
        %Varying largest box size taken into account
        pvalue3(i,2)=siz-i+1;
        %P-value demonstrating statistal significance of the second order polynomials
        %for the above smallest and largest box size
        pvalue3(i,3)=resnew.tstat.pval(3);
        f_pvalue3(i)=resnew.fstat.pval;
    end
end
clear indx indx2 indx3
indx=find(pvalue3(:,3)>0.1);
if ~isempty(indx)
    %keep the largest range of boxes that satisfy self-similarity
    [m3,indx2]=max((pvalue3(indx,2)-pvalue3(indx,1)+1));
    indx3=indx(indx2);
    pval3=pvalue3(indx3,:);
else
    pval3=0;
    m3=0;
end

%--------------------------------------------------------------------------
%         Erosion technique: Larger absolute range
%       of scales for which the object is self-similar
%--------------------------------------------------------------------------
%Keep the largest range of boxes that satisfy self-similarity

%If for all ways (A,B,C see above) cut-offs were calculated keep those that give the larger
%range of boxes
if (m1~=0)&&(m2~=0)&&(m3~=0)
    [~,ind]=max([m1 m2 m3]);
    if ind==1
        pval=pval1;
    elseif ind==2
        pval=pval2;
    elseif ind==3
        pval=pval3;
    end
    %Mark than a cut-off was found
    CutOff=1;
    %If cut-offs were calculated with way B (ie eliminate a box size, starting from
    %the smallest) & way C (Elimining two box sizes per time, biggest and smallest)
elseif (m1==0)&&(m2~=0)&&(m3~=0)
    [~,ind]=max([m2 m3]);
    if ind==1
        pval=pval2;
    elseif ind==2
        pval=pval3;
    end
    CutOff=1;
    %If cut-offs were calculated with  way A (ie Elimining one box size per
    %time starting from the biggest) & way C (Elimining two box sizes per time, biggest and smallest)
elseif (m1~=0)&&(m2==0)&&(m3~=0)
    [~,ind]=max([m1 m3]);
    if ind==1
        pval=pval1;
    elseif ind==2
        pval=pval3;
    end
    CutOff=1;
    %If cut-offs were calculated with way A (ie Elimining one box size per
    %time starting from the biggest) & way B (ie eliminate a box size,
    %starting from the smallest) &
elseif (m1~=0)&&(m2~=0)&&(m3==0)
    [~,ind]=max([m1 m2]);
    if ind==1
        pval=pval1;
    elseif ind==2
        pval=pval2;
    end
    CutOff=1;
    %If cut-off were calculated with way A
elseif (m1~=0)&&(m2==0)&&(m3==0)
    pval=pavl1;
    CutOff=1;
    %If cut-off were calculated with way B
elseif (m1==0)&&(m2~=0)&&(m3==0)
    pval=pval2;
    CutOff=1;
    %If cut-off were calculated with way C
elseif (m1==0)&&(m2==0)&&(m3~=0)
    pval=pval3;
    CutOff=1;
    %If cut-off was not calculated with any of the ways
elseif (m1==0)&&(m2==0)&&(m3==0)
    pval=pvalue1(1,:);
    CutOff=0;
end

%Final fractal dimension with cut-offs
[FDc]=fractal_cut_off2(x_init,y_init,pval(1,1),pval(1,2),[type,' final'],1,orderPol);
%Starting and ending points to be saved
x1=pval(1,1);
y1=pval(1,2);