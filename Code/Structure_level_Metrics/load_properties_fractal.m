function [properties] = load_properties_fractal(type,VolumePath,VolumePath2)
%Load properties already calculated using function Analysis_v3, calculate more Minkowski properties
%and save the results in a matrix where:
%                                   1st column: Fractal Dimension
%                                   2nd columd: Succolarity (Mean among 6
%                                   directions)
%                                   3rd columd: Succolarity (Max among 6
%                                   directions)
%                                   4rth columd: Succolarity (Min among 6
%                                   directions)
%                                   5th column: Lacunarity
%                                   6th column: Fractal dimension with
%                                   cut-offs (FDc)
%                                   7ht colum: If CutOff=1, cut-off could 
%                                   be calculated, else cut-off could not be calculated
%                                   8th column:  Lower Cut-off for FDc
%                                   9th column: Higher Cut-off for FDc
%
%Author: Xenia Gkontra, CNIC 2014

%Initialize
properties=[];

%Check whether the fractal-based metrics have been calculated at an earlier
%step as expected or otherwise print the missing file and stop.
A=exist([VolumePath,'Params_Complete_2','_',type,'.mat'],'file');
B=exist([VolumePath2,'succolarity2','_',type,'.mat'],'file');
if (A~=0)&&(B~=0)
    %Load structure with fractal-based metrics as caluclated in previous
    %steps
    load([VolumePath,'Params_Complete_2','_',type,'.mat']);
    load([VolumePath2,'succolarity2','_',type,'.mat']);
    %First column fractal dimension
    properties(1,1)=[parameters5.Df];
    %Second-Third-Forth column succolarity
    properties(1,2)=mean(suc_All);
    properties(1,3)=max([suc_dir1,suc_dir2,suc_dir3,suc_dir4,suc_dir5,suc_dir6]);
    properties(1,4)=min([suc_dir1,suc_dir2,suc_dir3,suc_dir4,suc_dir5,suc_dir6]);
    %Fifth column lacunarity
    properties(1,5)=mean(parameters5.L);
    %Calculate fractal dimension with cut-offs
    [FDc,CutOff,x1,y1] = fractal_dimension(parameters5.n1,parameters5.r1,[' ',type,' Complete'],4);
    %Sixth column Fractal dimension with cut-offs, 6th whether cut-offs
    %where calculated, 8th and 9th columns the starting and ending scales
    %respectevely for which the object can be considered a fractal
    properties =[properties FDc CutOff x1 y1];
else
    fprintf('The program cannot continue, it is now on pause, one of the following files was not found: . Press ctrl+c to stop')
    [VolumePath,'Params_Complete_2','_',type,'.mat']
    [VolumePath2,'succolarity2','_',type,'.mat']
end
end

