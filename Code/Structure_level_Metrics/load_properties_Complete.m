function [properties] = load_properties_Complete(BWCh2,type,VolumePath,voxelSize)
%Load properties already calculated using .../Code/Fractal-based/,
%refine fractal metrics (fractal dimension with cut-offs, succolarity
%approximated as mean, median, max), calculate Minkowski-based
%parameters and save the results in a matrix named properties (one metric per column)
%
%Input:
%                       - BWCh2: volume to be processed
%                       - type: image name to be used for saving the plot
%                       of FD
%
%
%Output:
%                       - proprties: matrix 1xnumber of parameters, ie
%                       1st column: Fractal Dimension without cut-offs
%                                   2nd columd: Density
%                                   3rd column: Lacunarity
%
%Author: Xenia Gkontra, CNIC

properties=[];
%Calculate fractal-based properties
[properties_fractals] = load_properties_fractal(type,VolumePath,VolumePath) ;
%Order in properties matrix the fractal properties and the
%minkowksi-based calculated during the call of the function
%calc_properties
properties =[properties_fractals(:,1:5) calc_properties(BWCh2,voxelSize) properties_fractals(:,6:9)];

end