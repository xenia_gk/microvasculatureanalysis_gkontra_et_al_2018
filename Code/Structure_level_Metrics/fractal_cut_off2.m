function [Fd,res3,resnew,r2,rmse] = fractal_cut_off2(x_init,y_init,starting,ending,type,last,orderPol)
% Statistical significance for curvilinearity present at the residuals plot
% of the fractal dimension fitting
%
%Input:
%                   - x_init: array with the boxes used 
%                   - y_init: array with the number of boxes necessary to
%                   cover the object under investigation for the different
%                   box sizes
%                   - starting: smallest box size to e taken into account
%                   - ending: largest box size to be taken into account
%                   - type: name of volume being processed to be saved
%                   -last: indicates if the starting and ending scales
%                   are the ones used to calculated the final fractal
%                   dimension with cut-offs so as to save the corresponding
%                   plot in Results2
%                   - orderPol: order of the polynomial to be used for
%                   fitting
%
%Output:
%                   - Fd: Fractal dimension calculting for the scales
%                   between the starting and ending
%                   - res3: Regression diagnostics for fitting model for fd
%                   - resnew: Regression diagnostics for fitting model for
%                   residuals plot
%                   - r2: Rsquare to evaluate the fitting for FD
%                   - rmse: Root mean square error for evaluating the
%                   fitting for FD
%
%Author: Xenia Gkontra, CNIC 

%Define region (i.e. box size to be taken into account)
x=x_init(starting:ending);
y=y_init(starting:ending);

%--------------------------------------------------------------------------
%               Fractal Dimension
%--------------------------------------------------------------------------

%Fit line using least squares
[p,S] = polyfit(x,y,1);
%[yd,delta]=polyval(p,x,S);
a1 = p(2);
%Slope ->its negative corresponds to the fractal dimension
a2 = p(1);
fit1 = a1+a2*x;

%Rsquare to evaluate the fitting
[r2,rmse] = rsquare(y,fit1);
%Test regstats
res3=regstats(y,x,(0:1)');


%Generate Figure
figure(2);
plot(x,y,'b-','LineWidth',1);
hold on;
plot(x,fit1,'r-','LineWidth',1);
for i=1:length(x)
    plot(x(i),y(i),'o','LineWidth',1);
end
hold off;
% xlim([0 max(x_init(:))])
% ylim([0 max(y_init(:))])
ylabel('ln(n(r))')
xlabel('ln(r)')
legend('ln(n(r))-ln(r)','Fitted line')
title(['3D box-count: ',type,' - Fractal dimension= ',num2str(-a2), ', R2 = ',num2str(r2),', RMSE = ',num2str(rmse)]);
%saveas(gca,['Results2\FD_',num2str(starting),'_',num2str(ending),'_',type,'.png']);
Fd=-a2;
%--------------------------------------------------------------------------
%                   Residuals Plot
%--------------------------------------------------------------------------

%Residuals of fitting
res=y-fit1;
%Fit third-order polynonimal to residuals plot
[p2,S2]=polyfit(fit1,res,orderPol);
[y2,delta]=polyval(p2,fit1,S2);
%Use regstats to get statistics -keep in mind that regstats order is
%stats.beta(1)+stas.beta(2)*x+stats.beta(3)*x^2+stats(4).beta*x^3, so to
%check for significance for curvilenearity stats.tstat.pval(4) (reverse
%compared with the output of polyfit stats(4).beta=p(1))
resnew=regstats(res,fit1,(0:orderPol)');
[y3]=polyval([resnew.beta(end:-1:1)],fit1);

%Rsquare to evaluate the fitting
[r2_2,rmse_2] = rsquare(res,y2);
%y2=polyval(p2,x);
%Generate Residuals plot
figure(1)
plot(fit1,res,'b','LineWidth',1);
title(['Residual plot',  ', R2 = ',num2str(r2_2),', RMSE = ',num2str(rmse_2)])
hold on;
plot(fit1,y2,'r','LineWidth',1);
plot(fit1,y3,'m','LineWidth',1);
plot([1:max(fit1(:))],zeros(size([1:max(fit1)]))','g--')

hold off;
ylabel('Residuals')
xlabel('Fitted values')
legend('Residuals plot',['Polynominal of order ',num2str(orderPol),' (polyfit)' ],['Polynominal of order ',num2str(orderPol),' (regstats)' ])
yl=ylim;
if last==1
    set(gca, ...
        'Box'         , 'off'     , ...
        'TickDir'     , 'out'     , ...
        'TickLength'  , [.02 .02] , ...
        'XMinorTick'  , 'on'      , ...
        'YMinorTick'  , 'on'      , ...
        'YGrid'       , 'on'      , ...
        'XColor'      , [.3 .3 .3], ...
        'YColor'      , [.3 .3 .3], ...
        'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
        'LineWidth'   , 1    );
    
    saveas(gca,['Results2\RP_',num2str(starting),'_',num2str(ending),'_',type,'.png']);
end

%--------------------------------------------------------------------------
%               Create the final plot
%--------------------------------------------------------------------------
if last==1
    %Fit line using least squares
    [p,S] = polyfit(x_init,y_init,1);
    %[yd,delta]=polyval(p,x,S);
    a1 = p(2);
    %Slope ->its negative corresponds to the fractal dimension
    a2 = p(1);
    fit1 = a1+a2*x_init;
    
    %Rsquare to evaluate the fitting
    [r2,rmse] = rsquare(y_init,fit1);
    %Generate Figure
    figure(2);
    plot(x_init,y_init,'b-','LineWidth',1);
    hold on;
    plot(x_init,fit1,'r-','LineWidth',1);
    
    for i=1:length(x_init)
        plot(x_init(i),y_init(i),'o','LineWidth',1);
    end
    
    plot(x(1),y(1),'r*','Linewidth',1);
    plot(x(end),y(end),'r*','Linewidth',2)
    hold off;
    % xlim([0 max(x_init(:))])
    % ylim([0 max(y_init(:))])
    ylabel('ln(n(r))')
    xlabel('ln(r)')
    legend('ln(n(r))-ln(r)','Fitted line')
    title(['3D box-count: ',type,' - Fractal dimension= ',num2str(Fd), ', R2 = ',num2str(r2),', RMSE = ',num2str(rmse)]);
    yl=ylim;
    set(gca, ...
        'Box'         , 'off'     , ...
        'TickDir'     , 'out'     , ...
        'TickLength'  , [.02 .02] , ...
        'XMinorTick'  , 'on'      , ...
        'YMinorTick'  , 'on'      , ...
        'YGrid'       , 'on'      , ...
        'XColor'      , [.3 .3 .3], ...
        'YColor'      , [.3 .3 .3], ...
        'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
        'LineWidth'   , 1    );
    saveas(gca,['Results2\FD_',num2str(starting),'_',num2str(ending),'_',type,'.png']);
end

end


