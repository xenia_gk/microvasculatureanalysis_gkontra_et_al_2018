%--------------------------------------------------------------------------
%Function to calculate and/or gather metrics at structure level, ie
%fractal-metrics & minkowski-based metrics
%
%Note!! 1. Please note that the fractal metrics should have already been
%calculated at the previous step, but
%here they are loaded and some refinements are performed, ie the cut-offs
%scales (scales that the microvasculature can considered as a fractal) are calculated,
%different approximations of succolarity (mean, median, max along 6
%directions). In particular, the following MAT-Files are expected to have
%been already created at the previous steps:
%                   - Params_Complete_2_imageName.mat with fractal-based 
%                   metrics regarding fractal dimension and lacunarity 
%                   created by module at Code/Fractal-Based/Fd_Lacunarity
%                   - succolarity2_imageName.mat with succolarity related
%                   parameters created by module Code/Fractal-Based/Succolarity/
%                   - BW_imageName.mat with the 3D reconstructed
%                   microvasculature after application of the filling
%                   pipeline
%                   - SegmSMA_imageName.mat with the 3D segmentation of the 
%                   SMA channel
%2. If you have a different number of time-points than in the original case
%remember to change accordingly lines 119-126
%
%Output:
%                   - a .mat file is saved with name Res3.mat in
%                   outputFolder. The file saves one matrix per tissue condition
%                   and time point (prop_infarct1MI, prop_remote1MI,
%                   prop_infract3MI, prop_infarct7MI, prop_remote7MI, 
%                   prop_basal) and two more for infarcted tissue and
%                   remote independently of the day post MI (prop_remote,
%                   prop_infarct) with the calcuated results.  The matrices are of
%                   size (number of samples)x(number of parameters), ie
%                   every row corresponds to a volume that has been
%                   analyzed and every column to a parameter. Furthremore,
%                   an array per tissue condition and time point 
%                   (subj_infarct1MI, subj_remote1MI, subj_infract3MI, 
%                   subj_infarct7MI, subj_remote7MI, subj_basal) as well as
%                   one for infarcted tissue and one for
%                   remote independently of the day post MI (subj_remote,
%                   subj_infarct) with the results is saved in Res3.mat.
%                   Each of those arrays contains in every row i the
%                   subject's number from which the image 
%                   volume at row i or the corresponding
%                   prop_remote/prop_infarct/prop_basal was obtained
%                   (e.g subj_remote7MI(i) indicates the subject's number 
%                   from which the volume whose parameters are saved in 
%                   prop_remote7MI(i,:) was obtained)
%                   
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clf;
clear;
close all;

%--------------------------------------------------------------------------
%                     Directories & Initialize
%--------------------------------------------------------------------------
%Call function to load the directories
directories=set_directories(GetFullPath('..'));
%Directory to fractal-based metrics caluclations during earlier step
mainDir_Fractals=directories.mainDir_Fractals;
%The images in this directory are not ordered by tissue category
mainDir=directories.mainDir;
%Directory where 3D volume with possible arterioles/venules areas are saved
mainDir_Art=directories.mainDir_Art;
%List time-points after MI to be studied. Please note first two folders
%returned by Matlab's "dir" function are '.' and '..' so we ignore them
daysMI_All=dir(mainDir);
%Directory where the VE-Cadherin segmentations after the application of teh
%filling algorithm can be found
mainDir_BW=directories.mainDir_Filled;
%Folder to save output
folderOut=directories.mainDir_Structure_Results;
if ~exist(folderOut)
    mkdir(folderOut);
end
%Folder to save the images created during caluclation of fractal dimension
%with cut-offs
folderOut2=['Results2/'];
if ~exist(folderOut2)
    mkdir(folderOut2);
end

%Initialize
prop_remote=[];prop_remote1MI=[];prop_remote3MI=[];prop_remote7MI=[];
prop_infarct=[];prop_infarct1MI=[];prop_infarct3MI=[];prop_infarct7MI=[];
prop_basal=[];
subj_remote=[];subj_remote1MI=[];subj_remote3MI=[];subj_remote7MI=[];
subj_infarct=[];subj_infarct1MI=[];subj_infarct3MI=[];subj_infarct7MI=[];
subj_basal=[];

%Matrix with column 1 the subject number and column 2 the image name. Note
%that this was added just for other comparisons between subjects etc not
%presented here, you can just put the same subjects for all volumes if you
%are not interested in sorting by subject in later steps
[Subjects]=image_to_subject_correspondance();

%--------------------------------------------------------------------------
%                 Calculate & Gather metrics
%--------------------------------------------------------------------------
for j=3:length(daysMI_All)
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    %Find images in the directory
    directoryIn=[mainDir,'/',daysMI,'/'];
    filenames=dir(directoryIn);
    %Directory for reconstructed microvasculatrue after the application of
    %the filling pipeline
    directoryFilled=[mainDir_BW,daysMI,'/'];
    %Directory where the arterioles are
    directoryArt=[mainDir_Art,daysMI,'/'];
    %Directory where the fractal-based metrics calculated in the previous
    %step are saved
    directoryFractals=[mainDir_Fractals,daysMI,'/'];
    %Load and calculate properties
    %Change the following lines accordingly to your time-points after MI if
    %this is not the same as in our case.
    if j==3
        [prop_remote,prop_infarct,prop_remote1MI,prop_infarct1MI,subj_remote,subj_infarct,subj_remote1MI,subj_infarct1MI,prop_basal,subj_basal] = measures_StructureLevel(directoryFractals,filenames,prop_remote,prop_infarct,prop_basal,Subjects,subj_remote,subj_infarct,subj_basal,directoryArt,directoryFilled);
    elseif j==4
        [prop_remote,prop_infarct,prop_remote3MI,prop_infarct3MI,subj_remote,subj_infarct,subj_remote3MI,subj_infarct3MI,prop_basal,subj_basal] = measures_StructureLevel(directoryFractals,filenames,prop_remote,prop_infarct,prop_basal,Subjects,subj_remote,subj_infarct,subj_basal,directoryArt,directoryFilled);
    elseif j==5
        [prop_remote,prop_infarct,prop_remote7MI,prop_infarct7MI,subj_remote,subj_infarct,subj_remote7MI,subj_infarct7MI,prop_basal,subj_basal] = measures_StructureLevel(directoryFractals,filenames,prop_remote,prop_infarct,prop_basal,Subjects,subj_remote,subj_infarct,subj_basal,directoryArt,directoryFilled);
    elseif j==6
         [~,~,~,~,~,~,~,~,prop_basal,subj_basal] = measures_StructureLevel(directoryFractals,filenames,prop_remote,prop_infarct,prop_basal,Subjects,subj_remote,subj_infarct,subj_basal,directoryArt,directoryFilled);
    end
end

%Save the results
save([folderOut,'/Res3.mat'],'subj_basal','subj_remote7MI','subj_infarct7MI','subj_remote1MI','subj_infarct1MI','subj_remote3MI','subj_infarct3MI','prop_basal','prop_remote','prop_infarct','prop_infarct1MI','prop_remote1MI','prop_infarct3MI','prop_remote3MI','prop_infarct7MI','prop_remote7MI')



