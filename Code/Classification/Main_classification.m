%Function to perform classification of unseen volumes in the different
%tissue categories. The feautures used for the clasification are the ones
%kept by function join_results. 9-fold cross-validation repeated 10 times
%is used in order to ensure different separations of the sample
%
%Output:
%               - .xls & .mat files during the call classification_all_pairs_CV
%               with the accuracy achieved for each classifier and each
%               comparison pair 
%               - .xls file with the results of the different
%               classification pairs gathered in similar format as the
%               table presented Gkontra et et., 2018
%               - xls.file with the results in a format ready to be copied 
%               to latex to create the table 2 of  Gkontra et et, 2018 
%               (rounded results)
%
%Note! You should first have run join_results to create the common matrices
%with all metrics that will be used as features for the classification of
%unseen microvascular networks
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com

clc;
clear;
close all;
%Output Folder
outputFolder='Results_CV\';
if ~exist(outputFolder,'dir')
    mkdir(outputFolder)
end

%Load data saved by join results
load(['Results/ResultsWithSMA.mat'])
%Number of sma metrics to keep: from 1 to 3
sma_num=3;
%Which metrics of SMA to use
sma_indexes=(size(prop_BASAL,2)-sma_num-1:size(prop_BASAL,2));
%Eliminate position 1 and 2 that contain the image name and the subject to
%which it belongs
m_indexes=(3:size(prop_BASAL,2)-sma_num-2);
%Names of the feautures used
FeatureNames=Columns([m_indexes,sma_indexes]);

%--------------------------------------------------------------------------
%           Classification with CV including SMA metrics
%--------------------------------------------------------------------------

%Matrices
prop_infarct1MI=[prop_I1MI(:,m_indexes) prop_I1MI(:,sma_indexes)];
prop_remote1MI=[prop_R1MI(:,m_indexes) prop_R1MI(:,sma_indexes)];
prop_infarct3MI=[prop_I3MI(:,m_indexes) prop_I3MI(:,sma_indexes)];
prop_remote3MI=[prop_R3MI(:,m_indexes) prop_R3MI(:,sma_indexes)];
prop_infarct7MI=[prop_I7MI(:,m_indexes) prop_I7MI(:,sma_indexes)];
prop_remote7MI=[prop_R7MI(:,m_indexes) prop_R7MI(:,sma_indexes)];
prop_basal=[prop_BASAL(:,m_indexes) prop_BASAL(:,sma_indexes)];
%Classification
classification_all_pairs_CV(prop_infarct1MI,prop_remote1MI,prop_infarct3MI,prop_remote3MI,prop_infarct7MI,prop_remote7MI,prop_basal,'SMA',outputFolder,FeatureNames)

%--------------------------------------------------------------------------
%               Save an overall .xls
%--------------------------------------------------------------------------
create_xls_overall(outputFolder);