function []=create_xls_overall(outputFolder)
%Function to create a common .xls that contains all results (rounded) from
%different classification pairs and an xls with the results in a format to
%be copied to latex for creating a table like the one in Gkontra et al.,
%2018
%
%Input:
%               - outputFolder: directory where the .xls with the results
%               for each classification pair are already saved and where
%               the created .xlsx will be saved
%
%Output:
%               - SMAOverall_CV_SMA_round.xlsx contains all results (rounded) 
%               from different classification pairs
%               - SMAOverall_CV_SMA_round_TABLE.xlsx contains all results
%               in a fomat that can be copied directly to latex to create a
%               table
%
%Author: Xenia Gkontra, CNIC

classif=[1:4];
%Infarcted vs Remote
data1=xlsread([outputFolder,'SMAInfarcted_vs_Remote.xls']);
table1(1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAI1MI_vs_R1MI.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAI3MI_vs_R3MI.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAI7MI_vs_R7MI.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));

%Infarcted progressively in time
data1=xlsread([outputFolder,'SMAInfarcted_over_time.xls']);
table1(size(table1,1)+3,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAI1MI_vs_I3MI.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAI1MI_vs_I7MI.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAI3MI_vs_I7MI.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));

%Remote progressively in time
data1=xlsread([outputFolder,'SMARemote_over_time.xls']);
table1(size(table1,1)+3,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAR1MI_vs_R3MI.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAR1MI_vs_R7MI.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAR3MI_vs_R7MI.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));

%Basal versus infarct
data1=xlsread([outputFolder,'SMABasal_vs_infarcted.xls']);
table1(size(table1,1)+3,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAI1MI_vs_basal.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAI3MI_vs_basal.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAI7MI_vs_basal.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));

%Basal versus remote
data1=xlsread([outputFolder,'SMABasal_vs_remote.xls']);
table1(size(table1,1)+3,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAR1MI_vs_basal.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAR3MI_vs_basal.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));
data1=xlsread([outputFolder,'SMAR7MI_vs_basal.xls']);
table1(size(table1,1)+1,1:4)=round(data1(1,classif));

%--------------------------------------------------------------------------
%                   Save results
%--------------------------------------------------------------------------

filename = [outputFolder,'SMAOverall_CV_SMA_round.xls'];
A = {'' 'Bayes ' 'knn' 'svn' 'ada';
    'Infarcted - remote'  table1(1,1)  table1(1,2) table1(1,3) table1(1,4);
    'I1MI_vs_R1MI' table1(2,1)  table1(2,2) table1(2,3) table1(2,4);
    'I3MI_vs_R3MI' table1(3,1)  table1(3,2) table1(3,3) table1(3,4);
    'I7MI_vs_R7MI'  table1(4,1)  table1(4,2) table1(4,3) table1(4,4);
    '' '' '' '' '';
    '' 'Bayes ' 'knn' 'svn' 'ada';
    'Infarcted over time' table1(7,1)  table1(7,2) table1(7,3)  table1(7,4);
    'I1MI_vs_I3MI' table1(8,1)  table1(8,2) table1(8,3) table1(8,4);
    'I1MI_vs_I7MI' table1(9,1)  table1(9,2) table1(9,3)  table1(9,4);
    'I7MI_vs_I3MI'  table1(10,1)  table1(10,2) table1(10,3)  table1(10,4);
    '' '' '' '' '';
   '' 'Bayes ' 'knn' 'svn' 'ada';
   'Remote_over_time' table1(13,1)  table1(13,2) table1(13,3) table1(13,4);
    'R1MI_vs_R3MI' table1(14,1)  table1(14,2) table1(14,3) table1(14,4);
    'R1MI_vs_R7MI' table1(15,1)  table1(15,2) table1(15,3) table1(15,4);
    'R7MI_vs_R3MI'  table1(16,1)  table1(16,2) table1(16,3) table1(16,4);
    '' '' '' '' '';
    '' 'Bayes ' 'knn' 'svn' 'ada';
    'Basal_vs_infarcted' table1(19,1)  table1(19,2) table1(19,3) table1(19,4);
    'Basal_vs_I1MI' table1(20,1)  table1(20,2) table1(20,3) table1(20,4);
    'Basal_vs_I3MI' table1(21,1)  table1(21,2) table1(21,3) table1(21,4);
    'Basal_vs_I7MI'  table1(22,1)  table1(22,2) table1(22,3) table1(22,4);
    '' '' '' '' '';
    '' 'Bayes ' 'knn' 'svn' 'ada';
    'Basal_vs_remote' table1(25,1)  table1(25,2) table1(25,3) table1(25,4);
    'Basal_vs_R1MI' table1(26,1)  table1(26,2) table1(26,3) table1(26,4);
    'Basal_vs_R3MI' table1(27,1)  table1(27,2) table1(27,3) table1(27,4);
    'Basal_vs_R7MI'  table1(28,1)  table1(28,2) table1(28,3) table1(28,4);
    };
xlswrite(filename,A)
B{1,:}=' Classifier  & \textbf{1} & \textbf{2} & \textbf{3} & \textbf{4} &Classifier & \textbf{1} & \textbf{2} & \textbf{3} & \textbf{4}\\' ;
B{size(B,1)+1,:}='\hline \hline';
B{size(B,1)+1,:}=['\textbf{Infarcted - Basal} & ',num2str(table1(19,1)),' & ',num2str(table1(19,2)),' & ',num2str(table1(19,3)),' & ',num2str(table1(19,4)),...
    ' & \textbf{Remote - Basal} & ',num2str(table1(25,1)),' & ',num2str(table1(25,2)),' & ',num2str(table1(25,3)),' & ',num2str(table1(25,4)),' \\ \hline'];
B{size(B,1)+1,:}=['I1MI - Basal & ',num2str(table1(20,1)),' & ',num2str(table1(20,2)),' & ',num2str(table1(20,3)),' & ',num2str(table1(20,4)),...
    ' & R1MI - Basal & ',num2str(table1(26,1)),' & ',num2str(table1(26,2)),' & ',num2str(table1(26,3)),' & ',num2str(table1(26,4)),' \\'];
B{size(B,1)+1,:}=['I3MI - Basal & ',num2str(table1(21,1)),' & ',num2str(table1(21,2)),' & ',num2str(table1(21,3)),' & ',num2str(table1(21,4)),...
    ' & R3MI - Basal & ',num2str(table1(27,1)),' & ',num2str(table1(27,2)),' & ',num2str(table1(27,3)),' & ',num2str(table1(27,4)),' \\'];
B{size(B,1)+1,:}=['I7MI - Basal & ',num2str(table1(22,1)),' & ',num2str(table1(22,2)),' & ',num2str(table1(22,3)),' & ',num2str(table1(22,4)),...
    ' & R7MI - Basal & ',num2str(table1(28,1)),' & ',num2str(table1(28,2)),' & ',num2str(table1(28,3)),' & ',num2str(table1(28,4)),' \\ \hline'];
B{size(B,1)+1,:}=['\textbf{Infarcted over time} & ',num2str(table1(7,1)),' & ',num2str(table1(7,2)),' & ',num2str(table1(7,3)),' & ',num2str(table1(7,4)),...
    '& \textbf{Remote over time} & ',num2str(table1(13,1)),' & ',num2str(table1(13,2)),' & ',num2str(table1(13,3)),' & ',num2str(table1(13,4)),' \\ \hline'];
B{size(B,1)+1,:}=['I1MI - I3MI	& ',num2str(table1(8,1)),' & ',num2str(table1(8,2)),' & ',num2str(table1(8,3)),' & ',num2str(table1(8,4)),...
    ' & R1MI - R3MI	& ',num2str(table1(14,1)),' & ',num2str(table1(14,2)),' & ',num2str(table1(14,3)),' & ',num2str(table1(14,4)),' \\'];
B{size(B,1)+1,:}=['I1MI - I7MI	& ', num2str(table1(9,1)),' & ',num2str(table1(9,2)),' & ',num2str(table1(9,3)),' & ' ,num2str(table1(9,4)),...
    ' & R1MI - R7MI	& ',num2str(table1(15,1)),' & ',num2str(table1(15,2)),' & ',num2str(table1(15,3)),' & ',num2str(table1(15,4)),' \\'];
B{size(B,1)+1,:}=['I7MI - I3MI	& ',num2str(table1(10,1)),' & ',num2str(table1(10,2)),' & ',num2str(table1(10,3)),' & ',num2str(table1(10,4)),...
    ' & R7MI - R3MI & ',num2str(table1(16,1)),' & ',num2str(table1(16,2)),' & ',num2str(table1(16,3)),' & ',num2str(table1(16,4)),' \\'];
B{size(B,1)+1,:}=['\hline'];
B{size(B,1)+1,:}=['\textbf{Infarcted - Remote}	& ',num2str(table1(1,1)),' & ',num2str(table1(1,2)),' & ',num2str(table1(1,3)),' & ',num2str(table1(1,4)),'  & \multicolumn{1}{l}{} \\ \cline{1-5}'];
B{size(B,1)+1,:}=['I1MI - R1MI & ',num2str(table1(2,1)),' & ',num2str(table1(2,2)),' & ',num2str(table1(2,3)),' & ',num2str(table1(2,4)),'  & \multicolumn{1}{l}{} \\'];
B{size(B,1)+1,:}=['I3MI - R3MI & ',num2str(table1(3,1)),' & ',num2str(table1(3,2)),' & ',num2str(table1(3,3)),' & ',num2str(table1(3,4)),'  & \multicolumn{1}{l}{} \\'];
B{size(B,1)+1,:}=['I7MI - R7MI & ',num2str(table1(4,1)),' & ',num2str(table1(4,2)),' & ',num2str(table1(4,3)),' & ',num2str(table1(4,4)),' & \multicolumn{1}{l}{} \\  \cline{1-5} '];
filename = [outputFolder,'SMAOverall_CV_SMA_round_TABLE.xlsx'];
xlswrite(filename,B)