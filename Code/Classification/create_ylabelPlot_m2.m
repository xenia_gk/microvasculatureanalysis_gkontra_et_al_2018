function [titlePlot]=create_ylabelPlot_m2()
%Function that contains the names for the fractal- & minkowksi based
%metrics that were calculated earlier.
%
%Input: -
%Output:
%                   -titlePlot: cell array with the name of the metrics
%                   that have been calculated. Cell i corresponds to column
%                   i of matrices(e.g. prop_remote,prp_infarct,prop_basal)
%                   were the metrics are saved
%
%Author: Xenia Gkontra


%--------------------------------------------------------------------------
%                   All vessels
%--------------------------------------------------------------------------
titlePlot{1}={'Branching nodes',;'per mm^3 of tissue'}; %1
titlePlot{size(titlePlot,2)+1}={'Vascular segments',;'per mm^3 of tissue'};   %2
titlePlot{size(titlePlot,2)+1}='Number of ind microvascular networks';  %3
titlePlot{size(titlePlot,2)+1}={'Blind-ends/sprouts ',;'per mm^3 of tissue'};   %4
titlePlot{size(titlePlot,2)+1}='Length (\mum)';     %5
titlePlot{size(titlePlot,2)+1}='Mean radius (\mum)';        %6
titlePlot{size(titlePlot,2)+1}={'Vascular segment radius ';'(\mum)'};    %7
titlePlot{size(titlePlot,2)+1}='Min radius (\mum)'; %8
titlePlot{size(titlePlot,2)+1}='Degree of nodes';   %9
titlePlot{size(titlePlot,2)+1}='Number of bifurcations';    %10
titlePlot{size(titlePlot,2)+1}='Number of trifurcations';   %11
titlePlot{size(titlePlot,2)+1}='Number of higher order connections';    %12
titlePlot{size(titlePlot,2)+1}='Tortuosity';        %13
titlePlot{size(titlePlot,2)+1}={'Vascular segment volume ';'(\mum^3)'};   %14
titlePlot{size(titlePlot,2)+1}={'Vascular segment length ';'(\mum)'};       %15
titlePlot{size(titlePlot,2)+1}='Vascular segment diameter (\mum)';     %16
titlePlot{size(titlePlot,2)+1}={'Vascular segment surface ';'(\mum^2)'};  %17
titlePlot{size(titlePlot,2)+1}={'Vascular Volume Density ';'(\mum^3/\mum^3)'};      %18
titlePlot{size(titlePlot,2)+1}={'Vascular Length Density ';'(\mum/\mum^3)'};      %19
titlePlot{size(titlePlot,2)+1}={'Vascular Surface Density ';'(\mum^2/\mum^3)'};     %20
titlePlot{size(titlePlot,2)+1}={'Diffusion distance ';'(\mum)'};      %21
titlePlot{size(titlePlot,2)+1}='Damage Index';      %22
titlePlot{size(titlePlot,2)+1}='Damage Index - Capillaries';          %23
titlePlot{size(titlePlot,2)+1}='Vessels Covered with SMA (%)';      %24
titlePlot{size(titlePlot,2)+1}='SMA thickness (\mum)';      %25
titlePlot{size(titlePlot,2)+1}='Vessels Covered with SMA  (%)';     %26
titlePlot{size(titlePlot,2)+1}='Vessels Covered with SMA  (%)';     %27
titlePlot{size(titlePlot,2)+1}='SMA thickness (\mum)';          %28
titlePlot{size(titlePlot,2)+1}='SMA thickness (\mum)';      %29
titlePlot{size(titlePlot,2)+1}='Capillary Density Ratio';          %30
titlePlot{size(titlePlot,2)+1}={'Intercapillary Distance ';'(\mum)'};      %31
titlePlot{size(titlePlot,2)+1}={'Capillary Density';'(capillaries per mm^2 of tissue)'};      %32
%--------------------------------------------------------------------------
%                 Characteristics of Vessels covered with sma
%--------------------------------------------------------------------------
titlePlot{size(titlePlot,2)+1}={'Vascular segment volume ';'(\mum^3)'};   %33
titlePlot{size(titlePlot,2)+1}={'Vascular segment length ';'(\mum)'};   %34
titlePlot{size(titlePlot,2)+1}={'Vascular segment diameter ';'(\mum)'}; %35
titlePlot{size(titlePlot,2)+1}={'Vascular segment surface ';'(\mum^2)'};  %36
titlePlot{size(titlePlot,2)+1}='Vascular Volume Density ';  %37
titlePlot{size(titlePlot,2)+1}='Vascular Length Density (\mum/\mum^3)';  %38
titlePlot{size(titlePlot,2)+1}='Vascular Surface Density (\mum^2/\mum^3)'; %39
titlePlot{size(titlePlot,2)+1}='Diffusion distance (\mum)';  %40
titlePlot{size(titlePlot,2)+1}='Tortuosity';        %41
titlePlot{size(titlePlot,2)+1}='Length (\mum)';      %42
titlePlot{size(titlePlot,2)+1}='Mean radius (\mum)';       %43
%Max
titlePlot{size(titlePlot,2)+1}='Vascular segment radius (\mum)';  %44
titlePlot{size(titlePlot,2)+1}='Min radius';    %45
%--------------------------------------------------------------------------
%                  Characteristics of Vessels not covered with SMA
%--------------------------------------------------------------------------
titlePlot{size(titlePlot,2)+1}='Vascular segment volume (\mum^3)';   %46
titlePlot{size(titlePlot,2)+1}='Vascular segment length  (\mum)';   %47
titlePlot{size(titlePlot,2)+1}='Vascular segment diameter (\mum)'; %48
titlePlot{size(titlePlot,2)+1}='Vascular segment surface (\mum^2)';  %49
titlePlot{size(titlePlot,2)+1}='Vascular Volume Density ';  %50
titlePlot{size(titlePlot,2)+1}='Vascular Length Density (\mum/\mum^3)';  %51
titlePlot{size(titlePlot,2)+1}='Vascular Surface Density '; %52
titlePlot{size(titlePlot,2)+1}='Diffusion distance (\mum)';  %53
titlePlot{size(titlePlot,2)+1}='Tortuosity';    %54
titlePlot{size(titlePlot,2)+1}='Length';    %55
titlePlot{size(titlePlot,2)+1}='Mean radius (\mum)';   %56
%Max
titlePlot{size(titlePlot,2)+1}='Vascular segment radius (\mum)'; %67
titlePlot{size(titlePlot,2)+1}='Min radius (\mum)';    %58
%-------------
titlePlot{size(titlePlot,2)+1}='Num of small vessels'; %59
titlePlot{size(titlePlot,2)+1}='Num of medium vessels'; %60
titlePlot{size(titlePlot,2)+1}='Num of large vessels'; %61
titlePlot{size(titlePlot,2)+1}={'Vessels of diameter ';'\leq 6.9 \mum (%)'}; %62
titlePlot{size(titlePlot,2)+1}={'Vessels of diameter between ';' 6.9 and 8.2 \mum (%)'}; %63
titlePlot{size(titlePlot,2)+1}={'Vessels of diameter  ';'> 8.2 \mum (%)'}; %64

%--------------------------------------------------------------------------
%                   Mertics per mm^3 & SMA thickness
%--------------------------------------------------------------------------
titlePlot{size(titlePlot,2)+1}={'Small vessels',;'per mm^3 of tissue'}; %65
titlePlot{size(titlePlot,2)+1}={'Medium vessels' ,;'per mm^3 of tissue'}; %66
titlePlot{size(titlePlot,2)+1}={'Large vessels',;'per mm^3 of tissue'}; %67
titlePlot{size(titlePlot,2)+1}={'Endothelial cells',;'per mm^3 of tissue'}; %68
titlePlot{size(titlePlot,2)+1}={'SMA+ cells touching vessels',;'per mm^3 of tissue'}; %69
titlePlot{size(titlePlot,2)+1}={'Myofibroblasts',;'per mm^3 of tissue'}; %70
titlePlot{size(titlePlot,2)+1}='SMA thickness (\mum)'; %71
titlePlot{size(titlePlot,2)+1}='SMA thickness (\mum)'; %72
titlePlot{size(titlePlot,2)+1}='SMA thickness (\mum)'; %73

%--------------------------------------------------------------------------
%               Metrics per vascular length
%--------------------------------------------------------------------------
titlePlot{size(titlePlot,2)+1}={'Cap Branching nodes',;'per mm^3 of vascular volume'}; %74
titlePlot{size(titlePlot,2)+1}={'Cap Vascular segments',;'per mm^3 vascular volume'}';   %75
titlePlot{size(titlePlot,2)+1}={'Cap Blind-ends/sprouts ',;'per mm^3 vascular volume'}';   %76
titlePlot{size(titlePlot,2)+1}={'Endothelial cells',;'per mm^3 vascular volume'}; %77
titlePlot{size(titlePlot,2)+1}={'SMA+ cells touching vessels',;'per mm^3 vascular volume'}; %78
titlePlot{size(titlePlot,2)+1}={'Myofibroblasts',;'per mm^3 vascular volume'}; %79

%--------------------------------------------------------------------------
%               Metrics per vascular volume
%--------------------------------------------------------------------------
titlePlot{size(titlePlot,2)+1}={'Branching nodes',;'per mm vascular length'}; %80
titlePlot{size(titlePlot,2)+1}={'Vascular segments',;'per mm vascular length'}';   %81
titlePlot{size(titlePlot,2)+1}={'Blind-ends/sprouts ',;'per mm vascular length'};  %82
titlePlot{size(titlePlot,2)+1}={'Endothelial cells',;'per mm vascular length'}; %83
titlePlot{size(titlePlot,2)+1}={'SMA+ perivascular cells',;'per mm vascular length'}; %84
titlePlot{size(titlePlot,2)+1}={'Myofibroblasts',;'per mm vascular length'}; %85

%----------------------------------------------------------------------
%                     Extravascular Distances
%----------------------------------------------------------------------
titlePlot{size(titlePlot,2)+1}={'Median  extravascular Distance',;'Frequency distibution (\mum)'}; %86
titlePlot{size(titlePlot,2)+1}={'Maximal extravascular Distance';'Frequency distibution  (\mum)'}; %87
titlePlot{size(titlePlot,2)+1}={'Median extravascular Distance',;'All microvessels (\mum)'}; %88
titlePlot{size(titlePlot,2)+1}={'Maximal extravascular Distance';'All microvessels (\mum)'}; %89