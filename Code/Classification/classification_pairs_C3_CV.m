function []=classification_pairs_C3_CV(categories,featVect,filename,FeatureNames)
%Function to classify into multiple classes using cross-validation
%During the call of the  function, the function that
%performs multi-calss classification is called (all_Classifiers) and the 
%classification results are saved in .mat and .xls
%
%Input:
%               - categories: cell array of size (number of samples)x1.
%               Each cell contains the class to which the sample belongs to
%               (e.g. Infarcted, Remote, Basal)
%               - featVect: matrix of size (number of samples)x(number of
%               features). Each row i contains the features for sample (ie
%               image volume) i with one feature per column
%               - filename: path with filename to save the results (.mat
%               and .xls)
%               - FeatureNames: names of the features used (in the current
%               version not used)
%
%Output:
%               - filename.mat file with the classification results is saved (filename
%               contains both the filename & path)
%               - filename.xls fle with the classification results is saved (filename
%               contains both the filename & path)
%
%Author: Xenia Gkontra, CNIC

%--------------------------------------------------------------------------
%               Perform classification
%--------------------------------------------------------------------------

[meanErrorRateBayes,meanCorrectRateBayes,meanErrorRateKNN,meanCorrectRateKNN,meanErrorRateSVM,meanCorrectRateSVM,meanErrorRateADA,meanCorrectRateADA]=all_Classifiers(categories,featVect);

%--------------------------------------------------------------------------
%                       Save results
%--------------------------------------------------------------------------

save([filename,'.mat'])
A = {'' 'Naive Bayes' 'knn' 'SVM' 'Adaboost';
    'No Selection' meanCorrectRateBayes*100 meanCorrectRateKNN*100 meanCorrectRateSVM*100 meanCorrectRateADA*100;};
xlswrite(filename,A)