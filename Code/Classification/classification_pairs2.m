function []=classification_pairs2(prop_infarct,prop_remote,filename,FeatureNames)
%Function to classify into binary classes using cross-validation
%During the call of the function the feature vector and the categories for
%classification are built, the function that performs binary classification
%is called and the classification results are saved in .mat and .xls
%
%Input: 
%               - prop_infarct: matrix of size (number of samples)x(number
%               of features) of the first class
%               - prop_remote: matrix of size (number of samples)x(number
%               of features) of the second class
%               - filename: path with filename to save the results (.mat
%               and .xls)
%               - FeatureNames: names of the features used (in the current
%               version not used)
%
%Output:
%               - filename.mat file with the classification results saved (filename
%               contains both the filename & path)
%               - filename.xls fle with the classification results saved (filename
%               contains both the filename & path)
%
%Author: Xenia Gkontra, CNIC

%Total images
infarct=size(prop_infarct,1);
remote=size(prop_remote,1);
total=infarct+remote;

%--------------------------------------------------------------------------
%                        Classification
%--------------------------------------------------------------------------

%Cell array of size (number of samples)x1 with the classes to which the
%samples belong to
categories=cell(total,1);
categories(1:infarct,1)={'Infarct'};
categories(infarct+1:infarct+remote)={'Remote'};

%1st method: Using all features - Each row i contains the features for sample (ie
%image volume) i with one feature per column
featVect=[prop_infarct;prop_remote];
%Perfom classification
[meanErrorRate_Bayes,meanCorrectRate_Bayes,meanErrorRate_knn,meanCorrectRate_knn,meanErrorRate_svm,meanCorrectRate_svm,meanErrorRate_Adaboost,meanCorrectRate_Adaboost]=all_Classifiers(categories,featVect);


%--------------------------------------------------------------------------
%                  Save results in .xls and .mat
%--------------------------------------------------------------------------

save([filename,'.mat'])
A = {'' 'Naive Bayes' 'knn' 'SVM' 'Adaboost';
'No Selection' meanCorrectRate_Bayes*100 meanCorrectRate_knn*100 meanCorrectRate_svm*100 meanCorrectRate_Adaboost*100};
xlswrite(filename,A)