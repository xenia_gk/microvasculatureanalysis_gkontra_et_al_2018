%Function to join results from the two categories of metrics, ie (i)
%metrics that regard the microvasculatrue as a whole (fractal & minkowski
%based metrics) & (ii) metrics based on the graph based form of the
%microvasculature as well as SMA-related metrics
%
%Author: Xenia Gkontra, CNIC

clear;
close all;


%If method==1, then only fractal & minkowski &sma  metrics are saved in the
%final matrices, otherwise the graph-based metrics are also saved
method=2;

%--------------------------------------------------------------------------
%                         Directories
%--------------------------------------------------------------------------


%Call function to load the directories
directories=set_directories(GetFullPath('..'));
%Directory for loading the image names
mainDir=directories.mainDir;
%Directory where fractal & minkowski-based metrics are saved
directory_fractals=directories.mainDir_Structure_Results;
%Directory where graph & SMA metrics are saved
directory_graph=directories.mainDir_Segment_Results;
%Check if folder exist

folderOut='Results/';
if ~exist(folderOut,'dir')
    mkdir(folderOut);
end

%--------------------------------------------------------------------------
%              Load fractal & minkowski based metrics &
%              find subject to which the volume belongs
%--------------------------------------------------------------------------
load([directory_fractals,'Res3.mat'])

%Input directory
mainDir2=[mainDir,'/1MI/'];
%Find images in the directory
filenames1MI=dir(mainDir2);
%Load and caluclate properties
[prop_I1MI,prop_R1MI] = findImg(filenames1MI,prop_remote1MI,prop_infarct1MI,prop_basal,subj_remote1MI,subj_infarct1MI,subj_basal);

%   3 days after MI
%Input directory
mainDir2=[mainDir,'/3MI/'];
%Find images in the directory
filenames3MI=dir(mainDir2);
%Load and caluclate properties
[prop_I3MI,prop_R3MI] = findImg(filenames3MI,prop_remote3MI,prop_infarct3MI,prop_basal,subj_remote3MI,subj_infarct3MI,subj_basal);

%   7 days after MI
%Input directory
mainDir2=[mainDir,'/7MI/'];
%Find images in the directory
filenames7MI=dir(mainDir2);
%Load and caluclate properties
[prop_I7MI,prop_R7MI] = findImg(filenames7MI,prop_remote7MI,prop_infarct7MI,prop_basal,subj_remote7MI,subj_infarct7MI,subj_basal);

%Basal
mainDir_basal2=[mainDir,'/Basal/'];
%Find images in the directory
filenamesBasal=dir(mainDir_basal2);
%Load and caluclate properties
[~,~,prop_BASAL] = findImg(filenamesBasal,[],[],prop_basal,[],[],subj_basal);

%Keep only mean succolarity and not max,min succolarity (column 5,6),
%eliminate some cuts offs related measures, ie 'Cut-Offs', 'Lower Cut-off'
% 'Higher Cut-off')
m_Index=[1:4 7:8 10:12 17 19];
prop_I1MI=prop_I1MI(:,m_Index);
prop_R1MI=prop_R1MI(:,m_Index);
prop_I3MI=prop_I3MI(:,m_Index);
prop_R3MI=prop_R3MI(:,m_Index);
prop_I7MI=prop_I7MI(:,m_Index);
prop_R7MI=prop_R7MI(:,m_Index);
prop_BASAL=prop_BASAL(:,m_Index);
%If you  want to check the metrics to which those indexes correspond,
%call create_ylabelplot as in the follwing lines - uncomment
m_index2=m_Index(3:end)-2;
metricsName_fractals=create_ylabelPlot_m1;
metricsNameFinal_fractals=metricsName_fractals([m_index2])';
for me=1:size(metricsNameFinal_fractals,1)
    if size(metricsNameFinal_fractals{me},1)==2
        metricsNameFinal_fractals{me}=[cell2mat(metricsNameFinal_fractals{me}(1,1)) ' ' cell2mat(metricsNameFinal_fractals{me}(2,1))];
    elseif size(metricsNameFinal_fractals{me},2)==2
        metricsNameFinal_fractals{me}=[cell2mat(metricsNameFinal_fractals{me}(1,1)) ' ' cell2mat(metricsNameFinal_fractals{me}(1,2))];
    end
end

%--------------------------------------------------------------------------
%              Load SMA & otehr network based metrics  &
%             find subject to which the volume belongs
%--------------------------------------------------------------------------
%Load graph based and sma metrics
load([directory_graph,'Res3.mat'])

%  1 day after MI
%Load and caluclate properties
[prop_SMA_I1MI,prop_SMA_R1MI] = findImg(filenames1MI,prop_remote1MI,prop_infarct1MI,prop_basal,subj_remote1MI,subj_infarct1MI,subj_basal);

%   3 days after MI
%Load and caluclate properties
[prop_SMA_I3MI,prop_SMA_R3MI] = findImg(filenames3MI,prop_remote3MI,prop_infarct3MI,prop_basal,subj_remote3MI,subj_infarct3MI,subj_basal);

%   7 days after MI
%Load and caluclate properties
[prop_SMA_I7MI,prop_SMA_R7MI] = findImg(filenames7MI,prop_remote7MI,prop_infarct7MI,prop_basal,subj_remote7MI,subj_infarct7MI,subj_basal);

%Basal
%Load and caluclate properties
[temp1,temp2,prop_SMA_BASAL] = findImg(filenamesBasal,[],[],prop_basal,[],[],subj_basal);


if method==1 %Keep only sma metrics (column 24, 26), image number (column 1) and subject
    %(2), no other graph-based metrics
    m_Index=[1 2];
    %Define sma related metrics to keep
    sma_indexes=[26 29 30];
else %if network metrics are kept (see README_matrices to see which metrics are kept)
    %Define network related metrics to keep. The positions are moved by two
    %compared with the order of metrics in create_ylabelPlot
    m_Index=[1 2 3 4 6 9 16 17 19 15 20 21 22 64 65 66 82 84];

    %Define sma related metrics to keep

    sma_indexes=[24 26 27];
    %If you  want to check the metrics to which those indexes correspond,
    %call create_ylabelplot as in the follwing lines - uncomment
    m_index2=m_Index(3:end)-2;
    sma_indexes2=sma_indexes-2;
    metricsName=create_ylabelPlot_m2;
    metricsNameFinal=metricsName([m_index2 sma_indexes2])';
    for me=1:size(metricsNameFinal,1)
        if size(metricsNameFinal{me},1)==2
            metricsNameFinal{me}=[cell2mat(metricsNameFinal{me}(1,1)) ' ' cell2mat(metricsNameFinal{me}(2,1))];
        elseif size(metricsNameFinal{me},2)==2
            metricsNameFinal{me}=[cell2mat(metricsNameFinal{me}(1,1)) ' ' cell2mat(metricsNameFinal{me}(1,2))];
        end
    end
end

SMA_basal=[prop_SMA_BASAL(:,m_Index) prop_SMA_BASAL(:,sma_indexes)];
SMA_I1MI=[prop_SMA_I1MI(:,m_Index) prop_SMA_I1MI(:,sma_indexes)];
SMA_R1MI=[prop_SMA_R1MI(:,m_Index) prop_SMA_R1MI(:,sma_indexes)];
SMA_I3MI=[prop_SMA_I3MI(:,m_Index) prop_SMA_I3MI(:,sma_indexes)];
SMA_R3MI=[prop_SMA_R3MI(:,m_Index) prop_SMA_R3MI(:,sma_indexes)];
SMA_I7MI=[prop_SMA_I7MI(:,m_Index) prop_SMA_I7MI(:,sma_indexes)];
SMA_R7MI=[prop_SMA_R7MI(:,m_Index) prop_SMA_R7MI(:,sma_indexes)];
%--------------------------------------------------------------------------
%           Sort according to image number
%--------------------------------------------------------------------------
%First sort according to number
if ~isempty(prop_I1MI)
    [~,indx]=sort(prop_I1MI(:,1));
    I1MI=prop_I1MI(indx,:);
end
if ~isempty(prop_R1MI)
    [~,indx]=sort(prop_R1MI(:,1));
    R1MI=prop_R1MI(indx,:);
end
if ~isempty(prop_I3MI)
    [~,indx]=sort(prop_I3MI(:,1));
    I3MI=prop_I3MI(indx,:);
end
if ~isempty(prop_R3MI)
    [~,indx]=sort(prop_R3MI(:,1));
    R3MI=prop_R3MI(indx,:);
end
if ~isempty(prop_I7MI)
    [~,indx]=sort(prop_I7MI(:,1));
    I7MI=prop_I7MI(indx,:);
end
if ~isempty(prop_R7MI)
    [~,indx]=sort(prop_R7MI(:,1));
    R7MI=prop_R7MI(indx,:);
end
if ~isempty(prop_BASAL)
    [~,indx]=sort(prop_BASAL(:,1));
    BASAL=prop_BASAL(indx,:);
end
if ~isempty(SMA_I1MI)
    [~,indx]=sort(SMA_I1MI(:,1));
    SMA_I1MI=SMA_I1MI(indx,:);
end
if ~isempty(SMA_R1MI)
    [~,indx]=sort(SMA_R1MI(:,1));
    SMA_R1MI=SMA_R1MI(indx,:);
end
if ~isempty(SMA_I3MI)
    [~,indx]=sort(SMA_I3MI(:,1));
    SMA_I3MI=SMA_I3MI(indx,:);
end
if ~isempty(SMA_R3MI)
    [~,indx]=sort(SMA_R3MI(:,1));
    SMA_R3MI=SMA_R3MI(indx,:);
end
if ~isempty(SMA_I7MI)
    [~,indx]=sort(SMA_I7MI(:,1));
    SMA_I7MI=SMA_I7MI(indx,:);
end
if ~isempty(SMA_R7MI)
    [~,indx]=sort(SMA_R7MI(:,1));
    SMA_R7MI=SMA_R7MI(indx,:);
end
if ~isempty(SMA_basal)
    [~,indx]=sort(SMA_basal(:,1));
    SMA_basal=SMA_basal(indx,:);
end
%--------------------------------------------------------------------------
%          Join Fractal, Minkowski, Graph-Based and SMA metrics
%--------------------------------------------------------------------------
%All metrics: Fractal, Minkowski+ SMA

if method==1
    prop_I1MI=[I1MI SMA_I1MI(:,3:end)];
    prop_R1MI=[R1MI SMA_R1MI(:,3:end)];
    prop_I3MI=[I3MI SMA_I3MI(:,3:end)];
    prop_R3MI=[R3MI SMA_R3MI(:,3:end)];
    prop_I7MI=[I7MI SMA_I7MI(:,3:end)];
    prop_R7MI=[R7MI SMA_R7MI(:,3:end)];
    prop_BASAL=[BASAL SMA_basal(:,3:end)];
else
    prop_I1MI=[I1MI SMA_I1MI(:,3:end)];
    prop_R1MI=[R1MI SMA_R1MI(:,3:end)];
    prop_I3MI=[I3MI SMA_I3MI(:,3:end)];
    prop_R3MI=[R3MI SMA_R3MI(:,3:end)];
    prop_I7MI=[I7MI SMA_I7MI(:,3:end)];
    prop_R7MI=[R7MI SMA_R7MI(:,3:end)];
    prop_BASAL=[BASAL SMA_basal(:,3:end)];
end
%--------------------------------------------------------------------------
%                   Sort according to subject
%--------------------------------------------------------------------------
[~,indx1]=sort(prop_I1MI(:,2));
prop_I1MI=prop_I1MI(indx1,:);
[~,indx1]=sort(prop_R1MI(:,2));
prop_R1MI=prop_R1MI(indx1,:);

[~,indx1]=sort(prop_I3MI(:,2));
prop_I3MI=prop_I3MI(indx1,:);
[~,indx1]=sort(prop_R3MI(:,2));
prop_R3MI=prop_R3MI(indx1,:);

[~,indx1]=sort(prop_I7MI(:,2));
prop_I7MI=prop_I7MI(indx1,:);
[~,indx1]=sort(prop_R7MI(:,2));
prop_R7MI=prop_R7MI(indx1,:);

[~,indx1]=sort(prop_BASAL(:,2));
prop_BASAL=prop_BASAL(indx1,:);

%--------------------------------------------------------------------------
%                   Column correspondance
%--------------------------------------------------------------------------
Columns=['Image name';'Subject';metricsNameFinal_fractals;metricsNameFinal];

%--------------------------------------------------------------------------
%                   Save results
%--------------------------------------------------------------------------
save([folderOut,'ResultsWithSMA.mat'],'Columns','prop_I1MI','prop_R1MI','prop_I3MI','prop_R3MI','prop_I7MI','prop_R7MI','prop_BASAL')


