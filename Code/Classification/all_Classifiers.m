function [meanErrorRate_Bayes,meanCorrectRate_Bayes,meanErrorRate_knn,meanCorrectRate_knn,meanErrorRate_svm,meanCorrectRate_svm,meanErrorRate_Adaboost,meanCorrectRate_Adaboost]=all_Classifiers(categories,featVect)
%Function for performing binary of multi-class classification using 4
%different classifiers: Naive Bayes, K-nearest neighbors classifier (Knn)
%Support Vector Machines (SVM), and Adaboost. 9-Fold cross validation is 
%repeated 10 times. At each repeatition of the cross validation
%the classification accuracy and error rate is saved and the mean of the
%iterations is considered the overall correct or erroneous classification 
%rate. 
%
%Input:
%               - categories: cell array of size (number of samples)x1.
%               Each cell contains the class to which the sample belongs to
%               (e.g. Infarcted, Remote, Basal)
%               - featVect: matrix of size (number of samples)x(number of
%               features). Each row i contains the features for sample (ie
%               image volume) i with one feature per column
%
%Output:
%               - meanErrorRate_Bayes & meanCorrectRate_Bayes: error and 
%               and correct classification rate using Naive Bayes for
%               classication
%               - meanErrorRate_knn & meanCorrectRate_knn: error and 
%               and correct classification rate using K-nearest neighbors 
%               classifier for classication
%               - meanErrorRate_svm,meanCorrectRate_svm: error and 
%               and correct classification rate using Support Vector
%               Machines for classication
%               - meanErrorRate_Adaboost & meanCorrectRate_Adaboost: error
%               and correct classification rate using adaboost for
%               classication
%
%Author: Xenia Gkontra, CNIC

%Choose adaboost algortihm
cl=length(unique(categories));
if cl==2
    adaboost='AdaBoostM1';
else
    adaboost='AdaBoostM2';
end

%Repeat 10 times the cross-fold validation
iter=1;
iterMax=10;
ErrorRate=zeros(iterMax,1);
CorrectRate=zeros(iterMax,1);
while iter<=iterMax
    %Cross validation
    indices = crossvalind('Kfold',categories,9);
    %Track performance of classifiers
    mycp_knn = classperf(categories);
    mycp_bayes = classperf(categories);
    mycp_svm = classperf(categories);
    mycp_Adaboost = classperf(categories);
    for i = 1:9
        myTest = (indices == i); myTrain = ~myTest;
        
        %------------------------------------------------------------------
        %                      Naive Bayes Classifier
        %------------------------------------------------------------------
        % standardize the predictors.
        Mdl = fitcnb(featVect(myTrain,:),categories(myTrain,:), 'ClassNames',unique(categories));
        %         %Next three lines can be commented, they are just for verification of
        %         %results
        %         rloss = resubLoss(Mdl)
        %         CVMdl = crossval(Mdl);
        %         %From mathworks: Examine the cross-validation loss,
        %         %which is the average loss of each cross-validation model
        %         %when predicting on data that is not used for training.
        %         kloss = kfoldLoss(CVMdl)
        
        %Test the classifier
        myClass_Bayes = predict(Mdl,featVect(myTest,:));
        classperf(mycp_bayes,myClass_Bayes,myTest)
        %------------------------------------------------------------------
        %           KNN classifier
        %------------------------------------------------------------------
        
        %Train the classifier - I changed the number of neighbors from the
        %default
        t = templateKNN('NumNeighbors',3,'Standardize',1);
        Mdl = fitcecoc(featVect(myTrain,:),categories(myTrain,:),'Learners',t,...
            'ClassNames',unique(categories));
        %Next three lines can be commented, they are just for verification of
        %results
        %         rloss = resubLoss(Mdl)
        %         CVMdl = crossval(Mdl);
        %         %From mathworks: Examine the cross-validation loss,
        %         %which is the average loss of each cross-validation model
        %         %when predicting on data that is not used for training.
        %         kloss = kfoldLoss(CVMdl)
        %Test the classifier
        myClass_knn = predict(Mdl,featVect(myTest,:));
        classperf(mycp_knn,myClass_knn,myTest)
        %------------------------------------------------------------------
        %                       SVM
        %------------------------------------------------------------------
        t = templateSVM('Standardize',1);
        Mdl = fitcecoc(featVect(myTrain,:),categories(myTrain,:),'Learners',t,...
            'ClassNames',unique(categories));
        %Next three lines can be commented, they are just for verification of
        %results
        %         rloss = resubLoss(Mdl)
        %         CVMdl = crossval(Mdl);
        %         %From mathworks: Examine the cross-validation loss,
        %         %which is the average loss of each cross-validation model
        %         %when predicting on data that is not used for training.
        %         kloss = kfoldLoss(CVMdl)
        %Test the classifier
        myClass_svm = predict(Mdl,featVect(myTest,:));
        classperf(mycp_svm,myClass_svm,myTest)
        
        
        %------------------------------------------------------------------
        %                       Adaboost
        %------------------------------------------------------------------
        % Use Adaboost to make a classifier
        
        Mdl = fitensemble(featVect(myTrain,:),categories(myTrain,:),'Subspace',100,'KNN');
        %Next three lines can be commented, they are just for verification of
        %results
        %{
        rloss = resubLoss(Mdl)
        CVMdl = crossval(Mdl);
        %From mathworks: Examine the cross-validation loss,
        %which is the average loss of each cross-validation model
        %when predicting on data that is not used for training.
        kloss = kfoldLoss(CVMdl)
        %}
        %Test the classifier
        myClass_Adaboost = predict(Mdl,featVect(myTest,:));
        classperf(mycp_Adaboost,myClass_Adaboost,myTest)
        
    end
    %Save in array the error and correct rate for each classification
    %scheme
    ErrorRate_Bayes(iter)=mycp_bayes.ErrorRate;
    CorrectRate_Bayes(iter)=mycp_bayes.CorrectRate;
    ErrorRate_knn(iter)=mycp_knn.ErrorRate;
    CorrectRate_knn(iter)=mycp_knn.CorrectRate;
    ErrorRate_svm(iter)=mycp_svm.ErrorRate;
    CorrectRate_svm(iter)=mycp_svm.CorrectRate;
    ErrorRate_Adaboost(iter)=mycp_Adaboost.ErrorRate;
    CorrectRate_Adaboost(iter)=mycp_Adaboost.CorrectRate;
    clear mycp_bayes mycp_knn mycp_svm mycp_Adaboost
    iter=iter+1;
end
%Final error & correct error, ie the mean of the repeation of the
%cross-validation
meanErrorRate_Bayes=mean(ErrorRate_Bayes);
meanCorrectRate_Bayes=mean(CorrectRate_Bayes);
meanErrorRate_knn=mean(ErrorRate_knn);
meanCorrectRate_knn=mean(CorrectRate_knn);
meanErrorRate_svm=mean(ErrorRate_svm);
meanCorrectRate_svm=mean(CorrectRate_svm);
meanErrorRate_Adaboost=mean(ErrorRate_Adaboost);
meanCorrectRate_Adaboost=mean(CorrectRate_Adaboost);
clear iter;