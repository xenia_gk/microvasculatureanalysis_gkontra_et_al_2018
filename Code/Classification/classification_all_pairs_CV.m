function []=classification_all_pairs_CV(prop_infarct1MI,prop_remote1MI,prop_infarct3MI,prop_remote3MI,prop_infarct7MI,prop_remote7MI,prop_basal,sma,outputFolder,FeatureNames)
%Function for constrcuting the pairs to be classified and calling relative
%functions for training the classifiers & testing on unseen volumes.
%The categories under comparison are
%           - binary classifications:
%                   1. Remote vs Infarcted indepentenly of the time point
%                   2. Infarcted (indepentenly of the time point) vs basal
%                   3. Remote (indepentenly of the time point) vs basal
%                   4. Infarcted at each time-point vs Remote at each
%                   time-point
%                            - I1MI vs R1MI
%                            - I3MI vs R3MI
%                            - I7MI vs R7MI
%                   5. Infracted  progressively in time
%                            - I1MI vs I3MI
%                            - I3MI vs I7MI
%                            - I1MI_vs_I7MI
%                   6. Remote progressively in time
%                            - R1MI vs R3MI
%                            - R3MI vs R7MI
%                            - R1MI_vs_R7MI
%                   7. Infarcted at each time-point versus basal
%                            - I1MI vs basal
%                            - I3MI vs basal
%                            - I7MI vs basal
%                   8. Remote at each time-point versus basal
%                            - R1MI vs basal
%                            - R3MI vs basal
%                            - R7MI vs basal
%           - multi-class classification:
%                   1. Infarcted over time
%                   2. Remote over time 
%
%Input:
%               - prop_infarct1MI:  matrix of size (number of
%               samples)x(number of features) for samples belonging to a
%               particular class that is under comparison (here infarcted 1
%               day post MI)
%               - prop_remote1MI: matrix of size (number of
%               samples)x(number of features) for samples belonging to a
%               particular class that is under comparison (here remote 1
%               day post MI)
%               - prop_infarct3MI: matrix of size (number of
%               samples)x(number of features) for samples belonging to a
%               particular class that is under comparison (here infarcted 3
%               days post MI)
%               - prop_remote3MI:  matrix of size (number of
%               samples)x(number of features) for samples belonging to a
%               particular class that is under comparison (here remote 3
%               days post MI)
%               - prop_infarct7MI:   matrix of size (number of
%               samples)x(number of features) for samples belonging to a
%               particular class that is under comparison (here remote 7
%               days post MI)
%               - prop_remote7MI:   matrix of size (number of
%               samples)x(number of features) for samples belonging to a
%               particular class that is under comparison (here remote 7
%               days post MI)
%               - prop_basal:   matrix of size (number of
%               samples)x(number of features) for samples belonging to a
%               particular class that is under comparison (here basal)
%               - sma: indicating if sma is used or not in order to be used
%               for the filename of the results (SMA_comparisonPair.xls in
%               case of sma metrics used as feautures for the
%               classification or just comparisonPair.xls if sma is not
%               used)
%               - outputFolder: folder to be used by the functions called
%               by classification_all_pairs_CV in order to save the results
%               - FeatureNames: names of the features used (in the current
%               version not used)
%       
%Author: Xenia Gkontra, CNIC

%--------------------------------------------------------------------------
%               Remote versus Infarcted all time points
%--------------------------------------------------------------------------

prop_infarct=[prop_infarct1MI;prop_infarct3MI;prop_infarct7MI];
prop_remote=[prop_remote1MI;prop_remote3MI;prop_remote7MI];
classification_pairs2(prop_remote,prop_infarct,[outputFolder,sma,'Infarcted_vs_Remote'],FeatureNames);
%To avoid error while writing .xls
pause(5)

%--------------------------------------------------------------------------
%                Infarcted (all time points) vs basal 
%--------------------------------------------------------------------------

classification_pairs2(prop_basal,prop_infarct,[outputFolder,sma,'Basal_vs_Infarcted'],FeatureNames);
%To avoid error while writing .xls
pause(5)

%--------------------------------------------------------------------------
%                 Remote (all time points) vs infarcted 
%--------------------------------------------------------------------------

classification_pairs2(prop_basal,prop_remote,[outputFolder,sma,'Basal_vs_Remote'],FeatureNames);
%To avoid error while writing .xls
pause(5)

%--------------------------------------------------------------------------
%                      Infarcted over time
%--------------------------------------------------------------------------

%Cell array of size (number of samples)x1 with the classes to which the
%samples belong to
category1=size(prop_infarct1MI,1);
category2=size(prop_infarct3MI,1);
category3=size(prop_infarct7MI,1);
total=category1+category2+category3;
categories=cell(total,1);
categories(1:category1,1)={'Infarct1'};
categories(category1+1:category1+category2)={'Infarct2'};
categories(category1+category2+1:category1+category2+category3)={'Infarct3'};
%------------   1st method: Using all features
featVect=[prop_infarct1MI;prop_infarct3MI;prop_infarct7MI];
classification_pairs_C3_CV(categories,featVect,[outputFolder,sma,'Infarcted_over_time'],FeatureNames);

%--------------------------------------------------------------------------
%                    Remote over time 
%--------------------------------------------------------------------------

%Total images
category1=size(prop_remote1MI,1);
category2=size(prop_remote3MI,1);
category3=size(prop_remote7MI,1);
total=category1+category2+category3;
%Feature vector
categories=cell(total,1);
categories(1:category1,1)={'Remote1'};
categories(category1+1:category1+category2)={'Remote2'};
categories(category1+category2+1:category1+category2+category3)={'Remote3'};
%------------   1st method: Using all features
featVect=[prop_remote1MI;prop_remote3MI;prop_remote7MI];
classification_pairs_C3_CV(categories,featVect,[outputFolder,sma,'Remote_over_time'],FeatureNames);

%--------------------------------------------------------------------------
%               Remote at each time-point versus 
%           Infarcted at the corresponding time-point 
%--------------------------------------------------------------------------

%Infarcted 1 day post MI versus remote 1 day post MI
classification_pairs2(prop_infarct1MI,prop_remote1MI,[outputFolder,sma,'I1MI_vs_R1MI'],FeatureNames);
%Infarcted 3 days post MI versus remote 3 days post MI 
classification_pairs2(prop_infarct3MI,prop_remote3MI,[outputFolder,sma,'I3MI_vs_R3MI'],FeatureNames);
%Infarcted 7 days post MI versus remote 7 days post MI 
classification_pairs2(prop_infarct7MI,prop_remote7MI,[outputFolder,sma,'I7MI_vs_R7MI'],FeatureNames);

%--------------------------------------------------------------------------
%               Infracted  progressively in time
%--------------------------------------------------------------------------

%Infarcted 1 day post MI versus Infarcted 3 days post MI
classification_pairs2(prop_infarct1MI,prop_infarct3MI,[outputFolder,sma,'I1MI_vs_I3MI'],FeatureNames);
%To avoid error while writing .xls
pause(5)
%Infarcted 3 days post MI versus Infarcted 7 days post MI
classification_pairs2(prop_infarct3MI,prop_infarct7MI,[outputFolder,sma,'I3MI_vs_I7MI'],FeatureNames);
%To avoid error while writing .xls
pause(5)
%Infarcted 1 day post MI versus Infarcted 7 days post MI
classification_pairs2(prop_infarct1MI,prop_infarct7MI,[outputFolder,sma,'I1MI_vs_I7MI'],FeatureNames);
%To avoid error while writing .xls
pause(5)

%--------------------------------------------------------------------------
%                     Remote progressively in time
%--------------------------------------------------------------------------

%Remote 1 days post MI versus remote 7 days post MI
classification_pairs2(prop_remote1MI,prop_remote7MI,[outputFolder,sma,'R1MI_vs_R7MI'],FeatureNames);
%To avoid error while writing .xls
pause(5)
%Remote 1 days post MI versus remote 3 days post MI
classification_pairs2(prop_remote1MI,prop_remote3MI,[outputFolder,sma,'R1MI_vs_R3MI'],FeatureNames);
%To avoid error while writing .xls
pause(5)
%Remote 3 days post MI versus remote 7 days post MI
classification_pairs2(prop_remote3MI,prop_remote7MI,[outputFolder,sma,'R3MI_vs_R7MI'],FeatureNames);
%To avoid error while writing .xls
pause(5)

%--------------------------------------------------------------------------
%                Infracted at each time point vs basal
%--------------------------------------------------------------------------

%Infarcted at 1 days post MI vs basal
classification_pairs2(prop_infarct1MI,prop_basal,[outputFolder,sma,'I1MI_vs_basal'],FeatureNames);
%To avoid error while writing .xls
pause(5)
%Infarcted at 3 days post MI vs basal
classification_pairs2(prop_infarct3MI,prop_basal,[outputFolder,sma,'I3MI_vs_basal'],FeatureNames);
%To avoid error while writing .xls
pause(5)
%Infarcted at 7 days post MI vs basal
classification_pairs2(prop_infarct7MI,prop_basal,[outputFolder,sma,'I7MI_vs_basal'],FeatureNames);
%To avoid error while writing .xls
pause(5)

%--------------------------------------------------------------------------
%               Remote at each time-point vs basal
%--------------------------------------------------------------------------

%Remote at 1 day post MI vs basal
classification_pairs2(prop_remote1MI,prop_basal,[outputFolder,sma,'R1MI_vs_basal'],FeatureNames);
%To avoid error while writing .xls
pause(5)
%Remote at 3 days post MI vs basal
classification_pairs2(prop_remote3MI,prop_basal,[outputFolder,sma,'R3MI_vs_basal'],FeatureNames);
%To avoid error while writing .xls
pause(5)
%Remote at 7 days post MI vs basal
classification_pairs2(prop_remote7MI,prop_basal,[outputFolder,sma,'R7MI_vs_basal'],FeatureNames);
%To avoid error while writing .xls
pause(5)