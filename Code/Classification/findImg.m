function [prop_IXMI,prop_RXMI,prop_BASAL] = findImg(filenames,prop_remoteXMI,prop_infarctXMI,prop_basal,subj_remoteXMI,subj_infarctXMI,subj_basal)
%Function to expendand input matrices with the calculated metrics per image by
%adding the image name & and the subject to which the tissue for acquiring
%the image was obtained
%
%Input:
%               - filenames
%               - prop_remoteXMI: matrix of size (number of images)x(number
%               of  metrics) containing the calcuted metrics for all images
%               from remote areas. Each row corresponds to an image and
%               each column to a metric
%               - prop_infarctXMI: matrix of size (number of images)x(number
%               of  metrics) containing the calcuted metrics for all images
%               from infarcted areas. Each row corresponds to an image and
%               each column to a metric
%               - prop_basal: matrix of size (number of images)x(number
%               of  metrics) containing the calcuted metrics for all images
%               from infarcted areas. Each row corresponds to an image and
%               each column to a metric
%               - subj_remoteXMI: array of size (number of images)x1. Each
%               row contains the name (number) of the subject from which
%               was acquired the image whose metrics are saved in  the same 
%               row of prop_remoteXMI 
%               - subj_infarctXMI: array of size (number of images)x1. Each
%               row contains the name (number) of the subject from which
%               was acquired the image whose metrics are saved in  the same 
%               row of prop_remoteXMI 
%               - subj_basal: array of size (number of images)x1. Each
%               row contains the name (number) of the subject from which
%               was acquired the image whose metrics are saved in  the same 
%               row of prop_remoteXMI 
%
%Output:
%               - prop_remoteXMI: updated prop_remoteXMI by adding two columns
%               (1st and 2nd column) from which the first ones corresponds
%               to the subject from which the tissue was obtained in order
%               to acquire the image whose name is saved in the 2nd columns
%               and for which the features are in the rest of the columns
%               - prop_infarctXMI: updated prop_infarctXMI by adding two
%               columns (1st and 2nd column) from which the first ones corresponds
%               to the subject from which the tissue was obtained in order
%               to acquire the image whose name is saved in the 2nd columns
%               and for which the features are in the rest of the columns
%               - prop_basal: updated prop_basal by adding two colums
%               (1st and 2nd column) from which the first ones corresponds
%               to the subject from which the tissue was obtained in order
%               to acquire the image whose name is saved in the 2nd columns
%               and for which the features are in the rest of the columns
%
%Author: Xenia Gkontra, CNIC

temp1=1;
temp2=1;
temp3=1;
prop_IXMI=[];
prop_BASAL=[];
prop_RXMI=[];
for i=3:length(filenames)
    %Image name
    type=filenames(i).name;
    %Tissue category
    [tissueCat]=define_tissueCat(type);
    %If remote add to remote
    if ~isempty(find(strcmp(tissueCat,'Remote'),1))
        prop_RXMI=[prop_RXMI;str2num(type) subj_remoteXMI(temp1,:) prop_remoteXMI(temp1,:)];
        temp1=temp1+1;
    elseif ~isempty(find(strcmp(tissueCat,'Infarct'),1))
        prop_IXMI=[prop_IXMI;str2num(type) subj_infarctXMI(temp2,:) prop_infarctXMI(temp2,:)];
        temp2=temp2+1;
    else
        prop_BASAL=[prop_BASAL;str2num(type) subj_basal(temp3,:) prop_basal(temp3,:)];
        temp3=temp3+1;
    end
end
end

