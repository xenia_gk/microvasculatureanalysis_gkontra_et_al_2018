function []=SegmentationNucleiCore(mainDir, filename, folderRaw,raw)
%Function for loading Data (Denoised image volums, Initial MMT Segmentation
%that needs refinement), call function for refinement by the use of Nuclei
%Information and saving results
%
%Input:             - mainDir : input directory
%                   - filename: name of image under processing
%                   - folderRaw: directory for saving results in .mha
%                   format
%                   - raw: indicates whether to save the results in .mha
%                   format (raw==1) or not (raw==0)
%
%Output:           
%                   - Segmentation results are saved in [mainDir,filename]
%                   in .mat format while and results in .mha format are
%                   saved in folderRaw
%
%Hint! Please note that you can change the intensity levels used in the 
%segmentation with MMT (multiLocalThresholding.m, line 85 e.g take only the 
%highest instead of the two highest as done here) or even change the
%permitted boxes sizes (call to MMT_z.m and first lines of 
%MMT_z.m and multiLocalThresholding.m)
%
%Author: Xenia Gkontra, CNIC

%--------------------------------------------------------------------------
%           PART 1: Load data & Segment Nuclei Image (Channel0)
%--------------------------------------------------------------------------

%Load Denoised volume
load([mainDir,filename,'/extras/NL_',filename])
%Segmentation
[BWCh2]=MMT_z(Channel2,10);
save([mainDir,filename,'/extras/Segm1_',filename,'.mat'],'BWCh2')
load([mainDir,filename,'/extras/Segm1_',filename,'.mat']);
%Nuclei Segmentation
Channel0=double(Channel0-min(Channel0(:))/(max(Channel0(:))-min(Channel0(:))));

%Uncomment if you rpefer to use simple thresholding instead of MMT and
%comment line 38
%{
%-------------Segmentation of Nuclei with Simple Thresholding--------------
for sl=1:size(Channel0,3)
    BWCh0(:,:,sl)=Channel0(:,:,sl)>graythresh(Channel0(:,:,sl));
end
%}
%Segmentation of the nuceli channel by MMT
BWCh0=MMT_z(Channel0,10);
%--------------------------------------------------------------------------
%              PART 2: Refine Microvasculature Segmentation
%               Using Nuclei information & Save Results
%--------------------------------------------------------------------------

%Keep the old segmentation
BWCh1=BWCh2;
[BWCh2]=NucleiBasedRefinement(BWCh2,BWCh0);
save([mainDir,filename,'/extras/SegmMMT_',filename,'.mat'],'BWCh2','BWCh0');

%Save images in raw and save .mat
if raw==1
    SCIMAT = scimat_im2scimat(double(BWCh2+2*BWCh1));
    scirunnrrd = scimat_save([folderRaw,'/raw/BWnuclei_',filename,'.mha'],SCIMAT);
    
    SCIMAT = scimat_im2scimat(double(Channel2));
    scirunnrrd = scimat_save([folderRaw,'/raw/D_',filename,'.mha'],SCIMAT);
end
