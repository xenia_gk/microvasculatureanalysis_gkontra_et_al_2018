function [output]=NucleiBasedRefinement(input,Nuclei)
%Use nuclei information to eliminating parts that were recognized as
%vessels but that in reality they are noise based on the concept that each
%vessel should at least have one nucleus.
%Compare with nuclei image: Components that do not touch edges and don't
%have nuclei are considered as noise
%Input:     --input:    3d Initial Segmented Volume
%           --Nuclei:   3d Volumed of Segmented Nuclei
%Output:    --output: Refined segmentation of the microvasculatrue based on
%                     Nuclei information
%Author: Xenia Gkontra, CNIC, 2014


%Label image
labeled=bwlabeln(input);
props=regionprops(labeled,'Image','BoundingBox','Area','PixelList');
%Initialize
[Nx,Ny,Nz]=size(input);
output=zeros(Nx,Ny,Nz);
%Find components that touch the edge ->here replaced by large
idx=find([props.Area]>0);

for index=1:length(idx)
    i=idx(index);
    %Check whether the components touches the border
    %  if find((props(i).PixelList(:,1)~=1|props(i).PixelList(:,1)~=Ny)&(props(i).PixelList(:,2)~=1|props(i).PixelList(:,2)~=Nx)|(props(i).PixelList(:,3)~=1|props(i).PixelList(:,3)~=Nz))
    %Image containing the specific component
    Im=props(i).Image;
    %Bounding box [x y z width]
    box=round(props(i).BoundingBox);
    %Keep only microvasculature's part that contain a nucleus or are at
    %least 120 voxels large
    if (nnz(Im.*Nuclei(box(2):box(2)+box(5)-1,box(1):box(1)+box(4)-1,box(3):box(3)+box(6)-1))>0)||nnz(Im)>120
        output(box(2):box(2)+box(5)-1,box(1):box(1)+box(4)-1,box(3):box(3)+box(6)-1)=output(box(2):box(2)+box(5)-1,box(1):box(1)+box(4)-1,box(3):box(3)+box(6)-1)+Im;
    end
end

output=output>0;
