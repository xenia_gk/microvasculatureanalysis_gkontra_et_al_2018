function [BWCh]=multiLocalThresholding(Channel1,siz2)
%Function to perform local multi-thresholding using different box sizes
%
%Input:
%               -Channel1: input image
%               - siz2: size of the boxes along z-dimemnsion
%
%Output
%               -BWCh: image containing the segmentation mask
%
%Author: Xenia Gkontra, 2015

%Size of input image
[Nx,Ny,Nz]=size(Channel1);
%Box sizes for x-y
boxes=[];
p=floor(log(min(Nx,Ny))/log(2));
for i=6:p
    boxes=[boxes 2^i];
end
if (2^p)<min(Nx,Ny)
    boxes=[boxes min(Nx,Ny)];
end
%Box size along z dimension
if isempty(siz2)
    siz2=10;
end

%Adjust image intensities
ChAdj=imadjust(reshape(Channel1,Nx*Ny*Nz,1));
c=reshape(ChAdj,Nx,Ny,Nz);
%Initialize output
output=zeros(Nx,Ny,Nz);

%Iterate thresholding for different box sizes
for num=1:length(boxes)
    siz=boxes(num);
    %Calculate thresholded image regions using the box of size
    %boxes(num)xboxes(num)xsiz2
    for i=1:siz:Nx
        for j=1:siz:Ny
            for k=1:siz2:Nz
                %Check if the box is out of limits along x-direction
                if ((i+siz-1)<=Nx)
                    x2=i+siz-1;
                else
                    x2=Nx;
                end
                %Check if the box is out of limits along y-direction
                if ((j+siz-1)<=Ny)
                    y2=j+siz-1;
                else
                    y2=Ny;
                end
                %Check if the box is out of limits along z-direction
                if ((k+siz2-1)<=Nz)
                    z2=k+siz2-1;
                else
                    z2=Nz;
                end
                
                %Copy the part of the volume inside the box under
                %investigation
                temp=c(i:x2,j:y2,k:z2);
                %Perform multi-level thresholding to separate the image in
                %four internsity classes so that the inter-class variance
                %is minimized
                try
                    N=4;
                    t1=multithresh(temp,N);
                catch err
                    try
                        fprintf('3 levels are used instead of 4')
                        N=3;
                        t1=multithresh(temp,N);
                    catch err
                        fprintf('2 levels are used instead of 3')
                        N=2;
                        t1=multithresh(temp,N);
                    end
                end
                %Apply calculated threshold
                im=imquantize(temp,t1);
                %Keep the two higher intensity classes
                BW=(im>=N);
                %Output for the specific image region
                output(i:x2,j:y2,k:z2,num)=BW;
            end
        end
    end
end

y=zeros(Nx,Ny,Nz);
for i=1:length(boxes)
    y(:,:,:)=y(:,:,:)+output(:,:,:,i);
end
%Find mean
y=y/length(boxes);
%Keep only those with probability >0.5
BWCh=y>0.5;
