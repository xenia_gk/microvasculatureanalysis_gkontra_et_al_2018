%--------------------------------------------------------------------------
%Function to perform segmentation of the VE-Cadherin channel by
%means of the multi-scale multi-level thresholding algorithm (MMT) as well as
%to further refine the segmentation using the segmentation of the nuclei
%channel.
%
%Input: 
%               - .mat file with denoised channels (particularly Channels
%               1,2 which correspond VE-Cadherin and nuclei channels is 
%               needed(NL_imageName.mat)
%
%Output: All results, ie segmentation of VE-Cadherin channel  by MMT before
%refinement -as well as the segmentation of the nuclei
%channel(Segm1_imageName)- and after refinement by nuclei information
%(Segm_MMT_imageName) are saved at the folderOut (please see below)
%
%Hind! Please note that you can change the intensity levels used in the 
%segmentation with MMT (multiLocalThresholding.m, line 85 e.g take only the 
%highest instead of the two highest as done here) or even change the
%permitted boxes sizes (call to MMT_z.m and first lines of 
%MMT_z.m and multiLocalThresholding.m)
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clear;
close all;
%--------------------------------------------------------------------------
%                   Directories
%--------------------------------------------------------------------------
%Directory for loading the denoised images
directories=set_directories(GetFullPath('..'));

%Input directory where the denoised 3d images of the channels are saved
mainDir=directories.mainDir;

%Load time-points after MI to be stadied. Please note first two folders are
%'.' and '..' so we ignore them
daysMI_All=dir(mainDir);

%Output folder; the same as the input as segmentations will be saved in the
%same folder as the denoised results
folderOut=directories.mainDir;


%--------------------------------------------------------------------------
%                       Parameters
%--------------------------------------------------------------------------

%Indicates whether to save .mha images or not (saves .mha only  if raw=1)
raw=0;

%--------------------------------------------------------------------------
%                          Segmentation
%--------------------------------------------------------------------------

for j=3:length(daysMI_All)
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    %Input directory
    directoryIn=[mainDir,'/',daysMI,'/'];
    %Find images in the directory
    filenames=dir(directoryIn);
    %Output directory
    dirOut=[folderOut,'/',daysMI,'/'];
    
    %Images files for 1MI
    for i=3:length(filenames)
        %Output directory
        folderRaw=[dirOut,'/',filenames(i).name];
        %Check whether the folder exist
        if ~exist(folderRaw)
            mkdir(folderRaw)
        end
        %VE-Cadherin Segmentation by MMT and refined by Nuclei
        SegmentationNucleiCore(directoryIn, filenames(i).name, folderRaw,raw)
    end
end

