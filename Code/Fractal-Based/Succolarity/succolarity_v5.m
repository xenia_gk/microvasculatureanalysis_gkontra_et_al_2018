function [suc_dir1,boxSize]=succolarity_v5(Im_dir,direction)
%Calculate succolarity of an image at a given dirrection using boxcount
%method and papers
%Melo et al., 2011, Diaye et al., 2013
%Modified code from F. Moisy's boxcount code (only itinialization aprt was kept)
%for the calculation of the fractal dimension
%
%Input:            
%           - Img: input image for calculating succolarity 
%           - direction: direction at which the fluid is flooding the image
%           , possible values:
%                       1 : from right to left
%                       2 : from left to right
%                       3 : from up to down
%                       4 : from down to up
%                       5 : from bottom to top
%                       6 : from top to bottom
%
%Output:     
%           - suc: succolarity values
%           - boxSize: sizes of boxes used for the calculation of succolarity
%
%Author: Xenia Gkonta


%--------------------------------------------------------------------------
%   Calculate box sizes and pad image
%--------------------------------------------------------------------------
%Adapted this part from fd
width = max(size(Im_dir));    % largest size of the box
p = log(width)/log(2);   % nbre of generations
if size(Im_dir,3)>1
    dim=3;
else
    if size(Im_dir,2)>1
        dim=2;
    else
        dim=1;
    end
end
% remap the array if the sizes are not all equal,
% or if they are not power of two
% (this slows down the computation!)
if p~=round(p) || any(size(Im_dir)~=width)
    p = ceil(p);
end

if p~=round(p) || any(size(Im_dir)~=width)
    p = ceil(p);
    width = 2^p;
    switch dim
        case 1
            mz = zeros(1,width);
            mz(1:length(Im_dir)) = Im_dir;
            Im_dir = mz;
            
        case 2
            mz = zeros(width, width);
            mz(1:size(Im_dir,1), 1:size(Im_dir,2)) = Im_dir;
            Im_dir = mz;
        case 3
            mz = zeros(width, width, width);
            mz(1:size(Im_dir,1), 1:size(Im_dir,2), 1:size(Im_dir,3)) = Im_dir;
            Im_dir = mz;
            
    end
end

%---------------------------------------------------------------------------
%                           3D boxcount
%---------------------------------------------------------------------------
suc_dir1 = zeros(1,p);

switch direction
    case 1
        %Direction 1
        Pr1=zeros(size(Im_dir));
        Im=repmat([1:width]',1,width)-0.5;
        for sl=1:size(Im_dir,3)
            Pr1(:,:,sl)=Im;
        end
    case 2
        %Direction 2
        Pr1=zeros(size(Im_dir));
        Im=size(Im_dir,1)-repmat([1:width]',1,width)+0.5;
        for sl=1:size(Im_dir,3)
            Pr1(:,:,sl)=Im;
        end
    case 3
        %Direction 3
        Pr1=zeros(size(Im_dir));
        Im=repmat([1:width],width,1)-0.5;
        for sl=1:size(Im_dir,3)
            Pr1(:,:,sl)=Im;
        end
    case 4
        %Direction 4
        Pr1=zeros(size(Im_dir));
        Im=size(Im_dir,2)-repmat([1:width],width,1)+0.5;
        for sl=1:size(Im_dir,3)
            Pr1(:,:,sl)=Im;
        end
    case 5
        %Direction 5
        Pr1=zeros(size(Im_dir));
        for sl=1:size(Im_dir,3)
            Pr1(:,:,sl)=sl-0.5;
        end
    case 6
        %Direction 6
        Pr1=zeros(size(Im_dir));
        for sl=1:size(Im_dir,3)
            Pr1(:,:,sl)=size(Im_dir,3)-sl+0.5;
        end
end

c_dir1=zeros(1,p+1);

suc_dir1(1) = sum(sum(sum(Im_dir.*Pr1)))/sum(Pr1(:));

boxSize=1;
switch direction
    %------------   Direction -> columns: from 0 towards Ny
    case 1
        for g=1:p
            siz = 2^g;
            siz2 = siz-1;
            boxSize=[boxSize siz];
            Pr1=[];
            for i=1:siz:(width-siz+1),
                for j=1:siz:(width-siz+1),
                    for k=1:siz:(width-siz+1)
                        temp=sum([i:i+siz2])/(length([i:i+siz2]))-0.5;
                        Pr1=[Pr1;temp];
                        % Pr1(i,j,k)=
                        c_dir1(1,g+1)=c_dir1(1,g+1)+sum(sum(sum(Im_dir(i:i+siz2,j:j+siz2,k:k+siz2))))/(siz*siz*siz).*temp;
                        
                    end
                end
            end
            sum_dir1=sum(Pr1(:));
            suc_dir1(1,g+1)=c_dir1(1,g+1)/sum_dir1;
        end
    case 2
        for g=1:p
            siz = 2^g;
            siz2 = siz-1;
            boxSize=[boxSize siz];
            Pr1=[];
            for i=1:siz:(width-siz+1),
                for j=1:siz:(width-siz+1),
                    for k=1:siz:(width-siz+1)
                        temp=width-sum([i:i+siz2])/(length([i:i+siz2]))+0.5;
                        Pr1=[Pr1;temp];
                        c_dir1(1,g+1)=c_dir1(1,g+1)+sum(sum(sum(Im_dir(i:i+siz2,j:j+siz2,k:k+siz2))))/(siz*siz*siz).*temp;
                    end
                end
            end
            sum_dir1=sum(Pr1(:));
            suc_dir1(1,g+1)=c_dir1(1,g+1)/sum_dir1;
        end
        %------------   Direction 3-> columns: from 0 towards Ny
    case 3
        for g=1:p
            siz = 2^g;
            siz2 = siz-1;
            boxSize=[boxSize siz];
            Pr1=[];
            for i=1:siz:(width-siz+1),
                for j=1:siz:(width-siz+1),
                    for k=1:siz:(width-siz+1)
                        temp=sum([j:j+siz2])/(length([j:j+siz2]))-0.5;
                        Pr1=[Pr1;temp];
                        c_dir1(1,g+1)=c_dir1(1,g+1)+sum(sum(sum(Im_dir(i:i+siz2,j:j+siz2,k:k+siz2))))/(siz*siz*siz).*temp;
                    end
                end
            end
            sum_dir1=sum(Pr1(:));
            suc_dir1(1,g+1)=c_dir1(1,g+1)/sum_dir1;
        end
        %------------   Direction 4 -> rows: rows: from Nx towards 0
    case 4
        for g=1:p
            siz = 2^g;
            siz2 = siz-1;
            boxSize=[boxSize siz];
            Pr1=[];
            for i=1:siz:(width-siz+1),
                for j=1:siz:(width-siz+1),
                    for k=1:siz:(width-siz+1)
                        temp=width-sum([j:j+siz2])/(length([j:j+siz2]))+0.5;
                        Pr1=[Pr1;temp];
                        c_dir1(1,g+1)=c_dir1(1,g+1)+sum(sum(sum(Im_dir(i:i+siz2,j:j+siz2,k:k+siz2))))/(siz*siz*siz).*temp;
                    end
                end
            end
            sum_dir1=sum(Pr1(:));
            suc_dir1(1,g+1)=c_dir1(1,g+1)/sum_dir1;
        end
        % ---------------     Direction 5 -> slices: from 0 towrds Nz
    case 5
        for g=1:p
            siz = 2^g;
            siz2 = siz-1;
            boxSize=[boxSize siz];
            Pr1=[];
            for i=1:siz:(width-siz+1),
                for j=1:siz:(width-siz+1),
                    for k=1:siz:(width-siz+1)
                        temp=sum([k:k+siz2])/(length([k:k+siz2]))-0.5;
                        Pr1=[Pr1;temp];
                        c_dir1(1,g+1)=c_dir1(1,g+1)+sum(sum(sum(Im_dir(i:i+siz2,j:j+siz2,k:k+siz2))))/(siz*siz*siz).*temp;
                    end
                end
            end
            sum_dir1=sum(Pr1(:));
            suc_dir1(1,g+1)=c_dir1(1,g+1)/sum_dir1;
        end
        %---------------     Direction 6 -> slices: from Nz towrds 0  end
        
    case 6
        for g=1:p
            siz = 2^g;
            siz2 = siz-1;
            boxSize=[boxSize siz];
            Pr1=[];
            for i=1:siz:(width-siz+1),
                for j=1:siz:(width-siz+1),
                    for k=1:siz:(width-siz+1)
                        temp=width-sum([k:k+siz2])/(length([k:k+siz2]))+0.5;
                        Pr1=[Pr1;temp];
                        %Direction 2.1: -> rows: from Nx towards 0
                        c_dir1(1,g+1)=c_dir1(1,g+1)+sum(sum(sum(Im_dir(i:i+siz2,j:j+siz2,k:k+siz2))))/(siz*siz*siz).*temp;
                    end
                end
            end
            sum_dir1=sum(Pr1(:));
            suc_dir1(1,g+1)=c_dir1(1,g+1)/sum_dir1;
        end
end


