%--------------------------------------------------------------------------
%Main function for calculating succolarity.
%
%Input:
%           - It is expected that the microvasculature has already been
%           reconstructed (BW)
%
%Output:
%           - mat. files (succolarity2_imageName.mat) that contains succolarity
%           along every direction (6 in total), corresponding volume used
%           for calculation, box size used and mean succolarity along all
%           directions
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clc
clear;
close all;

%--------------------------------------------------------------------------
%                   Directories
%--------------------------------------------------------------------------

%Call function to get the paths to be used by the module
directories=set_directories(GetFullPath('../..'));

%Directory to save the results
dirOut=directories.mainDir_Fractals;
%Directory where the VE-Cadherin segmentations after the application of teh
%filling algorithm can be found
dirIn=directories.mainDir_Filled;
%The images in this directory are not ordered by tissue category
mainDir=directories.mainDir;
%List time-points after MI to be studied. Please note first two folders
%returned by Matlab's "dir" function are '.' and '..' so we ignore them
daysMI_All=dir(mainDir);

%--------------------------------------------------------------------------
%               Calculate succolarity
%--------------------------------------------------------------------------

for j=3:length(daysMI_All)
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    filelist=dir([mainDir,daysMI]);
    
    for i=3:length(filelist)
        %Name of image used for loading and saving results
        type=filelist(i).name;
        %Tissue condition
        tissue_cat=define_tissueCat(type);
        %Directory for saving the results
        directoryOut=[dirOut,daysMI,'/',tissue_cat,'/',type,'/'];
        if ~exist(directoryOut)
            mkdir(directoryOut);
        end
        %Input directory
        directoryIn=[dirIn,daysMI,'/',tissue_cat,'/',type,'/'];
        data=load([directoryIn,'BW_',type,'.mat']);
        BWCh2=data.BW;
        tic
        calculateSuccolarity(BWCh2,directoryOut,type)
        toc
    end
    
end
