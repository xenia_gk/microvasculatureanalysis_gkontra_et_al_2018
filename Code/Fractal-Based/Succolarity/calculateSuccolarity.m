function []=calculateSuccolarity(FlowIm,directoryOut,type)
%Function for calculating succolarity of a 2d Image or a 3d volume based
%on papers: Melo et al., 2011, Diaye et al., 2013
%
%Input:            
%               - FlowIm: image with regions where the fluid can pass 1
%                and obstacles with 0
%               - directoryOut: directory to save the results
%               - type: name of image
%
% Author: Xenia Gkontra, 


%Size & dimensions
[Nx,Ny,Nz]=size(FlowIm);
dims=ndims(FlowIm);

%--------------------------------------------------------------------------
%       STEP 1: Find connected components in the Flow Image
%--------------------------------------------------------------------------
if size(FlowIm,3)>1
    Comps=bwlabeln(FlowIm,18);
else
    Comps=bwlabeln(FlowIm,4);
end
props=regionprops(Comps,'PixelList');
%Initialize image with on voxels according to the fluiding direction
Im_dir1=zeros(size(FlowIm));
Im_dir2=zeros(size(FlowIm));
Im_dir3=zeros(size(FlowIm));
Im_dir4=zeros(size(FlowIm));
Im_dir5=zeros(size(FlowIm));
Im_dir6=zeros(size(FlowIm));
suc_dir1=[];
suc_dir2=[];
suc_dir3=[];
suc_dir4=[];
suc_dir5=[];
suc_dir6=[];
boxSize1=[];
boxSize2=[];
boxSize3=[];
boxSize4=[];
boxSize5=[];
boxSize6=[];


%--------------------------------------------------------------------------
%           STEP 1: Create images with fluid flow per direction
%--------------------------------------------------------------------------
%matlabpool open 10
%Find common elements of each component with the each side
for i=1:length(props)
    
    %Direction 1: vertical direction from start to end (rows: from 0 towards Nx)
    if ~isempty(find(props(i).PixelList(:,1)==1, 1))
        Im_dir1=Im_dir1+double(Comps==i) ;
    end
    %Direction 2: vertical direction from end to start (rows: from Nx towards 0)
    if ~isempty(find(props(i).PixelList(:,1)==Nx, 1))
        Im_dir2=Im_dir2+double(Comps==i) ;
    end
    %Direction 2: horizontal direction from left to right (columns: from 0 towards Ny)
    if ~isempty(find(props(i).PixelList(:,2)==1, 1))
        Im_dir3=Im_dir3+double(Comps==i) ;
    end
    %Direction 3: horizontal direction from right to left (colums: from Ny towards 0)
    if ~isempty(find(props(i).PixelList(:,2)==Nx, 1))
        Im_dir4=Im_dir4+double(Comps==i) ;
    end
    % If the image is a volume
    %Direction 5: from upper slice to bottom (slices: from 0 towrds Nz)
    if ~isempty(find(props(i).PixelList(:,3)==1, 1))
        Im_dir5=Im_dir5+double(Comps==i) ;
    end
    %Direction 6: from lower to upper slice (slices: from Nz towards 0)
    if ~isempty(find(props(i).PixelList(:,3)==Nz, 1))
        Im_dir6=Im_dir6+double(Comps==i) ;
    end
end
%--------------------------------------------------------------------------
%         STEP 2: Calculate succolarity per direction
%                       & mean Succolarity
%--------------------------------------------------------------------------

[suc_dir1,boxSize]=succolarity_v5(Im_dir1,3);
[suc_dir2,boxSize]=succolarity_v5(Im_dir2,4);
[suc_dir3,boxSize]=succolarity_v5(Im_dir3,1);
[suc_dir4,boxSize]=succolarity_v5(Im_dir4,2);

[suc_dir5,boxSize]=succolarity_v5(Im_dir5,5);
[suc_dir6,boxSize]=succolarity_v5(Im_dir6,6);
suc_All=(suc_dir1+suc_dir2+suc_dir3+suc_dir4+suc_dir5+suc_dir6)/6;

%--------------------------------------------------------------------------
%                       STEP 3: Save Results
%--------------------------------------------------------------------------
save([directoryOut,'/succolarity2_',type,'.mat'],'Im_dir3','Im_dir4','Im_dir1','Im_dir2','Im_dir5','Im_dir6','suc_dir1','suc_dir2','suc_dir3','suc_dir4','suc_dir5','suc_dir6','boxSize','suc_All');
%delete(gcp)
%matlabpool close
clear Im_dir3 Im_dir4 Im_dir1 Im_dir2 Im_dir5 Im_dir6;