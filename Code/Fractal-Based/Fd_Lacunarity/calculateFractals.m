function [parameters] = calculateFractals(dirIn,dirOut,type)
%AnalysisPipeline: Image analysis of the vasculature - measures that are
%characterizing the vasculature as a whole
%Input:         -dirIn: directory where the original images are
%               -dirOut: directory where the results should be saved
%               type: name of image
%               -folderExtras: directory where the volume is saved
%               -voxelSize: size of the voxel
%
%Output:        Params_Complete_2.mat to be saved in folderExtras. The .mat
%file includes the calculated Fractal diamension and Lacunarity 
%Parameters to be quantified:   1. Fractal based measures: Fractal
%                                  Dimension and Lacunarity
%
%Author: Xenia Gkontra, CNIC, 2014


%--------------------------------------------------------------------------
%                 STEP 2:  Load filled images
%--------------------------------------------------------------------------
data=load([dirIn,'BW_',type,'.mat']);
BWCh2=data.BW;

%--------------------------------------------------------------------------
%               STEP 3: Quantification of parameters
%--------------------------------------------------------------------------
%Process the whole volume
[parameters5] = fractalAnalysis_v2(BWCh2,1,size(BWCh2,1),1,size(BWCh2,2));
save([dirOut,'/Params_Complete_2_',type,'.mat'],'parameters5')
parameters=[];


