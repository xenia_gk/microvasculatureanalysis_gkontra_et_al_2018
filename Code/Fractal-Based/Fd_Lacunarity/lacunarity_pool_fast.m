function [LC] = lacunarity_pool_fast(c)
%#codegen
%--------------------------------------------------------------------------
% lacunarity: Gliding box lacunarity algorithm based on the ideas presented
%  in the paper by Tolle et al., Physica D, 237, 306-315, 2008
% input is the binary file with 0's and 1's. 0's represent the holes or the
% lacunae. The output LC gives the lacunarity at various box lengths
% (edge sizes are multiples of 2)
%
%   Usage: LC = lacunarity(c)
%
%--------------------------------------------------------------------------
% Author: Tegy J. Vadakkan
% Date: 09/08/2009
%--------------------------------------------------------------------------
% Jun Li
% Date: 2013/12/16
% Revision: rewrite for 3D and efficiency
%--------------------------------------------------------------------------
%Small changes & added parfor to gain speed Xenia Gkontra
%--------------------------------------------------------------------------
%License for this file as redisrtibuted by Jun Li in Code/Programs/Lacunarity_license 

[rows, cols, hgts] = size(c);
p = floor(log(min([rows, cols, hgts]))/log(2));


count = zeros(1, p);
sigma = zeros(1,p);
sigma2 = zeros(1,p);
LC = zeros(1, p);

%parpool(2)
% fprintf('Total iteration is [%d]\n', p);
parfor index = 1:p
    %    fprintf('Iteration [%d]\n', index);
    n = 2^index;
    rnn = rows - n + 1;
    cnn = cols - n + 1;
    hnn = hgts - n + 1;
    count(index)= rnn * cnn * hnn;
    
    for i=1:rnn
        %       fprintf('Iteration [%d.%d]\n', index, i);
        i1 = (i+n-1);
        for j=1:cnn
            j1 = j+n-1;
            for k = 1:hnn
                k1 = k+n-1;
                x=c(i:i1,j:j1, k:k1);
                sums = sum(x(:));
                sigma(index) = sigma(index) + sums;
                sigma2(index) = sigma2(index) + power(sums,2);
              %  zeros1(index)=zeros1(index)+length(find(c(i:i1,j:j1, k:k1))==0);
            end
        end
    end
end

parfor i=1:p
    LC(i)= (count(i)*sigma2(i))/(power(sigma(i),2));
end

%toc
end