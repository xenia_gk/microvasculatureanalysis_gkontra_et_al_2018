function [parameters] = fractalAnalysis_v2(Tie2Vol,x1,x2,y1,y2)
%Fractal analysis: Calculates Fractal Dimension and Lacunarity (volume and
%corresponding
%Input:
%               -Tie2Vol: Volume to be processed
%               -x1,x2,y1,y2: indexes to cut the volume
%
%Output:            
%               -parameters: structure to be used to save fractal dimension
%               and lacunarity and the corresponding indertmediate results
%
%Xenia Gkontra, CNIC, 2014

%Crop to the specified region
Vol=Tie2Vol(x1:x2,y1:y2,:);
[Nx,Ny,Nz]=size(Vol);

%--------------------------------------------------------------------------
%                       Fractal Dimension
%--------------------------------------------------------------------------
%Box-xounting method
[n1,r1]=boxcount(Vol);
x=log(r1);
y=log(n1);
%Fit line using least squares
[p,S] = polyfit(x,y,1);
%[yd,delta]=polyval(p,x,S);
a1 = p(2);
%Slope ->its negative corresponds to the fractal dimension
a2 = p(1);
fit = a1+a2*x;


%--------------------------------------------------------------------------
%                          Lacunarity
%--------------------------------------------------------------------------
%Original volume
[LC] = lacunarity_pool_fast(Vol);
%Complementary volume
[cLC] = lacunarity_pool_fast(1-Vol);
parameters.n1=n1;
parameters.r1=r1;
parameters.Df=-a2;
parameters.Dens=sum(Vol(:))*100/(Nx*Ny*Nz);
parameters.LC=LC;
parameters.cLC=cLC;


L=2-(1./LC+1./cLC);


parameters.L=L;


end

