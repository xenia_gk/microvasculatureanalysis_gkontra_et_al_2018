%--------------------------------------------------------------------------
%Main function for calcualting fractal dimension and lacunarity.
%
%Input: 
%           - It is expected that the microvasculature has already been
%           reconstructed (BW)
%
%Output:
%           - .mat files (Params_Complete_2_imageName.mat) saved in dirOut 
%           that contains fractal dimension, lacunarity, complementary 
%           lacunarity, scales, number of boxes per scale and scales
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clc
clear;
close all

%--------------------------------------------------------------------------
%                   Directories
%--------------------------------------------------------------------------
%Call function to get the paths to be used by the module
directories=set_directories(GetFullPath('../..'));

%Directory to save the results
dirOut=directories.mainDir_Fractals;
%Directory where the VE-Cadherin segmentations after the application of teh
%filling algorithm can be found
dirIn=directories.mainDir_Filled;
%The images in this directory are not ordered by tissue category
mainDir=directories.mainDir;
%List time-points after MI to be studied. Please note first two folders
%returned by Matlab's "dir" function are '.' and '..' so we ignore them
daysMI_All=dir(mainDir);

%--------------------------------------------------------------------------
%               Calculate fractal dimension & laccunarity
%--------------------------------------------------------------------------

for j=3:length(daysMI_All)
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    filelist=dir([mainDir,daysMI]);
    
    for i=3:length(filelist)
        %Name of image used for loading and saving results
        type=filelist(i).name;
        %Tissue condition: 'Remote','Infarct','Basal'
        tissue_cat=define_tissueCat(type);
        %Directory for saving the results for the current image
        directoryOut=[dirOut,daysMI,'/',tissue_cat,'/',type,'/'];
        if ~exist(directoryOut)
            mkdir(directoryOut);
        end
        directoryFilled=[dirIn,daysMI,'/',tissue_cat,'/',type,'/'];
        [params_Inf] = calculateFractals(directoryFilled,directoryOut,type);
    end
    
end