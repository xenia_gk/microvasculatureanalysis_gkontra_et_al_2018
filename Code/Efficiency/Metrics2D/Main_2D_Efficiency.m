%--------------------------------------------------------------------------
%Main function to call for calculating the capillary density and
%interrcapillary distance in 2D
%
%Output:
%               - .mat files with filename ExtraMetrics_imageName.mat (if
%               all the microvasculature, ie arterioles, venules,
%               capillaries are taken into account) or
%               ExtraMetrics_B_imageName.mat (if the parameters are
%               calculated after excluding arterioles/venules) are saved in
%               outputFolder
%               - Plots of the area of the slice and the slice used for the
%                calculations of the parameters with its borders marked up.
%                are saved in outputFolder
%               - If the corners/borders of teh slice area used for
%               calculation of the metrics are not calculated property an
%               image is saved in outputFolder to demonstrate where the
%               corners were found
%
%Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clc
clear;

%Define whether arterioles should be excluded (1) or not (0) for the
%calculation of capillary density and intercapillary distance
artExcluded=1;
%Define whether you would like to save an image showing the slice that was
%used to calcualte the parameters
visualis=0;

%--------------------------------------------------------------------------
%                   Input & Output Directories
%--------------------------------------------------------------------------
%Call function to get the paths to be used by the module
directories=set_directories(GetFullPath('../..'));

%Directory for taking the names of all images pre time-point independently
%from tissue condition remote or infarct
dirIn=directories.mainDir;
%Directory where the microvasculature after filling is saved
inputFolder=directories.mainDir_Filled;
%Directory where 3D volume with possible arterioles/venules areas are saved
arteriolesFolder=directories.mainDir_Art;
%Directory for saving the results
outputFolder=directories.mainDir_2D;
%List time-points after MI to be studied. Please note first two folders
%listed by dir are '.' and '..' so we ignore them in the next steps
daysMI_All=dir(dirIn);

%--------------------------------------------------------------------------
%               Capillary Density & intercapillary distance
%--------------------------------------------------------------------------


for j=3:length(daysMI_All)
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    filelist=dir([dirIn,daysMI]);
    for i=3:length(filelist)
        %Name of image used for loading and saving results
        type=filelist(i).name;
        %Tissue category
        [tissueCategory]=define_tissueCat(type);
        %Directory for saving the results
        directoryOut=[outputFolder,daysMI,'/',tissueCategory,'/',type,'/'];
        if ~exist(directoryOut,'dir')
            mkdir(directoryOut);
        end
        %Load microvasculature
        folderExtras=[inputFolder,daysMI,'/',tissueCategory,'/',type,'/'];
        load([folderExtras,'BW_',type,'.mat'])
        [voxelSize,Voxel_volume]=define_voxelSize(type);
        %Output filename
        outName='ExtraMetrics_';
        %If arterioles should be excluded
        if artExcluded==1
            %Load arterioles Segmentation
            dataArt=load([arteriolesFolder,daysMI,'/',tissueCategory,'/',type,'/Arterioles_',type,'.mat']);
            arterioles=dataArt.Vfiltered>0;
            %Only the capillaries region is taken into account
            BW=BW.*(1-arterioles);
            %Add an A to the filename to discriminate that the calculation has
            %been date excluding arterioles
            outName=[outName,'B_'];
        end
        tic
        capillary_density(directoryOut,BW,voxelSize,visualis,type,outName)
        toc
    end
end
