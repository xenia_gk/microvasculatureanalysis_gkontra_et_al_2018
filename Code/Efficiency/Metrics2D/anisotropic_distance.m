function [dist]=anisotropic_distance(point1,point2,voxelSize)
%Function to calculate euclidean distance between 3D points when the
%voxelSize is not isotropic 
%
%Input: 
%               - point1: 1x3 array conatining the coordinates of point1
%               - point1: 1x3 array conatining the coordinates of point1
%               - voxelSize: size of voxel along x,y,z axis in microns, ie image
%               resolution
%Output:
%               - dista: calculated euclidean distance in microns
%
%Author: Xenia Gkontra, CNIC


dist=sqrt(((point1(1)-point2(1))*voxelSize(1))^2+((point1(2)-point2(2))*voxelSize(2))^2+((point1(3)-point2(3))*voxelSize(3))^2);