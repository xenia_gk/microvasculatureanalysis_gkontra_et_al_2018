function [Ures,ellipsoid,labels] = directionAngles(img, varargin)
%Find direction cosines
%Slightly modified from https://uk.mathworks.com/matlabcentral/fileexchange/34104-image-ellipsoid-3d
%included in Code/Programs/


% size of image
dim = size(img);

% extract spatial calibration
scales = [1 1 1];
if ~isempty(varargin)
    scales = varargin{1};
end

% extract the set of labels, and remove label for background
labels = unique(img(:));
labels(labels==0) = [];

nLabels = length(labels);

% allocate memory for result
ellipsoid = zeros(nLabels, 9);
s = struct('U',cell(1));
Ures= repmat(s,nLabels,1);
for i = 1:nLabels
    % extract points of the current particle
    inds = find(img==labels(i));
    length(inds)
    [y,x,z] = ind2sub(dim, inds);
    %Keep a copy of the coordinates in order to calculate euclidean
    %distance later
    yy=y;
    xx=x;
    zz=z;
   
    % number of points
    n = length(inds);
    
    % compute approximate location of ellipsoid center
    xc = mean(x);
    yc = mean(y);
    zc = mean(z);
    
    center = [xc yc zc] .* scales;
    
    % recenter points (should be better for numerical accuracy)
    x = (x - xc) * scales(1);
    y = (y - yc) * scales(2);
    z = (z - zc) * scales(3);
    
    points = [x y z];
    
    % compute the covariance matrix
    covPts = cov(points) / n;
    
    % perform a principal component analysis with 2 variables,
    % to extract inertia axes
    [U,S] = svd(covPts);
    
    % extract length of each semi axis
    radii = 2 * sqrt(diag(S)*n)';
    
    % sort axes from greater to lower
    [radii,ind] = sort(radii, 'descend');
    
    % format U to ensure first axis points to positive x direction
    U = U(ind, :);
    if U(1,1) < 0
        U = -U;
        % keep matrix determinant positive
        U(:,3) = -U(:,3);
    end
    Ures(i).U=U;
    % convert axes rotation matrix to Euler angles
    angles = rotation3dToEulerAngles(U);

    % concatenate result to form an ellipsoid object
    ellipsoid(i, :) = [center radii angles];
    
end