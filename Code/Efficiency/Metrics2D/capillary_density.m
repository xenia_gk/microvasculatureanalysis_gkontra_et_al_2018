function []=capillary_density(outputFolder,BW,voxelSize,vis,name,outName)
%Function to calculate capillary distance &intercapillary distance
%The metrics are calculated in 5 slices around the mean one
%
%Input:
%               - outputFolder: folder where to save the calculated metrics
%               and plots
%               - BW: binary volume for which the intercapilalry distance will be
%               calculated
%               - voxelSize: voxel size of image
%               - vis: create plots for visualization of closest capillary
%               - name: image name used for naming the output
%               - outName: main name used for output
%               - method: if methdo is set to 2 then the second method of
%               distance transform is used
%Output:
%               - .mat file including the calculated capilalry density,
%               capillary density ration,  intercapillary distance, area of
%               the slice used for the calculation in microns and in pixels
%               and capillary number. The .mat files are saved in outputFolder
%
%Author: Xenia Gkontra, CNIC


img=BW;
%Volume size
[Nx,Ny,Nz]=size(img);

%Main Orientation for the vasculature
[Ures,ellipsoid,labels] = directionAngles(img);
xCenter=ellipsoid(1,1);
yCenter=ellipsoid(1,2);
zCenter=ellipsoid(1,3);


%--------------------------------------------------------------------------
%               Slices perpendicular to the main directions
%--------------------------------------------------------------------------

%Calculate perpendicular to main direction
x_P1=-Ures.U(2,1);
y_P1=Ures.U(1,1);
z_P1=Ures.U(3,1);
clear capillaries_Number;
count=1;
ImgSlice1_P=zeros(Nx,Ny,Nz);
%Repeat the calculation for 5 slices around the one in the middle for saver
%conclusions
for sliceNumIndex=1:5
    sliceNum=xCenter+40*(sliceNumIndex-3);
    s{sliceNumIndex}=sliceNum;
    %Main direction of vessels (longitudinal capillaries): Extract slice perpendicular to the main direction
    [slice1_P, sliceInd1_P, subX1_P, subY1_P, subZ1_P] = extractSlice(img,sliceNum,yCenter,zCenter,x_P1,y_P1,z_P1,Nx);
    imshow(slice1_P,[]);
    
    %Uncomment if you wish to visualize but you can't use the parfor - use for
    %instead
    %{
    if vis==1
        %Create a volume with voxels set to 1 on the
        %perpedicular slice
        
        %Find non nan voxels
        indx1_P=find(~isnan(sliceInd1_P));
        for i=1:length(indx1_P)
            %Voxel coordinates
            [x,y,z]=ind2sub([Nx,Ny,Nz],sliceInd1_P(indx1_P(i)));
            ImgSlice1_P(x,y,z)=1;
        end
    end
    %}
    
    %--------------------------------------------------------------------------
    %           Calculate capillary density 2d slice
    %--------------------------------------------------------------------------
    %Calculate region in pixels
    region(sliceNumIndex,1)=length(find(~isnan(sliceInd1_P)));
    slice1_P(find(isnan(sliceInd1_P)))=0;
    labeled=bwlabeln(slice1_P);
    %Calculate capillary number
    capillaries_Number(sliceNumIndex,1)=length(unique(labeled))-1;
    %Calculate area in microns^2
    %Create image that contains only the non-nan area
    slice_Area=~isnan(sliceInd1_P);
    %Find extrema pixels & the bounding box to calculate width and
    %
    %[1.top-left 2.top-right 3.right-top 4.right-bottom 5.bottom-right 6.bottom-left 7.left-bottom 8.left-top]
    %Due to the way extrema points are calculated, it can lead to extrema top-left same as
    %right-top are the same etc, so the bounding box as well is used to
    %identify upper-left, upper-right, lower-left lower-right
    propsSlice=regionprops(bwlabel(slice_Area),'Extrema','BoundingBox');
    ulBox=propsSlice(1).BoundingBox;
    extrema=propsSlice(1).Extrema;
    %Rge four corners of the slice
    co=corner(slice_Area,4);
    point1_xy=[co(1,2) co(1,1)];
    point1_xyz=[subX1_P(round(point1_xy(1)),round(point1_xy(2))),subY1_P(round(point1_xy(1)),round(point1_xy(2))),subZ1_P(round(point1_xy(1)),round(point1_xy(2)))];
    point2_xy=[co(2,2) co(2,1)];
    point2_xyz=[subX1_P(round(point2_xy(1)),round(point2_xy(2))),subY1_P(round(point2_xy(1)),round(point2_xy(2))),subZ1_P(round(point2_xy(1)),round(point2_xy(2)))];
    point3_xy=[co(3,2) co(3,1)];
    point3_xyz=[subX1_P(round(point3_xy(1)),round(point3_xy(2))),subY1_P(round(point3_xy(1)),round(point3_xy(2))),subZ1_P(round(point3_xy(1)),round(point3_xy(2)))];
    point4_xy=[co(4,2) co(4,1)];
    point4_xyz=[subX1_P(round(point4_xy(1)),round(point4_xy(2))),subY1_P(round(point4_xy(1)),round(point4_xy(2))),subZ1_P(round(point4_xy(1)),round(point4_xy(2)))];
    
    %Visualize the slice area
    if vis==1
        figure(1)
        imshow(slice_Area,[]);title('Slice area')
        hold on
        rectangle('Position', ulBox,...
            'EdgeColor','r', 'LineWidth', 3)
        plot(point1_xy(2),point1_xy(1),'r*')
        plot(point2_xy(2),point2_xy(1),'b*')
        plot(point3_xy(2),point3_xy(1),'m*')
        plot(point4_xy(2),point4_xy(1),'y*')
        legend({'upper-left','upper-right','lower-left','lower-right'})
        figure(2)
        imshow(slice1_P,[]);title('Slice')
        hold on
        rectangle('Position', ulBox,...
            'EdgeColor','r', 'LineWidth', 3)
        plot(point1_xy(2),point1_xy(1),'r*')
        plot(point2_xy(2),point2_xy(1),'b*')
        plot(point3_xy(2),point3_xy(1),'m*')
        plot(point4_xy(2),point4_xy(1),'y*')
        legend({'upper-left','upper-right','lower-left','lower-right'})
    end
    
    %Correspondance between 2D index to subscripts: each row contains the
    %index and the corresponding
    %Distance between Point1  vs Point2
    d_top=anisotropic_distance(point1_xyz,point2_xyz,voxelSize);
    %Distance between Point2 vs Point 4
    d_right=anisotropic_distance(point2_xyz,point4_xyz,voxelSize);
    %Distance between Point 1 vs Point 3
    d_left=anisotropic_distance(point1_xyz,point3_xyz,voxelSize);
    %Distance between Point 3 vs Point 4
    d_bottom=anisotropic_distance(point3_xyz,point4_xyz,voxelSize);
    %Find two maxiumum edges
    [temp,idE]=sort([d_top d_right d_left d_bottom],'descend');
    maxEdges=temp(1:2);
    minEdges=temp(4);
    %Calculate area (microns^2)
    area(sliceNumIndex,1)= maxEdges(1)*minEdges(1);
    %If the corners are not calculated property an image is saved to
    %demonstrate where the corners were erroneously found
    if (temp(1)-temp(2))>5|((temp(3)-temp(4))>5)
        imshow(slice_Area,[]);
        hold on
        rectangle('Position', ulBox,...
            'EdgeColor','r', 'LineWidth', 3)
        plot(point1_xy(2),point1_xy(1),'r*')
        plot(point2_xy(2),point2_xy(1),'b*')
        plot(point3_xy(2),point3_xy(1),'m*')
        plot(point4_xy(2),point4_xy(1),'y*')
        legend({'upper-left','upper-right','lower-left','lower-right'})
        print(['densityPoblem_',name,'_',num2str(round(sliceNum))],'-dpng')
    end
    
    count=count+1;
    %Capillaries per mm^2
    cap_Density(sliceNumIndex,1)=capillaries_Number(sliceNumIndex,1)/ (area(sliceNumIndex,1)*10^(-6));
    
    %----------------------------------------------------------------------
    %               Calculate intercapillary distance
    %----------------------------------------------------------------------
    
    clear distCap distCap2
    % Calculate centroids
    propsCap=regionprops(labeled,'Centroid');
    for props=1:length(propsCap)
        tempCentroid=sub2ind(size(slice_Area),round(propsCap(props).Centroid(2)),round(propsCap(props).Centroid(1)));
        [Centroid_xy(1),Centroid_xy(2)]=ind2sub(size(slice_Area),tempCentroid);
        %Convert to 3D indexes
        Centroid_xyz(props,:)=[subX1_P(round(Centroid_xy(1)),round(Centroid_xy(2))),subY1_P(round(Centroid_xy(1)),round(Centroid_xy(2))),subZ1_P(round(Centroid_xy(1)),round(Centroid_xy(2)))];
        for cap2=1:props-1
            %Element i,j of distCap refers to the distance between
            %capillary i with capillaries j. Thus, in every row there are
            %the distances of the centroid a capillary (props) to the
            %centroids of all the others
            distCap(props,cap2)=anisotropic_distance(Centroid_xyz(props,:),Centroid_xyz(cap2,:),voxelSize);
            distCap(cap2,props-1)= distCap(props,cap2);
            distCap2{props,cap2}=[props,cap2];
            distCap2{cap2,props-1}=[cap2,props];
        end
    end
    %Keep the minimum distance between every capillary and the rest of them
    %(ie distance to closest capillary) and take the mean over all
    %capillaries to calculate intercapillary distance
    dMinAll(:,sliceNumIndex)=mean(min(distCap,[],2));
    
end

save([outputFolder,outName,name,'.mat'],'cap_Density','area','region','capillaries_Number','dMinAll')




