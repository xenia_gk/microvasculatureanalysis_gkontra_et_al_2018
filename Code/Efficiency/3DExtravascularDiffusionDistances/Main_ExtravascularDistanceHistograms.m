function []=Main_ExtravascularDistanceHistograms(create_plots,numberOfBins,dirOut,nameMat)
%--------------------------------------------------------------------------
%Function to calculate frequency histograms of extravascular distances for
%all images of the dataset. If desirable a 3D bar plot with histograms
%for all subjects organized and color-coded by tissue conditions can be
%be created along with plots of the histogram for each volume separately,
%plots of mean distributions as well as for each volume separately and
%a plot with convexity indexes.
%
%Input:
%               - create_plots: define whether you want to create and save 
%               plots or you rather prefer to only calculate histograns of 
%               extravascular distances. If create_plot=1, then plots
%               are created.
%               - numberOfBins: defines the bins for the histograms. More
%               precisely, if numberOfBins is empty, then 10 equally spaced
%               bins will be used. Otherwise, the numberOfBin will be used
%               so as to create a vector [1:numberOfBin] whose elements
%               represent the centers of the bins
%               - dirOut: directory to save the output (.mat file and
%               plots). If it is provided empty then by default the results
%               are saved in a folder created in the current directory with
%               name 'Results' 
%               - name: filename to be used for the output MAT-File with
%               the counts of the frequency histograms. If provided empty,
%               the default name used is DiffusionHistogram.
%
%Output:
%               - .mat file (DiffusionHistogram.mat) with the counts of
%               frequency histograms for the different time-points, the
%               centers of those bins, median and maximal extravascular
%               distances as well as slopes of frequency distributions
%               calculated over different ranges (the latter is not
%               presented in Gkontra et al., 2018)
%               - a plot (lamda_vs_max_dist) in different formats .png, .eps etc
%               with convexity indexes (Baish et al., 2011) versus
%               maximum extravascular distance (not presented in Gkontra et
%               al., 2018 but maybe interesting in other contexts)
%               - 3D bar plot of histograms of extravascular diffusion 
%               distances for all volumes of the dataset during
%               the call to create_3D_bar_overtime.m in .eps, .png. and 
%               .fig format
%               - .eps&.png.&.fig files with the plot of mean relative frequency
%               distribution during the call to
%               extravascular_distance_histogram function(figure 6c)
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clc

%Call function to set the paths to be used by the module to load the
%previously calculated data
directories=set_directories(GetFullPath('../..'));
%Directory where the volumes of extravascular distances are saved
dirMain=directories.mainDir_Segment;
%List time-points after MI to be studied. Please note first two folders
%listed by dir are '.' and '..' so we ignore them in the next steps
daysMI_All=dir(dirMain);
%Directory to save the output results
if isempty(dirOut)
    dirOut='Results/';
end
if ~exist(dirOut)
    mkdir(dirOut);
end
%The name for saving the .mat file. If it is no specified, then use the
%default, i.e. DiffusionHistogram
if isempty(nameMat)
    nameMat='DiffusionHistogram';
end

%--------------------------------------------------------------------------
%                Create histograms of relative frequencies
%--------------------------------------------------------------------------
%Use as title the region (endocardium, myocardium, epicardium) from which
%was obtained the tissue used to acquire the particular image. 
titlePlots=region_title_updated;

%==================   Basal    ============================================
%Time-point under investigation & tissue condition (Basal)
daysMI='Basal';
tissueCat='';
filelist=dir([dirMain,daysMI,'/',tissueCat,'/']);
[countsBasal,binCentersBasal,maxDistBasal,maxDistBasal_2,medianDistBasal,lamdaBasal,lamdaBasal_2,lamdaBasal_3,lamdaBasal_4]=histograms_per_tissue_condition(filelist,dirMain,dirOut,daysMI,tissueCat,titlePlots,numberOfBins,create_plots);


%================== 1 day post MI =========================================
%Time-point under investigation & tissue condition (Infarcted 1 day post MI)
daysMI='1MI';
tissueCat='Infarct';
filelist=dir([dirMain,daysMI,'/',tissueCat,'/']);
[countsI1MI,binCenterI1MI,maxDistI1MI,maxDistI1MI_2,medianDistI1MI,lamdaI1MI,lamdaI1MI_2,lamdaI1MI_3,lamdaI1MI_4]=histograms_per_tissue_condition(filelist,dirMain,dirOut,daysMI,tissueCat,titlePlots,numberOfBins,create_plots);

%Time-point under investigation & tissue condition (Remote 1 day post MI)
daysMI=daysMI_All(3).name;
tissueCat='Remote';
filelist=dir([dirMain,daysMI,'/',tissueCat,'/']);
[countsR1MI,binCenterR1MI,maxDistR1MI,maxDistR1MI_2,medianDistR1MI,lamdaR1MI,lamdaR1MI_2,lamdaR1MI_3,lamdaR1MI_4]=histograms_per_tissue_condition(filelist,dirMain,dirOut,daysMI,tissueCat,titlePlots,numberOfBins,create_plots);


%================== 3 days post MI =========================================
%Time-point under investigation & tissue condition (Infarcted 3 day post MI)
daysMI='3MI';
tissueCat='Infarct';
filelist=dir([dirMain,daysMI,'/',tissueCat,'/']);
[countsI3MI,binCenterI3MI,maxDistI3MI,maxDistI3MI_2,medianDistI3MI,lamdaI3MI,lamdaI3MI_2,lamdaI3MI_3,lamdaI3MI_4]=histograms_per_tissue_condition(filelist,dirMain,dirOut,daysMI,tissueCat,titlePlots,numberOfBins,create_plots);

%Time-point under investigation & tissue condition (Remote 3 day post MI)
tissueCat='Remote';
filelist=dir([dirMain,daysMI,'/',tissueCat,'/']);
[countsR3MI,binCenterR3MI,maxDistR3MI,maxDistR3MI_2,medianDistR3MI,lamdaR3MI,lamdaR3MI_2,lamdaR3MI_3,lamdaR3MI_4]=histograms_per_tissue_condition(filelist,dirMain,dirOut,daysMI,tissueCat,titlePlots,numberOfBins,create_plots);

%================== 7 days post MI =========================================
%Time-point under investigation & tissue condition (Infarcted 7 days post MI)
daysMI='7MI';
tissueCat='Infarct';
filelist=dir([dirMain,daysMI,'/',tissueCat,'/']);
[countsI7MI,binCenterI7MI,maxDistI7MI,maxDistI7MI_2,medianDistI7MI(:,2),lamdaI7MI,lamdaI7MI_2,lamdaI7MI_3,lamdaI7MI_4]=histograms_per_tissue_condition(filelist,dirMain,dirOut,daysMI,tissueCat,titlePlots,numberOfBins,create_plots);

%Time-point under investigation & tissue condition (Remote 7 days post MI)
daysMI=daysMI_All(3).name;
tissueCat='Remote';
filelist=dir([dirMain,daysMI,'/',tissueCat,'/']);
[countsR7MI,binCenterR7MI,maxDistR7MI,maxDistR7MI_2,medianDistR7MI,lamdaR7MI,lamdaR7MI_2,lamdaR7MI_3,lamdaR7MI_4]=histograms_per_tissue_condition(filelist,dirMain,dirOut,daysMI,tissueCat,titlePlots,numberOfBins,create_plots);

%--------------------------------------------------------------------------
%                       Save results
%--------------------------------------------------------------------------
save([dirOut,nameMat,'.mat'],'countsI7MI','binCenterI7MI','countsI3MI','binCenterI3MI','countsI1MI','binCenterI1MI',...
    'countsR7MI','binCenterR7MI','countsR3MI','binCenterR3MI','countsR1MI','binCenterR1MI',...
    'countsBasal','binCentersBasal','maxDistBasal','lamdaBasal','maxDistR1MI','lamdaR1MI','maxDistI1MI','lamdaI1MI',...
    'maxDistI3MI','lamdaI3MI','maxDistR3MI','lamdaR3MI','maxDistI7MI','lamdaI7MI','maxDistR7MI','lamdaR7MI',...
    'maxDistBasal_2','lamdaBasal_2','maxDistR1MI_2','lamdaR1MI_2','maxDistI1MI_2','lamdaI1MI_2',...
    'maxDistI3MI_2','lamdaI3MI_2','maxDistR3MI_2','lamdaR3MI_2','maxDistI7MI_2','lamdaI7MI_2','maxDistR7MI_2','lamdaR7MI_2',...
    'lamdaBasal_4','lamdaR1MI_4','lamdaI1MI_4','lamdaI3MI_4','lamdaR3MI_4','lamdaI7MI_4','lamdaR7MI_4',...
    'medianDistBasal','lamdaBasal_3','medianDistR1MI','lamdaR1MI_3','medianDistI1MI','lamdaI1MI_3',...
    'medianDistI3MI','lamdaI3MI_3','medianDistR3MI','lamdaR3MI_3','medianDistI7MI','lamdaI7MI_3','medianDistR7MI','lamdaR7MI_3')


%--------------------------------------------------------------------------
%                      Create Comparative Plots
%--------------------------------------------------------------------------

if create_plots==1
    %----------------------------------------------------------------------
    %         Plot convexity index versus maximum extravascular distances
    %----------------------------------------------------------------------
    close all
    %Colors
    
    col(1,1:3)=[230 159 0]/255;
    col(2,1:3)=[86 180 233]/255;
    col(3,1:3)=[0 158 115]/255;
    col(4,1:3)=col(1,1:3);
    col(5,1:3)=col(2,1:3);
    col(6,1:3)=col(3,1:3);
    col(7,1:3)=[0.5 0.5 0.5];
    figure('units','normalized','outerposition',[0 0 1 1])
    plot(lamdaI1MI_2,maxDistI1MI_2, 'o','LineWidth',2,'Color',col(1,:));
    hold on
    plot(lamdaI3MI_2,maxDistI3MI_2, 'o','LineWidth',2,'Color',col(2,:))
    plot(lamdaI7MI_2,maxDistI7MI_2, 'o','LineWidth',2,'Color',col(3,:))
    plot(lamdaR1MI_2,maxDistR1MI_2, 'x','LineWidth',3,'Color',col(4,:))
    plot(lamdaR3MI_2,maxDistR3MI_2, 'x','LineWidth',3,'Color',col(5,:))
    plot(lamdaR7MI_2,maxDistR7MI_2, 'x','LineWidth',3,'Color',col(6,:))
    plot(lamdaBasal_2,maxDistBasal_2, 'x','LineWidth',3,'Color',col(7,:))
    hold off
    yl = ylim;
    ylabelPlot={'Maximum distance to vessel';' /delta _{max} (/mu m)'};
    xlabelPlot='Convexity index /lambda';
    hl = legend({'Infarcted 1 day post MI (I1MI)','Infarcted 3 days post MI (I3MI)','Infarcted 7 days post MI (I7MI)','Remote 1 day post MI (R1MI)','Remote 3 days post MI (R3MI)',...
        'Remote 7 days post MI (R7MI)','Basal'},'Location','NorthEast','Fontsize',16);
    ylabel(ylabelPlot,'FontName', 'Helvetica','FontSize',24)
    xlabel(xlabelPlot,'FontName', 'Helvetica','FontSize',24)
    xt = get(gca, 'YTick');
    set(gca, 'FontName', 'Helvetica','FontSize',26)
    set(gca, ...
        'Box'         , 'off'     , ...
        'TickDir'     , 'out'     , ...
        'TickLength'  , [.02 .02] , ...
        'XMinorTick'  , 'on'      , ...
        'YMinorTick'  , 'on'      , ...
        'YGrid'       , 'on'      , ...
        'XColor'      , [.3 .3 .3], ...
        'YColor'      , [.3 .3 .3], ...
        'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
        'LineWidth'   , 1 );
    saveas(gca,[dirOut,'lamda_vs_max_dist.png'])
    saveas(gca,[dirOut,'lamda_vs_max_dist.fig'])
    print(fullfile([dirOut,'lamda_vs_max_dist.eps']), '-depsc');
    saveas(gca,[dirOut,'lamda_vs_max_dist.eps'],'epsc')
    
    %----------------------------------------------------------------------
    %           Create common 3D bar plot of frequency
    %           histograms for all subjects
    %----------------------------------------------------------------------
    %Create 3D bar plot with histograms for all volume and all tissue
    %categories
    create_3D_bar_overtime(dirOut,countsI1MI,countsI3MI,countsI7MI,countsR1MI,countsR3MI,countsR7MI,countsBasal)
    
    %----------------------------------------------------------------------
    %               Mean relative frequency distributions
    %----------------------------------------------------------------------   
    %Create plot with mean relative frequencies (extravascularDistancePlot) & return pvalues for
    %comparing mean frequency distributions (kolmogorov-smirnov)
    p=create_plot_overtime(dirOut,countsI1MI,countsI3MI,countsI7MI,countsR1MI,countsR3MI,countsR7MI,countsBasal,binCenterI1MI,binCenterI3MI,binCenterI7MI,binCenterR1MI,binCenterR3MI,binCenterR7MI,binCentersBasal);
end