function [p]=create_plot_overtime(dirOut,countsI1MI,countsI3MI,countsI7MI,countsR1MI,countsR3MI,countsR7MI,countsBasal,binCenterI1MI,binCenterI3MI,binCenterI7MI,binCenterR1MI,binCenterR3MI,binCenterR7MI,binCentersBasal)
%Function to create a common plot with mean frequency histograms for all
%subjects per time-point
%
%Input:
%               - dirOut: directory to save the created plot
%               - countsI1MI: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 1st
%               (here infarcted 1 day post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsI3MI: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 2nd
%               (here infarcted 3 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsI7MI: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 3rd
%               (here infarcted 7 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsR1MI: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 4th
%               (here remote 1 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsR3MI:  matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 5th
%               (here remote 3 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsR7MI:  matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 6th
%               (here remote 7 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsBasal: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 7th
%               (here basal conditions) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - binCenterI1MI,binCenterI3MI,binCenterI7MI,binCenterR1MI,
%               binCenterR3MI,binCenterR7MI,binCentersBasal: corresponding
%               bin centers for the aforementioned matrices
%
%Output:
%               - p: array of size (number of comparisons)x1 containing the
%               pvalues returned by kolmogorov-smirnov test to test for
%               significant differences between mean frequency
%               distributions.
%
%Author: Xenia Gkontra


%--------------------------------------------------------------------------
%           Mean counts per tissue category over all samples of the
%           particular category
%--------------------------------------------------------------------------

countsI1MI_All=sum(countsI1MI,2)/size(countsI1MI,2);
countsI3MI_All=sum(countsI3MI,2)/size(countsI3MI,2);
countsI7MI_All=sum(countsI7MI,2)/size(countsI7MI,2);
countsR1MI_All=sum(countsR1MI,2)/size(countsR1MI,2);
countsR3MI_All=sum(countsR3MI,2)/size(countsR3MI,2);
countsR7MI_All=sum(countsR7MI,2)/size(countsR7MI,2);
countsBasal_All=sum(countsBasal,2)/size(countsBasal,2);

%--------------------------------------------------------------------------
%               Create Plot
%--------------------------------------------------------------------------
close all
figure('units','normalized','outerposition',[0 0 1 1])
%Colors to use
col(1,1:3)=[230 159 0]/255;
col(2,1:3)=[86 180 233]/255;
col(3,1:3)=[0 158 115]/255;
col(4,1:3)=col(1,1:3);
col(5,1:3)=col(2,1:3);
col(6,1:3)=col(3,1:3);
col(7,1:3)=[0.5 0.5 0.5];
%Plot

plot(binCenterI1MI(:,1), countsI1MI_All,'--','LineWidth',2,'Color',col(1,:));
hold on
plot(binCenterI3MI(:,1), countsI3MI_All,'--','LineWidth',2,'Color',col(2,:))
plot(binCenterI7MI(:,1), countsI7MI_All,'--','LineWidth',2,'Color',col(3,:))
plot(binCenterR1MI(:,1), countsR1MI_All,'LineWidth',2,'Color',col(4,:))
plot(binCenterR3MI(:,1), countsR3MI_All,'LineWidth',2,'Color',col(5,:))
plot(binCenterR7MI(:,1), countsR7MI_All,'LineWidth',2,'Color',col(6,:))
plot(binCentersBasal, countsBasal_All,'LineWidth',2,'Color',col(7,:))
hl = legend({'Infarcted 1 day post MI (I1MI)','Infarcted 3 days post MI (I3MI)','Infarcted 7 days post MI (I7MI)','Remote 1 day post MI (R1MI)','Remote 3 days post MI (R3MI)',...
    'Remote 7 days post MI (R7MI)','Basal'},'Location','NorthEast','Fontsize',16);
yl = ylim;
xlim([0 30]);
ylabelPlot='Relative Frequency';
xlabelPlot='Distance to closest vessel (\mu m)';
ylabel(ylabelPlot,'FontName', 'Helvetica','FontSize',26)
xlabel(xlabelPlot,'FontName', 'Helvetica','FontSize',26)
ylabel(ylabelPlot,'FontName', 'Helvetica','FontSize',26)
xt = get(gca, 'YTick');
set(gca, 'FontName', 'Helvetica','FontSize',26)

%set(gca,'XTick',[categories(2) categories(5) categories(7)]);
set(gca, ...
    'Box'         , 'off'     , ...
    'TickDir'     , 'out'     , ...
    'TickLength'  , [.02 .02] , ...
    'XMinorTick'  , 'on'      , ...
    'YMinorTick'  , 'on'      , ...
    'YGrid'       , 'on'      , ...
    'XColor'      , [.3 .3 .3], ...
    'YColor'      , [.3 .3 .3], ...
    'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
    'LineWidth'   , 1 );

%--------------------------------------------------------------------------
%                   Save plot
%--------------------------------------------------------------------------

saveas(gca,[dirOut,'extravascularDistancePlot.png'])
saveas(gca,[dirOut,'extravascularDistancePlot.fig'])
print(fullfile([dirOut,'extravascularDistancePlot.eps']), '-depsc');
saveas(gca,[dirOut,'extravascularDistancePlot.eps'],'epsc')
%Statistics

%--------------------------------------------------------------------------
%               Statistics
%--------------------------------------------------------------------------

%Two sample kolomogorov-smirnof test
%Remote vs Infarcted
[h(1),p(1)]=kstest2(countsI1MI_All(countsI1MI_All>0),countsR1MI_All(countsR1MI_All>0));
[h(2),p(2)]=kstest2(countsI3MI_All(countsI3MI_All>0),countsR3MI_All(countsR3MI_All>0));
[h(3),p(3)]=kstest2(countsI7MI_All(countsI7MI_All>0),countsR7MI_All(countsR7MI_All>0));
%Infarcted over time
[h(4),p(4)]=kstest2(countsI1MI_All(countsI1MI_All>0),countsI3MI_All(countsI3MI_All>0));
[h(5),p(5)]=kstest2(countsI1MI_All(countsI1MI_All>0),countsI7MI_All(countsI7MI_All>0));
[h(6),p(6)]=kstest2(countsI3MI_All(countsI3MI_All>0),countsI7MI_All(countsI7MI_All>0));
%Remote over time
[h(7),p(7)]=kstest2(countsR1MI_All(countsR1MI_All>0),countsR3MI_All(countsR3MI_All>0));
[h(8),p(8)]=kstest2(countsR1MI_All(countsR1MI_All>0),countsR7MI_All(countsR7MI_All>0));
[h(9),p(9)]=kstest2(countsR3MI_All(countsR3MI_All>0),countsR7MI_All(countsR7MI_All>0));
%Infarcted vs basal
[h(10),p(10)]=kstest2(countsI1MI_All(countsI1MI_All>0),countsBasal_All(countsBasal_All>0));
[h(11),p(11)]=kstest2(countsI3MI_All(countsI3MI_All>0),countsBasal_All(countsBasal_All>0));
[h(12),p(12)]=kstest2(countsI7MI_All(countsI7MI_All>0),countsBasal_All(countsBasal_All>0));
%Remote vs basal
[h(13),p(13)]=kstest2(countsR1MI_All(countsR1MI_All>0),countsBasal_All(countsBasal_All>0));
[h(14),p(14)]=kstest2(countsR3MI_All(countsR3MI_All>0),countsBasal_All(countsBasal_All>0));
[h(15),p(15)]=kstest2(countsR7MI_All(countsR7MI_All>0),countsBasal_All(countsBasal_All>0));


