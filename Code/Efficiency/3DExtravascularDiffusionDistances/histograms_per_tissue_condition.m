function [countsR1MI,binCenterR1MI,maxDistR1MI,maxDistR1MI_2,medianDistR1MI,lamdaR1MI,lamdaR1MI_2,lamdaR1MI_3,lamdaR1MI_4] = histograms_per_tissue_condition(filelist,dirMain,dirOut,daysMI,tissueCat,titlePlots,numberOfBins,create_plots)
%Function to call extravascular_distance_histogram.m so as to create a
%histogram per image in filelist struct
%
%Input:
%               - filelist: structure that contains the list of images to
%               be processed
%               - outputFolder: folder to save the results
%               - dirMain: directory to load the distance transformation
%               volume (each voxel contains the extravascular distance, ie
%               distance to the closest vessel)
%               - dirOut: directory to save the results
%               - daysMI: time-point under investigation (here it will be
%               '1MI', '3MI', '7MI' or 'Basal'
%               - tissueCat: category of tissue (here it will be 'Remote',
%               'Infarct', or '' in the case of Basal)
%               - titlePlots: title to be used for the plot
%               - numberOfBins: defines the bins for the histogram. More
%               precesily, if numberOfBins is empty, then 10 equally spaced
%               bins will be used. Otherwise, the numberOfBin will be used
%               so as to create a vector [1:numberOfBin] whose elements
%               represent the centers of the bin
%               -create_plots: indicate whether you want to create and save
%               plots with the histograms for each volume sepaterly. If
%               create_plot=1 then plots are created.
%
%Output:
%               - counts: number of counts in each bin
%               - binCenters: centers of the bins
%               - maxSum: maximum extravascular distance found
%               - maxDist_95prct: maximal extravascular distance defined as
%               the 95% quartile of the frequency distribution
%               - medianDist: median extravascular distance defiend as the
%               50% quartile of the frequency distribution
%               - lamda1: slope in the frequency distribution. If the slope 
%               was calculated over the area where the power low is true (1/3 
%               of the maximum extravascular distance found in
%               distribution)  according to Baish et al., 2011, we end up with the
%               convexity index
%               - lamda2: convexity index (Baish et al., 2011) calculated
%               as as the slope in the frequency distribution until the 1/3
%               of the maximal extravascular distance
%               - lamda3: slope of frequency distribution between median
%               extravascular distance & maximal
%               - lamda4: slope of frequency distribution until the median
%               extravascular distance
%               - .eps file with the plot of fequency distribution
%
%
%Author: Xenia Gkontra


for i=3:length(filelist)
        %Name of image used for loading and saving results
        type=filelist(i).name;
        %Directory for saving the results
        directoryOut=[dirOut,daysMI,'/',tissueCat,'/',type,'/'];
        if ~exist(directoryOut)
            mkdir(directoryOut);
        end
        %Directory for loading 3D extravascular distances volume
        directoryIn=[dirMain,daysMI,'/',tissueCat,'/',type,'/'];
        load([directoryIn,'SDF_',type,'.mat'])
        tic
        close all
        %Load and calculate properties
        [countsR1MI(:,i-2),binCenterR1MI(:,i-2),maxDistR1MI(:,i-2),maxDistR1MI_2(:,i-2),medianDistR1MI(:,i-2),lamdaR1MI(:,i-2),lamdaR1MI_2(:,i-2),lamdaR1MI_3(:,i-2),lamdaR1MI_4(:,i-2)]=extravascular_distance_histogram(SDF, directoryOut, type,titlePlots,numberOfBins,create_plots);
end



