function []=create_3D_bar_overtime(dirOut,countsI1MI,countsI3MI,countsI7MI,countsR1MI,countsR3MI,countsR7MI,countsBasal)
%Function to create 3D bar plot with histograms of extravascular distances 
%for all subjects and all time points color-coded according to the tissue
%category the volume belongs to(plot appearing in Gkontra et al.,
%2018, figure 6b).
%
%Input:
%               - dirOut: directory to save the created plot
%               - countsI1MI: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 1st
%               (here infarcted 1 day post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsI3MI: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 2nd
%               (here infarcted 3 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsI7MI: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 3rd
%               (here infarcted 7 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsR1MI: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 4th
%               (here remote 1 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsR3MI:  matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 5th
%               (here remote 3 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsR7MI:  matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 6th
%               (here remote 7 days post MI) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%               - countsBasal: matrix size of (number of images)x(number of bins)
%               containing the counts per image per bin for the 7th
%               (here basal conditions) time-point under comparison.
%               Every row refers to an image and every column to a bin of
%               the frequency disrtibution histogram
%
%Output:
%               - 3D bar plot of histograms of extravascular diffusion 
%               distances for all volumes of the dataset during
%               the call to create_3D_bar_overtime.m in .eps, .png. and 
%               .fig format
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com

close all;

%--------------------------------------------------------------------------
%               Create bar plot
%--------------------------------------------------------------------------

%Plot bar plot with default colors
b=bar3([countsI1MI countsI3MI countsI7MI zeros(size(countsI1MI,1),18) countsR1MI countsR3MI countsR7MI zeros(size(countsI1MI,1),18) countsBasal]);
%Define desired colors to be used per time point
col(1,1:3)=[230 159 0]/255;
col(2,1:3)=[86 180 233]/255;
col(3,1:3)=[0 158 115]/255;
col(4,1:3)=col(1,1:3);
col(5,1:3)=col(2,1:3);
col(6,1:3)=col(3,1:3);
col(7,1:3)=[0.5 0.5 0.5];
%Infarct 1 day post MI color
for k = 1:size(countsI1MI,2)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = col(1,:);
end

%Infarct 3 days post MI color
for k = size(countsI1MI,2)+1:size(countsI1MI,2)+size(countsI3MI,2)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = col(2,:);
end

%Infarct 7 days post MI color
for k = size(countsI1MI,2)+size(countsI3MI,2)+1:size(countsI1MI,2)+size(countsI3MI,2)+size(countsI7MI,2)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = col(3,:);
end
%All infarcted subjects number
siz_temp=size(countsI3MI,2)+size(countsI1MI,2)+size(countsI7MI,2);
%Keep a copy of only the infarcted number
siz_inf=siz_temp;
%Gap
for k = siz_temp+1:siz_temp+18
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = [255 255 255]/255;
end
%Infarcted+gap
siz_temp=siz_temp+18;
%Remote 1 day post MI color
for k = siz_temp+1:siz_temp+size(countsR1MI,2)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = col(4,:);
end

%Remote 3 day post MI color
for k = siz_temp+size(countsR1MI,2)+1:siz_temp+size(countsR1MI,2)+size(countsR3MI,2)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = col(5,:);
end
%%Remote 7MI
for k = siz_temp+size(countsR1MI,2)+size(countsR3MI,2)+1:siz_temp+size(countsR1MI,2)+size(countsR3MI,2)+size(countsR7MI,2)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = col(6,:);
end
%Infracted+gap+remote
siz_temp=siz_temp+size(countsR1MI,2)+size(countsR3MI,2)+size(countsR7MI,2);
%Gap
for k = siz_temp+1:siz_temp+18
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = [255 255 255]/255;
end
%Infracted+2*gap+remote
siz_temp=siz_temp+18;
%Basal color
for k = siz_temp+1:siz_temp+size(countsBasal,2)
    zdata = b(k).ZData;
    b(k).CData = zdata;
    b(k).FaceColor = col(7,:);
end

%--------------------------------------------------------------------------
%           Format the figure for publication
%--------------------------------------------------------------------------

%Set labels on y,z axis
zlabelPlot='Relative Frequency';
ylabelPlot='Distance to closest vessel (\mu m)';
zlabel(zlabelPlot,'FontName', 'Avant Garde','FontSize',26)
ylabel(ylabelPlot,'FontName', 'Avant Garde','FontSize',26,'rot',-30)
set(gca, 'FontName', 'Avant Garde','FontSize',26)
ylim([0 40]);
yl=ylim;
set(gca,'XTick',[1 siz_inf+18+1 siz_temp+1])
set(gca,'XTickLabel',{'Infarcted','Remote','Basal'},'FontName', 'Avant Garde','FontSize',26);
title('Extravascular distance distribution','FontName', 'Avant Garde','FontSize',26)
set(gca, ...
    'Box'         , 'off'     , ...
    'TickDir'     , 'out'     , ...
    'TickLength'  , [.02 .02] , ...
    'XMinorTick'  , 'on'      , ...
    'YMinorTick'  , 'on'      , ...
    'YGrid'       , 'on'      , ...
    'XColor'      , [.3 .3 .3], ...
    'ZColor'      , [.3 .3 .3], ...
    'YColor'      , [.3 .3 .3], ...
    'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
    'LineWidth'   , 1 );
set(gcf, 'units','normalized','outerposition',[0 0 1 1]);

%--------------------------------------------------------------------------
%                       Save bar plot
%--------------------------------------------------------------------------
legend([b(1) b(size(countsI1MI,2)+1) b(size(countsI1MI,2)+size(countsI3MI,2)+1) b(siz_temp+1)],{'1 day post MI','3 days post MI','7 days post MI','Basal'},'Location','northeast');
saveas(gca,[dirOut,'extravascular_distance','.png'])
saveas(gca,[dirOut,'extravascular_distance','.fig'])
print(fullfile([dirOut,'extravascular_distance','.eps']), '-depsc');
