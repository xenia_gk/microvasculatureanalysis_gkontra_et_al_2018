function [counts,binCenters,maxSum,maxDist_95prct,medianDist,lamda1,lamda2,lamda3,lamda4]=extravascular_distance_histogram(SDF1, outputFolder, name,titlePlots, numberOfBins,create_plots)
%Function to create frequency histogram of extravascular distances
%
%Input:
%               - SDF1: volume with distances at each point to the closest
%               vessel
%               - outputFolder: folder to save the results
%               - name: name to be used for saving the plot
%               - titlePlots: title to be used for the plot
%               - numberOfBins: defines the bins of the histogram. More precisely,
%               If it is empty creaty 10 equally spaced bins, else use
%               numberOfBin to create vector with the centers of the bins
%               -create_plots: indicate whether you want to create and save
%               plots with the histograms for each volume sepaterly. If
%               create_plot=1 then plots are created.
%
%Output:
%               - counts: number of counts in each bin
%               - binCenters: centers of the bins
%               - maxSum: maximum extravascular distance found
%               - maxDist_95prct: maximal extravascular distance defined as
%               the 95% quartile of the frequency distribution
%               - medianDist: median extravascular distance defiend as the
%               50% quartile of the frequency distribution
%               - lamda1: slope in the frequency distribution. If the slope
%               was calculated over the area where the power low is true (1/3
%               of the maximum extravascular distance found in
%               distribution)  according to Baish et al., 2011, we end up with the
%               convexity index
%               - lamda2: convexity index (Baish et al., 2011) calculated
%               as as the slope in the frequency distribution until the 1/3
%               of the maximal extravascular distance
%               - lamda3: slope of frequency distribution between median
%               extravascular distance & maximal
%               - lamda4: slope of frequency distribution until the median
%               extravascular distance
%               - .eps file with the plot of fequency distribution
%
%Author: Xenia Gkontra


%--------------------------------------------------------------------------
%           Calculate histogram's bins
%--------------------------------------------------------------------------

data=SDF1([find(SDF1>0)]);
minSum = min(data);
maxSum = max(data);
%If it is empty creaty 10 equally spaced bins, else use numberOfBin to
%create vector with the centers of the bins
if isempty(numberOfBins)
    binsNum=10;
else
    binsNum=[1:numberOfBins] ;
end

[counts binCenters] = hist(data,binsNum ); % No loop over i needed.
%Keep a copy of counts before normalizing
counts_Init=counts;
counts = counts / sum(counts);
medianDist=prctile(data,50);
maxDist_95prct=prctile(data,95);
%--------------------------------------------------------------------------
%    Calculate convexity indexes (Baish et al., 2011)
%--------------------------------------------------------------------------
%Approach 1: lamda1
%---------
%Calculate slope using normalized values for the bins
%Bins with values
index=find(counts>0);
ind=index;
x=log(binCenters(ind));
y=log(counts(ind));
%Fit line using least squares
[p,S] = polyfit(x,y,1);
%Slope corresponds to the convexity index
lamda1 = p(1);

%Lamda 2 regions at <maxSum/3 (Area where the power law is true)
index2=find(binCenters(index)<maxDist_95prct/3);
ind=index(index2);
x=log(binCenters(ind));
y=log(counts(ind));
%Fit line using least squares
[p,S] = polyfit(x,y,1);
%Slope corresponds to the convexity index
lamda2 = p(1);

%Lamda 2 regions at <maxSum/3
index2=find(binCenters(index)<maxDist_95prct&binCenters(index)>medianDist);
ind=index(index2);
x=log(binCenters(ind));
y=log(counts(ind));
%Fit line using least squares
[p,S] = polyfit(x,y,1);
%Slope corresponds to the convexity index
lamda3 = p(1);

%Lamda 2 regions at <medianDist
index2=find(binCenters(index)<medianDist);
ind=index(index2);
x=log(binCenters(ind));
y=log(counts(ind));
%Fit line using least squares
[p,S] = polyfit(x,y,1);
%Slope corresponds to the convexity index
lamda4 = p(1);

%--------------------------------------------------------------------------
%               Plot histogram
%--------------------------------------------------------------------------
%Based on https://blogs.mathworks.com/loren/2007/12/11/making-pretty-graphs/
if create_plots==1
    H=bar(binCenters, counts, 'BarWidth', 1);
    set(H,'FaceColor',[0.5,0.5,0.5])
    grid on;
    % Enlarge figure to full screen.
    set(gcf, 'units','normalized','outerposition',[0 0 1 1]);
    hold on;
    plot(binCenters, counts,'LineWidth',2,'Color','R')
    
    %Add annotations for median (50% quartile) and maximal (95% quartile)
    %extravascular distances
    dim = [0.49 0.36 0.41 0.55];
    str{1,1}='All microvessels';
    str{2,1}='-------------------';
    str{3,1} = ['50% Quartile: ',num2str(prctile(data,50)),' \mu m'];
    str{4,1} = ['95% Quartile: ',num2str(prctile(data,95)),' \mu m'];
    str{5,1}=['Mean: ',num2str(mean(data)),' \mu m'];
    %first <than median, smaller tha dmax/3 and finally between median and dmax
    str{6,1}=['\lambda _1: ',num2str(lamda4),', \lambda _2: ',num2str(lamda2)];
    str{7,1}='Frequency distribution';
    str{8,1}='-------------------';
    str{9,1} = ['50% Quartile: ',num2str(prctile(binCenters(counts>0),50)),' \mu m'] ;
    str{10,1} = ['95% Quartile: ',num2str(prctile(binCenters(counts>0),95)),' \mu m'];
    annotation('textbox',dim,'String',str,'FontName', 'Helvetica','FontSize',19);
    ylim([0 0.5]);
    yl = ylim;
    xlim([0 numberOfBins]);
    %Add title
    indIm=find(cellfun(@(x)ismember(x(1,1),str2num(name)),titlePlots(:,1)));
    title(titlePlots(indIm,2), 'FontName', 'Helvetica','FontSize',26);
    ylabelPlot{1}='Relative Frequency';
    xlabelPlot='Distance to closest vessel (\mu m)';
    ylabel(ylabelPlot,'FontName', 'Helvetica','FontSize',26)
    xlabel(xlabelPlot,'FontName', 'Helvetica','FontSize',26)
    set(gca, 'FontName', 'Helvetica','FontSize',26)
    set(gca, 'FontName', 'Helvetica','FontSize',26)
    
    %Create quality graphs for publication
    set(gca, ...
        'Box'         , 'off'     , ...
        'TickDir'     , 'out'     , ...
        'TickLength'  , [.02 .02] , ...
        'XMinorTick'  , 'on'      , ...
        'YMinorTick'  , 'on'      , ...
        'YGrid'       , 'on'      , ...
        'XColor'      , [.3 .3 .3], ...
        'YColor'      , [.3 .3 .3], ...
        'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
        'LineWidth'   , 1 );
    
    %--------------------------------------------------------------------------
    %                  Save results
    %--------------------------------------------------------------------------
    print(fullfile([outputFolder,'relativeFrequenceDistance_',name,'.eps']), '-depsc');
end
