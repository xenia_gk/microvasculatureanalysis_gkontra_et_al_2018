%--------------------------------------------------------------------------
%Function to fit superellipsoids to mean frequency distributions
%
%
%Author: Xenia Gkontra, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

%Output folder
curdir=pwd;
dirOut=[curdir,'/Results/'];
if ~exist(dirOut)
    mkdir(dirOut);
end
%whether to save .mha files with the estimated super-ellipsoid (raw=1) or not
raw=1;
%--------------------------------------------------------------------------
%           Counts & centers of mean frequency histograms (10 bins)
%                   per tissue condition
%--------------------------------------------------------------------------

%Check whether .mat file with frequency histograms with 10 bins exist
%otherwise create it by calling Main_ExtravascularDistanceHistograms
if ~exist([dirOut,'DiffusionHistogram_10bins.mat'])
    nameMAT='DiffusionHistogram_10bins';
    %Function Main_ExtravascularDistanceHistograms is called with the
    %following inputs: (i) to not create plots (create_plots=0), (ii)
    %to use 10 bins for calculating the histograms, (iii) the current path
    %as the path where the output should be saved, and (iv) the filename
    %used for the MAT-Files is 'DiffusionHistogram_10bins.mat'
    run('../3DExtravascularDiffusionDistances/Main_ExtravascularDistanceHistograms(0,10,dirOut,nameMAT)');
end
load([dirOut,'DiffusionHistogram_10bins.mat']);
%Counts per bin per tissue category
countsI1MI_All=sum(countsI1MI,2)/size(countsI1MI,1);
countsI3MI_All=sum(countsI3MI,2)/size(countsI3MI,1);
countsI7MI_All=sum(countsI7MI,2)/size(countsI7MI,1);
countsR1MI_All=sum(countsR1MI,2)/size(countsR1MI,1);
countsR3MI_All=sum(countsR3MI,2)/size(countsR3MI,1);
countsR7MI_All=sum(countsR7MI,2)/size(countsR7MI,1);
countsBasal_All=sum(countsBasal,2)/size(countsBasal,1);
%Centers of the bins
binCenterI1MI_All=sum(binCenterI1MI,2)/size(binCenterI1MI,1);
binCenterI3MI_All=sum(binCenterI3MI,2)/size(binCenterI3MI,1);
binCenterI7MI_All=sum(binCenterI7MI,2)/size(binCenterI7MI,1);
binCenterR1MI_All=sum(binCenterR1MI,2)/size(binCenterR1MI,1);
binCenterR3MI_All=sum(binCenterR3MI,2)/size(binCenterR3MI,1);
binCenterR7MI_All=sum(binCenterR7MI,2)/size(binCenterR7MI,1);
binCentersBasal_All=sum(binCentersBasal,2)/size(binCentersBasal,1);

%--------------------------------------------------------------------------
%           Find super-ellipsoid's parameters that
%           best fit the mean frequancy histograms/distributions
%           per tissue condition
%--------------------------------------------------------------------------

close all
fit_3d_superellipsoid_tissue_global(countsI1MI_All',binCenterI1MI_All(:,1)','I1MI',dirOut,raw)
close all
fit_3d_superellipsoid_tissue_global(countsI3MI_All',binCenterI3MI_All(:,1)','I3MI',dirOut,raw)
close all
fit_3d_superellipsoid_tissue_global(countsI7MI_All',binCenterI7MI_All(:,1)','I7MI',dirOut,raw)
close all
fit_3d_superellipsoid_tissue_global(countsR1MI_All',binCenterR1MI_All(:,1)','R1MI',dirOut,raw)
close all
fit_3d_superellipsoid_tissue_global(countsR3MI_All',binCenterR3MI_All(:,1)','R3MI',dirOut,raw)
close all
fit_3d_superellipsoid_tissue_global(countsR7MI_All',binCenterR7MI_All(:,1)','R7MI',dirOut,raw)
close all
fit_3d_superellipsoid_tissue_global(countsBasal_All',binCentersBasal_All(:,1)','Basal',dirOut,raw)
