function []=fit_3d_superellipsoid_tissue_global(countsTarget,binCentersTarget,type,folderOut,raw)
%Function to estimate the n and/or axis of a superellipse that could fit
%the distance distribution of tissue points to the closest vesssel as
%calculated by 3D distance transform of the tissse
%Input:
%               - countsTarget: counts of target histogram
%               - binCentersTarget: centers of bins of target histogram
%               - type: name of the volume to be processed used to save the
%               plots and parameters
%               - folderOut: directory to save the calculated parameters and
%               a figur with the fitting
%               - raw: whether to save (raw=1) .mha files or not
%Output:
%               The functions does not return any parameters but the
%               parameters calculated for the superllipsoid, alonmg with a figure
%               of the fitting between the target distance distribution and
%               the one produced by the estimated superllipse are saved in
%               the outputFolder
%
%Author: Xenia Gkontra, CNIC

%--------------------------------------------------------------------------
%                    Target distribution: Maximum & 
%               minimum extravascular distances founs
%--------------------------------------------------------------------------

maxDistance=max(binCentersTarget(countsTarget>0));
minDistance=min(binCentersTarget(countsTarget>0));
%Convert microns to voxels
minDistance=minDistance;
maxDistance=maxDistance;
figure(1)
plot(binCentersTarget,countsTarget,'r');title('Target distribution')

%--------------------------------------------------------------------------
%        Find superelliposid that could fit this distribution
%--------------------------------------------------------------------------
Nx=100;Ny=100;Nz=100;
[x,y,z]=meshgrid(1:Nx,1:Ny,1:Nz);
%Create central capillary with radius equal to the smallest distance found
%in data
temp2=( ((x-Nx/2)/minDistance).^2+((y-Ny/2)/minDistance).^2 )<=1;
%Calculate the distance transform of the volume containing the central
%vessel
distanceCenter=bwdist(temp2);

%Set parameters for the cylinder
%Axis-x: Unknow parameter
[~,indx]=(max(countsTarget));
%Axis-y:set to the maximal distance
a0(1)=maxDistance;
a3=[maxDistance 100];
e10=1;
e20=0.2;

%Optimization
opts = optimoptions('fmincon');
opts =  optimoptions(opts,'Algorithm','sqp');

%Parameters to define -initial values
b0=[a0 e10 e20];
%Optimization so as to find parameters that define a superllispoid for
%which the distribution of extravascular distances best fits the target
%distribution, ie histogram
opts = optimoptions(@fmincon,'Algorithm','sqp','Display','iter');
problem = createOptimProblem('fmincon','objective',...
    @(rn)objectiveFun_superellipsoid(rn,x,y,z,Nx/2,Ny/2,Nz/2,a3,countsTarget,binCentersTarget,distanceCenter),...
    'x0',b0,'lb',[minDistance 0 0],'ub',[maxDistance 3 3],'options',opts);
gs = GlobalSearch;
[b,f] = run(gs,problem);
%Updated after optimization parameters for the super-elliposid that
%best fits the given frequency histogram
a=[b(1) a3];
e1=b(2);e2=b(3);



%--------------------------------------------------------------------------
%               Verify distances
%--------------------------------------------------------------------------
%Estimated shape & volumes with extarvascular distances
[objEstimated]=superelleipsoid(a,e1,e2,x,y,z,Nx/2,Ny/2,Nz/2);
if raw==1
    SCIMAT = scimat_im2scimat(objEstimated+2*temp2);
    scirunnrrd = scimat_save([folderOut,'objEstimated_',type,'.mha'],SCIMAT);
end
distanceVol_Est=distanceCenter.*objEstimated;
if raw==1
    SCIMAT = scimat_im2scimat(distanceVol_Est);clc
    scirunnrrd = scimat_save([folderOut,'distanceVol_Est',type,'.mha'],SCIMAT);
end
%Create histogram
[countsEst binCentersEst] = hist(distanceVol_Est(distanceVol_Est>0),binCentersTarget);
countsEst=countsEst/sum(countsEst);
figure(3)
hist(distanceVol_Est(distanceVol_Est>0),binCentersTarget)
hold on
plot(binCentersEst,countsEst,'b');title('Estimated disribution')

%--------------------------------------------------------------------------
%               Common and goodness of fit between curves
%--------------------------------------------------------------------------

figure(4)
plot(binCentersTarget, countsTarget,'LineWidth',2,'Color','R')
hold on
plot(binCentersEst, countsEst,'LineWidth',2,'Color','B')
[r2,rmse] = rsquare (countsEst,countsTarget);
%Format figure for publication
dim = [0.63 0.35 0.30 0.59];
str{1,1}=['RMSE=',];
str{2,1}=[num2str(rmse)];
str{3,1}=['a=',num2str(a(1))];
str{4,1} = ['b=',num2str(a(2))];
str{5,1} = ['c=',num2str(a(3))];
str{6,1} = ['e1=',num2str(round(e1*100)/100)];
str{7,1}= ['e2=',num2str(round(e2*100)/100)];
annotation('textbox',dim,'String',str,'FontSize',32);
%limits along x & y axis
ylim([0 0.6]);
yl = ylim;
xlim([0 30]);
%Labels for axis
ylabelPlot{1}='Relative Frequency';
xlabelPlot='Distance to closest vessel (\mum)';
ylabel(ylabelPlot,'FontSize',32)
xlabel(xlabelPlot,'FontSize',32)
xt = get(gca, 'XTick');
set(gca, 'FontSize',32)
xt = get(gca, 'YTick');
set(gca, 'FontSize',32)

%set(gca,'XTick',[categories(2) categories(5) categories(7)]);
set(gca, ...
    'Box'         , 'off'     , ...
    'TickDir'     , 'out'     , ...
    'TickLength'  , [.02 .02] , ...
    'XMinorTick'  , 'on'      , ...
    'YMinorTick'  , 'on'      , ...
    'YGrid'       , 'on'      , ...
    'XColor'      , [.3 .3 .3], ...
    'YColor'      , [.3 .3 .3], ...
    'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
    'LineWidth'   , 1 );
%Save Figure
h_legend=legend('Target Shape','Estimated','Location','Northwest','FontSize');
set(h_legend,'FontSize',32)
print(fullfile([folderOut,'fit_agreement_c',type,'.eps']), '-depsc');
%--------------------------------------------------------------------------
%                    Save parameters
%--------------------------------------------------------------------------
save([folderOut,'params',type,'mat'],'b')