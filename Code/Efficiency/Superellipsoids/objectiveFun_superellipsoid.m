function f=objectiveFun_superellipsoid(r,x,y,z,Cx,Cy,Cz,a3,countsTarget,binCentersTarget,distanceCenter)
%Function to be minimized during optimization so as to find parameters that 
%define a superllispoid for which the distribution of extravascular distances best fits a target
%distribution. Here the function to minizmize is the root mean square error
%difference between the counts of the target histogram, and 
%the one that results from the superellipsoid
%
%Input:
%                   - r: array of size 1x3 containing parameters of the
%                   superellipsoid that are tested so as to find the
%                   optimal. r(1) corrsponds to the scale along x
%                   axis,r(2)& r(3) to e1,e2 respectively 
%                   that control the shape of the superellpsoid
%                   - x,y,z: coordinates of the grid for creating the
%                   superellispoid
%                   - Cx,Cy,Cz: coordinates for the center of the vessel
%                   and thus of the superllispoid
%                   - a3
%                   - countsTarget: counts of the target frequency histogram
%                   - binCentersTarget: centers of bins of the target
%                   histogram
%                   - distanceCenter: the distance transform of the volume 
%                   containing the central vessel, every voxel's value
%                   correspond to the closest distance to the vessel
%
%Output:
%                   - f: root mean square error between target and
%                   histogram resulting from the super-elliposid given by the 
%                   parameters r,a3
%
%Author: Xenia Gkontra, CNIC

%Ellipsoid to test
objEstimated=superelleipsoid([r(1) a3(1) a3(2)],r(2),r(3),x,y,z,Cx,Cy,Cz);

%Distances on estimated shape
distanceVol_Est=distanceCenter.*objEstimated;

%Create histogram
[countsEst binCentersEst] = hist(distanceVol_Est(distanceVol_Est>0),binCentersTarget);
countsEst=countsEst/sum(countsEst);

[r2 rmse] = rsquare (countsEst,countsTarget);

%Function to minimize
f=rmse^2;