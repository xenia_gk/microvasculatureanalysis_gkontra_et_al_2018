function [obj]=superelleipsoid(a,e1,e2,x,y,z,Cx,Cy,Cz)
%Furnction to create superellispoid. The shape of the superellispoid is
%controlled by e1, e2 (eg. e1=e2=1 shpere,ermember theory
%http://paulbourke.net/geometry/superellipse/) and the scale along the axis
%is given by a
%Input:
%                   - a: scale factors for each axis. In particular 
%                    a(1), a(2), a(3) correspond to x,y,z axis respectively
%                   - e1, e2:  control the shape of the superellpsoid. e1 acts as the "squareness" parameter in
%                     the z axis and n2 as the squareness in the x-y plane.
%                     From file:///C:/Users/pgkontra/Downloads/Fontes%20Portal.pdf
%                     e1 determines the shape of the superquadric cross section
%                     in a perpendicular plane to (x, y) and containing z, while e2 determines
%                     the shape of the cross section parallel to (x, y)
%                   - x,y,z: coordinates of the grid for creating the
%                   superellispoid
%                   - Cx,Cy,Cz: coordinates of the center of the shape
%Author: Xenia Gkontra, CNIC, 2016

obj= (  ( abs((x-Cx)/a(1)).^(2/e2)  +  (abs(y-Cy)/a(2)).^(2/e2)  ).^(e2/e1) + (abs(z-Cz)/a(3)).^(2/e1))<=1;