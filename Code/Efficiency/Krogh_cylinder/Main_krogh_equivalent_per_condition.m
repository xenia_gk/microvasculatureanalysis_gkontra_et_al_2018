%--------------------------------------------------------------------------
%Create a mean equivalent krogh cylinder and its corresponding
%distribution using the mean intercapillary distance, mean length and mean
%radius.
%The radius of the vessel is set equal to the mean radius found in the tissue 
%Length of the vessel and the corresponding cylidner is set equal the mean length
%Krogh radius is set equal to mean intercapillary distance (Ro-Ri is the max distance)
%
%Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clc
clear;
close all;
outputFolder='Results/Krogh/';
if ~exist(outputFolder)
mkdir(outputFolder)
end

%Call function to get the paths to be used by the module
directories=set_directories(GetFullPath('../..'));
%Load network metrics
dirSegment=directories.mainDir_Segment_Results;
load([dirSegment,'Res3.mat'])
%Set voxelsize
voxelSize=[1 1 1];
%Size of the cylinder to be used 100x100xlength
rows=70;
cols=70;
slices=50;
%Save images in  .mha format 
raw=1;
%--------------------------------------------------------------------------
%                       Basal
%--------------------------------------------------------------------------
type='Basal';
equivalent_krogh(type,prop_basal,outputFolder,rows,cols,slices,voxelSize,raw)
close all
%--------------------------------------------------------------------------
%                       1 day post MI
%--------------------------------------------------------------------------
type='R1MI';
equivalent_krogh(type,prop_remote1MI,outputFolder,rows,cols,slices,voxelSize,raw)
close all
type='I1MI';
equivalent_krogh(type,prop_infarct1MI,outputFolder,rows,cols,slices,voxelSize,raw)
close all
%--------------------------------------------------------------------------
%                       3 days post MI
%--------------------------------------------------------------------------
type='R3MI';
equivalent_krogh(type,prop_remote3MI,outputFolder,rows,cols,slices,voxelSize,raw)
close all
type='I3MI';
equivalent_krogh(type,prop_infarct3MI,outputFolder,rows,cols,slices,voxelSize,raw)
close all
%--------------------------------------------------------------------------
%                       7 days post MI
%--------------------------------------------------------------------------
type='R7MI';
equivalent_krogh(type,prop_remote7MI,outputFolder,rows,cols,slices,voxelSize,raw)
close all
type='I7MI';
equivalent_krogh(type,prop_infarct7MI,outputFolder,rows,cols,slices,voxelSize,raw)
close all