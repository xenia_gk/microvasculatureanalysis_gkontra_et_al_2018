function []=extravascular_distance_histogram_krogh(krogh_Radius,SDF1, outputFolder, name,titlePlots)
%Function to create plot of frequency histogram of 3D extavascular distance for
%krogh cylinder models.
%
%Input:
%               - krogh_Radius: radius of krogh cylinder
%               - SDF1: volume with distances at each point to the closest
%               vessel
%               - outputFolder: folder to save the results
%               - name: name to be used for saving the plot
%               - titlePlots: title to be used for the plot
%
%Output: 
%               - A plot (.eps) with name relativeFrequenceDistance_name is
%               saved with the frequency distribution calculated
%
%Author: Xenia Gkontra, CNIC

%--------------------------------------------------------------------------
%               Calculate histogram's bins
%--------------------------------------------------------------------------

data=SDF1([find(SDF1>0)]);
krogh_Radius1=krogh_Radius;
krogh_Radius=floor(krogh_Radius);
minSum = min(data)
maxSum = max(data)
%Histogram
numberOfBins = maxSum - minSum + 1
[counts binCenters] = hist(data, [1:krogh_Radius]); % No loop over i needed.
%Keep a copy of counts before normalizing
counts_Init=counts;
counts = counts / sum(counts);

%--------------------------------------------------------------------------
%               Plot histogram
%--------------------------------------------------------------------------
H=bar(binCenters, counts, 'BarWidth', 1);
set(H,'FaceColor',[0.5,0.5,0.5])
grid on;
% Enlarge figure to full screen.
set(gcf, 'units','normalized','outerposition',[0 0 1 1]);
hold on;
plot(binCenters, counts,'LineWidth',2,'Color','R')
%calculated slop of log(n(delta))/log(delta)
%Add annotation for the median (50% quartile) and maximal (90% quartile) extravascular distance 
dim = [0.49 0.54 0.41 0.2];
str{1,1}=['50% Quartile: ',num2str(prctile(binCenters(counts>0),50)),' \mu m']
str{2,1}=['95% Quartile: ',num2str(prctile(binCenters(counts>0),95)),' \mu m'];
str{3,1} = ['Krogh radius: ',num2str(krogh_Radius1),' \mu m'];
annotation('textbox',dim,'String',str,'FontName', 'Helvetica','FontSize',19);
ylim([0 0.32]);
yl = ylim;
xlim([0 30]);
%Add title 
if ~isempty(titlePlots)
    indIm=find(cellfun(@(x)ismember(x(1,1),str2num(name)),titlePlots(:,1)));
    title(titlePlots(indIm,2), 'FontName', 'Helvetica','FontSize',26)
end
ylabelPlot{1}='Relative Frequency';
xlabelPlot='Distance to closest vessel (\mu m)';
ylabel(ylabelPlot,'FontName', 'Avant Garde','FontSize',26)
xlabel(xlabelPlot,'FontName', 'Avant Garde','FontSize',26)
xt = get(gca, 'XTick');
set(gca, 'FontName', 'Avant Garde','FontSize',26)
xt = get(gca, 'YTick');
set(gca, 'FontName', 'Avant Garde','FontSize',26)
%Format for publication
set(gca, ...
    'Box'         , 'off'     , ...
    'TickDir'     , 'out'     , ...
    'TickLength'  , [.02 .02] , ...
    'XMinorTick'  , 'on'      , ...
    'YMinorTick'  , 'on'      , ...
    'YGrid'       , 'on'      , ...
    'XColor'      , [.3 .3 .3], ...
    'YColor'      , [.3 .3 .3], ...
    'YTick'       , yl(1):(yl(2)-yl(1))/5:yl(2), ...
    'LineWidth'   , 1 );

%--------------------------------------------------------------------------
%               Save results
%--------------------------------------------------------------------------
%saveas(gca,[outputFolder,'relativeFrequenceDistance_',name,'.png'])
%saveas(gca,[outputFolder,'relativeFrequenceDistance_',name,'.fig'])
print(fullfile([outputFolder,'relativeFrequenceDistance_',name,'.eps']), '-depsc');
%saveas(gca,[outputFolder,'relativeFrequenceDistance2_',name,'.eps'],'epsc')

save([outputFolder,name,'.mat'],'counts','numberOfBins','data','str')