function [cylinderVoxels,cylinderCenter]=createCylinder2(Nx,Ny,Nz,cyl,voxelSize)
%Equation to create a cylinder between two voxels of specific radius
%Input:         Nx,Ny,Nz: number of rows, columns, slices of the volume in
%               which the cylinder should be created
%               cyl: is of the form [x1 y1 z1 x2 y2 z2 r], where (x1,y1,z1)
%               are the coordinated of the first voxel, (x2,y2,z2) are the
%               coordinates of the second voxel and r is the radius that
%               should be used
%Output:        cylinderVoxels: binary volume of size [Nx,Ny,Nz] with 1 at
%               the voxels that belong to the cylinder
%
%Author: Xenia Gkontra, CNIC



%Extreme points/voxels of the cylinder
p1 = cyl(1:3);
p2 = cyl(4:6);
%Center of the cylinder
centerX=round(abs((p1(1)+p2(1)))/2);
centerY=round(abs((p1(2)+p2(2)))/2);
centerZ=round(abs((p1(3)+p2(3)))/2);
%Radius of cylinder
r = cyl(7);
beta=(p2(1)-p1(1));
alpha=(p2(2)-p1(2));
gamma=(p2(3)-p1(3))*2.6570;
ro=sqrt(alpha^2+beta^2+gamma^2);
%Create mesh grid
[X,Y,Z] = meshgrid(1:Nx,1:Ny,1:Nz);

%--------------------------------------------------------------------------
%           Infinite length cylinder between two points
%--------------------------------------------------------------------------
%From http://arstechnica.com/civis/viewtopic.php?f=26&t=1202255
%A = -x * sin(theta) * cos(phi) + y * cos(theta) - z * sin(theta) * sin(phi)
%B = -x * sin(phi) + z * cos(phi)
%Based on spherical coordinates
theta=asin(beta/ro);
phi=atan2(gamma,alpha);
A = -(X-centerY).*sin(theta).*cos(phi) + (Y-centerX).*cos(theta) - (Z-centerZ)*2.6570.*sin(theta).*sin(phi);
B = -(X-centerY).*sin(phi) + (Z-centerZ)*2.6570.*cos(phi);

%--------------------------------------------------------------------------
%           Crop cylinder to the specified length
%--------------------------------------------------------------------------
%http://tutorial.math.lamar.edu/Classes/CalcII/EqnsOfPlanes.aspx
%Based on: Equation of plane a.*x+b.*y+c.*z=d,normal to the plane
% N=(x2-x1,y2-y1,z2-z1)/sqrt((x2-x1).^2+(y2-y1).^2+(z2-z1).^2)
%So plane passing from point P1: d1=ax1+by1+cz1, plane passing from point P2:
%d2=ax2+by2+cz2 and all points inside the cylinder should give
%d1<a.*x+b.*x+c.*z<d2
ax=(p2(2)-p1(2))/ro;
by=(p2(1)-p1(1))/ro;
cz=(p2(3)-p1(3))/ro;
%Create volume with 1 in the voxels belonging to the cylinder
cylinderVoxels=double((A.^2+B.^2)<=r.^2&((ax.*X+by.*Y+cz.*Z)<(ax.*p2(2)+by.*p2(1)+cz.*p2(3)))&((ax.*X+by.*Y+cz.*Z)>(ax.*p1(2)+by.*p1(1)+cz.*p1(3))));
cylinderCenter=double(round(A.^2+B.^2)==0).*cylinderVoxels;
%Use it to note P1 point
%cylinderVoxels(p1(1),p1(2),p1(3))=5;
%Use it to note P2 point
%cylinderVoxels(p2(1),p2(2),p2(3))=5;
%Use to note the center of the cylinder
%cylinderVoxels(centerX,centerY,centerZ)=2;
