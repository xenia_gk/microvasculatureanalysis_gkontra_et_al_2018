function []=equivalent_krogh(type,prop_Basal,outputFolder,rows,cols,slices,voxelSize,raw)
%Function to create equivalent krogh cylinders given the radius, length of
%the vessel and intercapillary distance
%
%Input:
%               - type: name of image to be used for save the results
%               - prop_Basal: matrix of size (number of images)x(number of
%               metrics). To be used for mean radius (column 7), mean
%               length (column ), mean intercapillary distance(column 31)
%               - outputFolder: folder where to save the results
%               - rows,cols,slices: size of image volume (number of rows,
%               columns, slices) fro creating the vessel and the equivalent
%               krogh cylinder
%               - voxelSize: image resolution (size along x,y,z)
%               - raw: variable to indicate whether .mha images should be
%               saved (raw=1) or not (raw=0)
%Output:
%               - .mat file with the volume containing the vessel
%               (CylVol_Vessel), voxels of value 1 represent vessels
%               - .mat file with the volume containing the krogh cylinder
%               (CylVol_krogh)
%               - .mat file with the volume containing the distance of each
%               point inside the equivalent krogh cylinder to the vessel
%               (CylVol_SDF)
%               - .mha files with CylVol_Vessel, CylVol_krogh, CylVol_SDF
%               if it was asked (raw==1)
%
%Author: Xenia Gkontra, CNIC

%--------------------------------------------------------------------------
%                Characteristics of the vessel and 
%                the equivalent krogh cylinder
%--------------------------------------------------------------------------

%Mean radius: radius of the vessel
Ri=mean(prop_Basal(:,7));
%Mena length:length of the vessel and of the cylinder
l=mean(prop_Basal(:,15));
%Radius of the cylidnder
krogh_Radius=mean(prop_Basal(:,31));
%Normalize radii
krogh_Radius_voxel=krogh_Radius/voxelSize(1);
Ri=Ri/voxelSize(1);

%--------------------------------------------------------------------------
%                   Create the vessel
%--------------------------------------------------------------------------

%Create vessel
[CylVol_Vessel,CylCenter_Vessel]=createCylinder2(rows,cols,slices,[rows/2 cols/2 0 rows/2 cols/2 l Ri],voxelSize);
%Create the krogh cylinder
[CylVol_krogh,CylCenter_krogh]=createCylinder2(rows,cols,slices,[rows/2 cols/2 0 rows/2 cols/2 l krogh_Radius_voxel],voxelSize);

%--------------------------------------------------------------------------
%           Create the image with the cylinder and the vessel
%--------------------------------------------------------------------------

%Distance of each tissue point inside the cylinder to the vessel
SDF_CylCenter=bwdistsc(CylCenter_Vessel,voxelSize);
CylVol_SDF=double(CylVol_krogh>0).*SDF_CylCenter;

%--------------------------------------------------------------------------
%               Save results
%--------------------------------------------------------------------------

%Save in mat format
save([outputFolder,'\krogh_',type,'.mat'],'CylVol_Vessel','CylVol_krogh','CylVol_SDF');
%If the images should be saved in .mha format (visualization with
%paraview for example )
if raw==1
    SCIMAT = scimat_im2scimat(double(CylVol_krogh>0)+2*double(CylVol_Vessel>0));
    scirunnrrd = scimat_save([outputFolder,'\CylVol_',type,'.mha'],SCIMAT);
    SCIMAT = scimat_im2scimat(double(CylVol_krogh>0)+2*double(CylVol_Vessel>0));
    scirunnrrd = scimat_save([outputFolder,'\CylVol_Vessel_',type,'.mha'],SCIMAT);
    SCIMAT = scimat_im2scimat(CylVol_SDF);
    scirunnrrd = scimat_save([outputFolder,'\CylVol_SDF',type,'.mha'],SCIMAT);
    extravascular_distance_histogram_krogh(krogh_Radius,CylVol_SDF, outputFolder, type,[])
end