function [prop_remote,prop_infarct,prop_remoteXMI,prop_infarctXMI,subj_remote,subj_infarct,subj_remoteXMI,subj_infarctXMI,prop_basal,subj_basal] = measures_2(filenames,mainDir_Denois,mainDir,mainDir_Art,mainDir_2D,prop_remote,prop_infarct,Subjects,subj_remote,subj_infarct,prop_basal,subj_basal)
%Function to process all volumes of a particular time-point so as to calculate the
%fractal and minkowski-based parameters and return updated matrices with
%the parameters calculated for each volume processed.
%
%Input:
%               - filenames: names of images to be processed
%               - mainDir_Denois: directory where the segmentations of SMA
%               channel are saved
%               - mainDir: directory where the previously calcualted
%               metrics and segment level and related and properties
%               necessary for calculating more metrics at segment level,
%               SMA and capillary suplly regions are saved
%               - prop_remote: matrix where the parameters for every volume
%               are saved if the tissue from which they were acquired
%               belongs to remote areas independently of the time point.
%               Evey row correspond to a different volume and every column
%               to a particular metric
%               - prop_infarct: matrix where the parameters for every volume
%               are saved if the tissue from which they were acquired
%               belongs to infarcted areas independently of the time point.
%               Evey row correspond to a different volume and every column
%               to a particular metric.
%               - prop_basal: matrix where the parameters for every volume
%               are saved if the tissue from which they were acquired
%               belongs to basal areas.Evey row correspond to a different
%               volume and every column to a particular metric.
%               - Subjects: matrix of size (number of images)x2. The second
%               column corresponds to the image name and the first column
%               to the subject's name/number from which the image was taken
%               - subj_remote: matrix to save the subject's name for every
%               volume from remote area being processed. Each row
%               of subj_remote refers to the same row at prop_remote
%               - subj_infarct: matrix to save the subject's name for every
%               volume from remote area being processed. Each row
%               of subj_remote refers to the same row at prop_remote
%               - subj_basal: matrix to save the subject's name for every
%               volume from basal area being processed. Each row
%               of subj_remote refers to the same row at prop_remote
%               - mainDir_Art: directory where the volume indicating
%               possible arterioles or venules regions are saved
%               - mainDir_BW: directory where the reconstructed
%               microvasculature is saved (BW)
%
%
%Output:
%               - prop_remote,prop_infarct, subj_remote,subj_infarct,
%               prop_basal,subj_basal: Input matrixes updated by the values
%               for the volumes being processed (ie the ones in filenames)
%               during the call of the function
%               -prop_remoteXMI,prop_infarctXMI: matrix of size (number of
%               volumes)x(number of parameters). If volumes belonging to a
%               time-point after MI were processed during the call of the
%               function then the parameters for each volume are saved
%               on prop_remoteXMI,prop_infarctXMI depending on whether the
%               volume belongs to a remote or infarcted area. Please not
%               that each row refers to a volume and each column to a
%               parameter. If the volumes being processed where from
%               infarcted areas, those matrices will be empty.
%               - subj_remoteXMI: array of size (number of
%               volumes). If volumes belonging to a
%               time-point after MI were processed during the call of the
%               function then each row i contains the subject's name/number
%               to which the volume whose paraemeters are saved at the row i
%               of prop_remoteXMI
%               - subj_infarctXMI: similar to subj_remoteXMI but for
%               infarcted volumes
%
%Author: Xenia Gkontra, CNIC

%Initialize
prop_remoteXMI=[];
prop_infarctXMI=[];
subj_remoteXMI=[];
subj_infarctXMI=[];

for i=3:length(filenames)
    
    %Image name
    name=filenames(i).name;
    %Tissue category
    [tissueCat]=define_tissueCat(name);
    %Input directory
    directoryIn=[mainDir,tissueCat,'/',name,'/'];
    
    %Load metrics calculated previously (Code/GraphBased_SMA_Metrics/)
    load ([directoryIn,'RXMI6_',name,'.mat']);
    y1=p_net2;
    %Load skeleton
    data=load([directoryIn,'NoBubblesSkel2_',name,'.mat']);
    [Nx,Ny,Nz]=size(data.skel_NoBubbles);
    
    %Define voxel size and volume
    [voxelSize,volume_voxel]=define_voxelSize(name);
    %Convert voxel volume to mm^3
    volume_voxel=volume_voxel*(10^(-18));
    
    %Tissue volume in mm^3
    tissue_volume=volume_voxel*Nx*Ny*Nz;
    
    %Branch points per tissue volume
    branchPoints_TV=y1(1)/((tissue_volume));
    %Vascular segments per tissue volume
    segments_TV=y1(2)/((tissue_volume));
    %End-points (blind-ends/sprouts) per tissue volume
    endPoints_TV=y1(4)/((tissue_volume));
    
    
    %----------------------------------------------------------------------
    %           SMA related metrics
    %----------------------------------------------------------------------
    
    %Characteristcs of vessels covered with sma or without coverage. If they where
    %calculated druing the call of
    %Code/Graph-Based_SMA/Main_Graph_SMA_Analysis.m p_SMA_Cov and
    %p_SMA_UnCov will exist
    %Load properties of vessels  covered with sma
    if exist('p_SMA_Cov','var')
        y3=p_SMA_Cov;
    else
        y3=zeros(1,13);
    end
    %Load properties of vessels not covered with sma
    if exist('p_SMA_UnCov','var')
        y4=p_SMA_UnCov;
    else
        y4=zeros(1,13);
    end
    
    %Percentage of vessels covered with SMA
    y5=(size(p_net{1},1))*100/(size(p_net{1},1)+size(p_net{2},1));
    %Mean thickness of sma coverage
    y6=mean(SMA_thick);
    
    %Radius of vessels covered with SMA
    r1=p_net{1}(:,3);
    %Radius of vessels not covered with SMA
    r2=p_net{2}(:,3);
    
    %According to separation of capillaries into 3 classes classes based on
    %the morphological data of Kassab et al., 1994.:
    %(i) segments with diameter smaller or equal to $6.9$ $\mu m$ corresponding to $C_{cc}$ and $C_{oo}$ capillaries (smallest microvessels),
    %(ii) segments with diameter between $6.9$ and $8.2$ $\mu m$ corresponding to $C_{oa}$ and $C_{ov}$ capillaries (medium sized microvessels),
    %and (iii) segments with diameter larger than $8.2$ $\mu m$ representing arterioles or venules.
    
    %Capillaries of the two classes covered with SMA
    capSMA=find(r1<=(8.2/2));
    %Capillaries of the two classes  not covered with SMA
    capNotSMA=find(r2<=(8.2/2));
    %Larger vessels covered with SMA
    largSMA=find(r1>(8.2/2));
    largNotSMA=find(r2>(8.2/2));
    
    %Percentage of capillaries covered with SMA
    capillaries_SMA=length(capSMA)/(length(capSMA)+length(capNotSMA));
    %Total Number of larger vessels covered with SMA
    largeVessels_SMA=length(largSMA)/(length(largSMA)+length(largNotSMA));
    
    %Thickness of SMA of the capilalries
    capillaries_Thick=mean(SMA_thick(capSMA));
    %Thickness of SMA of the larger vessels
    largeVessels_Thick=mean(SMA_thick(largSMA));
    
    
    %Damage index for capillaries & for the complete microvasculature
    sma_Capillaries=sma_cap(1);
    sma_Arterioles=sma_cap(2);
    % sma_Fibro=sma_cap(3);
    load([mainDir_Denois,name,'/extras/SegmSMA_',name,'.mat'])
    
    sma_Fibro=sum(sum(sum(BWCh3-output)));
    %Damage index if we take into account only capillaries
    c_damage=sma_Fibro/(sma_Capillaries+sma_Fibro);
    %Damage index capilalries + large vessels
    damage=sma_Fibro/(sma_Capillaries+sma_Fibro+sma_Arterioles);
    y2=[damage c_damage] ;
    
    
    %Thickness of SMA in the 3 classes of vessels
    diameter1=2*r1;
    %Thickness of SMA taking into account only the smallest capillaries
    Small_Thick=mean(SMA_thick(find(diameter1<=6.9)));
    %Thickness of SMA taking into account only the medium sized vessels
    Medium_Thick=mean(SMA_thick(find(diameter1<=8.2&diameter1>6.9)));
    %Thickness of SMA taking into account only the larger vessels
    Large_Thick=mean(SMA_thick(find(diameter1>8.2)));
    
    %----------------------------------------------------------------------
    %        Percentages and numbers for the three classes of vessels
    %----------------------------------------------------------------------
    
    %Diamater of all vessel segment
    diameter=2*[r1;r2];
    %Number of all vessels
    numVessels=length(diameter);
    %Number of vessels with diameter smaller than 6.9, ie the smallest
    %vessels
    numSmall=length(find(diameter<=6.9));
    %Number of vessels with diameter in the range 6.9 and 8.2, ie the
    %medium sized vessels
    numMedium=length(find(diameter<=8.2&diameter>6.9));
    %Number of vessels with diameter larger than 8.2, ie the larger vessels
    %like arterioles/venules
    numLarge=length(find(diameter>8.2));
    
    %Precentages of vessels with specific radius
    perSmall=numSmall*100/numVessels;
    perMedium=numMedium*100/numVessels;
    perLarge=numLarge*100/numVessels;
    %Number per tissue volume
    numTissueSmall=numSmall/tissue_volume;
    numTissueMedium=numMedium/tissue_volume;
    numTissueLarge=numLarge/tissue_volume;
    
    %----------------------------------------------------------------------
    %                  Cell related metrics
    %----------------------------------------------------------------------
    %Number of endothelial cells
    num_endothelialCells=cell_metrics(1)/(tissue_volume);
    %Number of sma+ cells that are perivascualr
    nun_Nuclei_SMATouch=cell_metrics(2)/(tissue_volume);
    %Numebr of myofibroblasts
    nun_Nuclei_fibro=cell_metrics(3)/(tissue_volume);
    
    
    %----------------------------------------------------------------------
    %                   Load 2D metrics
    %---------------------------------------------------------------------
    %    load ([mainDir_2D,tissueCat,'/',name,'/ExtraMetrics_B_',name,'.mat']);
    load ([mainDir_2D,tissueCat,'/',name,'/ExtraMetrics_B_',name,'.mat']);
    %Number of capillaries/area in voxels
    capillaryDensityRatio=mean([capillaries_Number./region]);
    cap_Density(isinf(cap_Density))=[];
    %Inetrcapillary distance (mean along the 5 slices for which it was
    %calculated)
    intercapillaryDistance=mean(dMinAll);
    %Capilalry density (mean along the 5 slices for which it was
    %calculated)
    capillaryDensity=mean(cap_Density);
    
    %----------------------------------------------------------------------
    %               Metrics per vascular volume
    %----------------------------------------------------------------------
    data_net=load([directoryIn,'net8_',name,'.mat']);
    BW=data_net.BW;
    vascular_volume=volume_voxel*sum(BW(:));
    %   branchPoints_VS=y1(1)/((vascular_volume));
    %   segments_VS=y1(2)/((vascular_volume));
    %   endPoints_VS=y1(4)/((vascular_volume));
    %Number of cells per vascular volume
    num_endothelialCells_VS=cell_metrics(1)/(vascular_volume);
    nun_Nuclei_SMATouch_VS=cell_metrics(2)/(vascular_volume);
    nun_Nuclei_fibro_VS=cell_metrics(3)/(vascular_volume);
    
    %----------------------------------------------------------------------
    %           Metrics per vascular length
    %----------------------------------------------------------------------
    %Vascular length
    vascular_len = sum(sum(p_net{1}(:,2))+sum(p_net{2}(:,2)));
    %Length in mm
    vascular_length=vascular_len*(10^-3);
    %    branchPoints_VL=y1(1)/((vascular_length));
    %    segments_VL=y1(2)/((vascular_length));
    %    endPoints_VL=y1(4)/((vascular_length));
    %Cells per mm
    num_endothelialCells_VL=cell_metrics(1)/(vascular_length);
    nun_Nuclei_SMATouch_VL=cell_metrics(2)/(vascular_length);
    nun_Nuclei_fibro_VL=cell_metrics(3)/(vascular_length);
    
    %----------------------------------------------------------------------
    %                   Metrics only for Capillaries
    %----------------------------------------------------------------------
    dataArt=load([mainDir_Art,'/',tissueCat,'/',name,'/Arterioles_',name,'.mat']);
    Vfiltered=dataArt.Vfiltered>0;
    node=data_net.node;
    link=data_net.link;
    %Number of end-points
    %num4=length(find(arrayfun(@(x)length(x.links),node)==1));
    %Nodes that are not on border along x-axis
    x_NotBorder=~cellfun('isempty',(arrayfun(@(x)find(round(x.comx)>9&round(x.comx)<(Nx-8)),node,'UniformOutput',0)));
    %Nodes that are not on border along y-axis
    y_NotBorder=~cellfun('isempty',(arrayfun(@(x)find(round(x.comy)>9&round(x.comy)<(Ny-8)),node,'UniformOutput',0)));
    %Nodes that are not on border along z-axis
    z_NotBorder=~cellfun('isempty',(arrayfun(@(x)find(round(x.comz)>3&round(x.comz)<(Nz-2)),node,'UniformOutput',0)));
    %Total number of end nodes that are on Capillaries
    Capillaries=(1-Vfiltered).*BW;
    endNodes=~cellfun('isempty',(arrayfun(@(x)find(length(x.links)==1&Capillaries(x.idx)==1),node,'UniformOutput',0)));
    %Branch nodes in capillaries
    branchPoints_cap=length(find(~cellfun('isempty',(arrayfun(@(x)find(length(x.links)>1&Capillaries(x.idx)==1),node,'UniformOutput',0)))));
    %End-nodes that are not in Borders
    endPoints_cap=length(find(x_NotBorder==1&y_NotBorder==1&z_NotBorder==1&endNodes==1));
    capil_links=find(~cellfun('isempty',(arrayfun(@(x)find(Capillaries(x.point(1))==1),link,'UniformOutput',0))));
    segments_cap=length(capil_links);
    
    cap_vol=sum(Capillaries(:))*volume_voxel;
    branchPoints_C_VS=branchPoints_cap/(cap_vol);
    segments_C_VS=segments_cap/(cap_vol);
    endPoints_C_VS=endPoints_cap/(cap_vol);
    
    cp_labels1=find(ismember(labels1,capil_links));
    cp_labels2=find(ismember(labels2,capil_links));
    %Length capillaries
    capillaries_length=sum(sum(p_net{1}(cp_labels1,2))+sum(p_net{2}(cp_labels2,2)));
    capillaries_length=capillaries_length*(10^-3);
    branchPoints_C_VL=branchPoints_cap/(capillaries_length);
    segments_C_VL=segments_cap/(capillaries_length);
    endPoints_C_VL=endPoints_cap/(capillaries_length);
    
    %----------------------------------------------------------------------
    %                  Extravascular distances
    %----------------------------------------------------------------------
    %Calculating 3D maps of extravascular distances might some time,
    %so if was already caclauted avoid recalculating it
    if exist([directoryIn,'SDF_',name,'.mat'])
        load([directoryIn,'SDF_',name,'.mat'])
    else
        SDF=bwdistsc(BW,voxelSize);
        save([directoryIn,'SDF_',name,'.mat'],'SDF')
    end
    %Calculate mean and max extravascular distance
    extravascularDistance=SDF([find(SDF>0)]);
    minSum = min(extravascularDistance);
    maxSum = max(extravascularDistance);
    numberOfBins = maxSum - minSum + 1;
    [counts,binCenters] = hist(extravascularDistance, numberOfBins); % No loop over i needed.
    %Median & maximal extravascular distance taking into account the bins
    %of the frequency distribution
    exDist(1)=prctile(binCenters(counts>0),50);
    exDist(2)=prctile(binCenters(counts>0),95);
    %Median & maximal extravascular distance taking all points intoa ccount
    %and not their distribution
    exDist(3)=(prctile(extravascularDistance,50));
    exDist(4)=(prctile(extravascularDistance,95));
    %----------------------------------------------------------------------
    %                   Save results
    %----------------------------------------------------------------------
    all_metrics=[branchPoints_TV segments_TV y1(3) endPoints_TV y1(5:end),...
        y2 y5 y6 capillaries_SMA largeVessels_SMA capillaries_Thick largeVessels_Thick capillaryDensityRatio intercapillaryDistance capillaryDensity y3 y4,...
        numSmall numMedium numLarge perSmall perMedium perLarge numTissueSmall numTissueMedium numTissueLarge,...
        num_endothelialCells nun_Nuclei_SMATouch nun_Nuclei_fibro Small_Thick Medium_Thick Large_Thick,...
        branchPoints_C_VS segments_C_VS endPoints_C_VS num_endothelialCells_VS nun_Nuclei_SMATouch_VS nun_Nuclei_fibro_VS,...
        branchPoints_C_VL segments_C_VL endPoints_C_VL num_endothelialCells_VL nun_Nuclei_SMATouch_VL nun_Nuclei_fibro_VL exDist];
    
    if ~isempty(find(strcmp(tissueCat,'Remote'),1))   %If the volume under ivestigation was taken from remote tissue
        prop_remote=[prop_remote;all_metrics];
        prop_remoteXMI=[prop_remoteXMI;all_metrics];
        %Find row corresponding to this particular image
        indx=find(ismember(Subjects(:,1),name));
        %Save the subject number
        subj_remoteXMI=[subj_remoteXMI;Subjects{indx,2}];
        subj_remote=[subj_remote;Subjects{indx,2}];
    elseif ~isempty(find(strcmp(tissueCat,'Infarct'),1))  %If the volume under ivestigation was taken from infarcted tissue
        prop_infarct=[prop_infarct;all_metrics];
        prop_infarctXMI=[prop_infarctXMI;all_metrics];
        %Find row corresponding to this particular image
        indx=find(ismember(Subjects(:,1),name));
        %Save the subject number
        subj_infarctXMI=[subj_infarctXMI;Subjects{indx,2}];
        subj_infarct=[subj_infarct;Subjects{indx,2}];
    else %If the volume under investigation was taken from basal tissue
        prop_basal=[prop_basal;all_metrics];
        %Find row corresponding to this particular image
        indx=find(ismember(Subjects(:,1),name));
        %Save the subject number
        subj_basal=[subj_basal;Subjects{indx,2}];
    end
end


