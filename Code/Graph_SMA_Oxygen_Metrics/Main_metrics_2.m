%--------------------------------------------------------------------------
%Function to calculate and gather metrics at the segment level extracted
%from the graph-based representation of the microvasculature as well as
%metrics related to SMA+ cells and efficiency in oxygen diffusion
%
%Note!!1. It is expected that main calculations regarding the 
%aforementioned features have already been performed in previous steps. 
%Here those previous metrics are put together  and enriched
%with addiotional metrics. In particular, the following MAT-Files are 
%expected to have been already created:
%                   - RXMI6_imageName.mat with aegment-level parameters
%                    calculated by module at Code/Graph-Based_SMA/
%                   - net8_imageName.mat with skeleton-related parameters
%                   such as links, nodes calcultaed by module at
%                   Code/Graph-Based_SMA/
%                   -ExtraMetrics_B_imageName.mat with parameters based on
%                   2d approaches traditionally related to oxygen diffusion
%                   calculated by module at Code/Efficiency/Metrics2D/
%                   - Arterioles_imageName.mat: 3D volumes with areas
%                   that might belong to arterioles/venules calculated
%                   by module at Code/SMA
%                   - BW_imageName.mat with the 3D reconstructed
%                   microvasculature after application of the filling
%                   pipeline
%                   - SegmSMA_imageName.mat with the 3D segmentation of the 
%                   SMA channel
%2. If you have a different number of time-points than in the original case
%remember to change accordingly lines 127-134
%
%Output:
%                   (as with Code/Structure_level_Metrics/Main_metrics_1.m)
%                   - a .mat file is saved with name Res3.mat in
%                   outputFolder. The file saves one matrix per tissue condition
%                   and time point (prop_infarct1MI, prop_remote1MI,
%                   prop_infract3MI, prop_infarct7MI, prop_remote7MI,
%                   prop_basal) and two more for infarcted tissue and
%                   remote independently of the day post MI (prop_remote,
%                   prop_infarct) with the calcuated results.  The matrices are of
%                   size (number of samples)x(number of parameters), ie
%                   every row corresponds to a volume that has been
%                   analyzed and every column to a parameter. Furthremore,
%                   an array per tissue condition and time point
%                   (subj_infarct1MI, subj_remote1MI, subj_infract3MI,
%                   subj_infarct7MI, subj_remote7MI, subj_basal) as well as
%                   one for infarcted tissue and one for
%                   remote independently of the day post MI (subj_remote,
%                   subj_infarct) with the results is saved in Res3.mat.
%                   Each of those arrays contains in every row i the
%                   subject's number from which the image
%                   volume at row i or the corresponding
%                   prop_remote/prop_infarct/prop_basal was obtained
%                   (e.g subj_remote7MI(i) indicates the subject's number
%                   from which the volume whose parameters are saved in
%                   prop_remote7MI(i,:) was obtained)
%                  	- SDF_imageName.mat: MAT-File with 3D volumes with
%                   extravascular distances (every voxel�s value corresponds
%                   to the distance to the closest vessel) saved at the
%                   image's corresponding folder under Code/Graph-Based_SMA/Results
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clf;
clear;
close all;

%--------------------------------------------------------------------------
%                   Directories & Initialize
%--------------------------------------------------------------------------
%Call function to load the directories
directories=set_directories(GetFullPath('..'));
%Directory to metrics at segment level & metrics related to sma+ that have
%been already calculated
mainDir_Mertics=directories.mainDir_Segment;
%Directory for metrics extacted from 2D slices traditionally related to
%oxygen diffusion
mainDir_2D=directories.mainDir_2D;
%The images in this directory are not ordered by tissue category
mainDir=directories.mainDir;
%Directory where 3D volume with possible arterioles/venules areas are saved
mainDir_Art=directories.mainDir_Art;
%List time-points after MI to be studied. Please note first two folders
%listed by dir are '.' and '..' so we ignore them in the next steps
daysMI_All=dir(mainDir);

%Chack if the folder exists, else create it
folderOut=directories.mainDir_Segment_Results;
if ~exist(folderOut)
    mkdir(folderOut);
end

%Initialize
prop_remote=[];prop_remote1MI=[];prop_remote3MI=[];prop_remote7MI=[];
prop_infarct=[];prop_infarct1MI=[];prop_infarct3MI=[];prop_infarct7MI=[];
prop_basal=[];
subj_remote=[];subj_remote1MI=[];subj_remote3MI=[];subj_remote7MI=[];
subj_infarct=[];subj_infarct1MI=[];subj_infarct3MI=[];subj_infarct7MI=[];
subj_basal=[];

%Matrix with column 1 the subject number and column 2 the image name. Note
%that this was added just for other comparisons between subjects etc not
%presented here, you can just put the same subjects for all volumes if you
%are not interested in sorting by subject in later steps
[Subjects]=image_to_subject_correspondance();

%--------------------------------------------------------------------------
%                 Calculate & Gather metrics
%--------------------------------------------------------------------------
for j=3:length(daysMI_All)
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    %Find images in the directory
    directoryIn=[mainDir,'/',daysMI,'/'];
    filenames=dir(directoryIn);
    %Directory for reconstructed microvasculature after the application of
    %the filling pipeline
    directory2D=[mainDir_2D,daysMI,'/'];
    %Directory where the arterioles are
    directoryArt=[mainDir_Art,daysMI,'/'];
    %Directory where the fractal-based metrics calculated in the previous
    %step are saved
    directoryMertics=[mainDir_Mertics,daysMI,'/'];
    %Load and calculate properties
    %Change the following lines accordingly to your time-points after MI if
    %this is not the same as in our case.
    if j==3
        [prop_remote,prop_infarct,prop_remote1MI,prop_infarct1MI,subj_remote,subj_infarct,subj_remote1MI,subj_infarct1MI,prop_basal,subj_basal] = measures_2(filenames,directoryIn,directoryMertics,directoryArt,directory2D,prop_remote,prop_infarct,Subjects,subj_remote,subj_infarct,prop_basal,subj_basal);
    elseif j==4
        [prop_remote,prop_infarct,prop_remote3MI,prop_infarct3MI,subj_remote,subj_infarct,subj_remote3MI,subj_infarct3MI,prop_basal,subj_basal] = measures_2(filenames,directoryIn,directoryMertics,directoryArt,directory2D,prop_remote,prop_infarct,Subjects,subj_remote,subj_infarct,prop_basal,subj_basal);
    elseif j==5
        [prop_remote,prop_infarct,prop_remote7MI,prop_infarct7MI,subj_remote,subj_infarct,subj_remote7MI,subj_infarct7MI,prop_basal,subj_basal] = measures_2(filenames,directoryIn,directoryMertics,directoryArt,directory2D,prop_remote,prop_infarct,Subjects,subj_remote,subj_infarct,prop_basal,subj_basal);
    elseif j==6
        [~,~,~,~,~,~,~,~,prop_basal,subj_basal] =  measures_2(filenames,directoryIn,directoryMertics,directoryArt,directory2D,prop_remote,prop_infarct,Subjects,subj_remote,subj_infarct,prop_basal,subj_basal);
    end
end

%Save results
save([folderOut,'/Res3.mat'],'subj_basal','subj_remote7MI','subj_infarct7MI','subj_remote1MI','subj_infarct1MI','subj_remote3MI','subj_infarct3MI','prop_basal','prop_remote','prop_infarct','prop_infarct1MI','prop_remote1MI','prop_infarct3MI','prop_remote3MI','prop_infarct7MI','prop_remote7MI')

