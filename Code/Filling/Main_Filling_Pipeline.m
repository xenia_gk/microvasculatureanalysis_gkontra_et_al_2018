%--------------------------------------------------------------------------
%Main function for applying the filling pipeline on top of the VE-CAdherin
%segmentation in order to fill the gaps in the microvasculature produced by
%VE-Cadherin fluorochrome(as it only stains the endothelial junctions) using
%information regarding the position of arterioles/venules and capillaries (
%3D Guidance map for filling)
%
%Input:
%           - Directories (mainDir) where the
%           segmentations of the VE-Cadherin channel are saved
%           - Directory where the Arterioles estimation is saved
%
%Output:
%
%           - BW_imageName: filled volume saved at folderOut (ie
%           Code/Filling/Results)
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clear;
close all;
%--------------------------------------------------------------------------
%                       Directories
%--------------------------------------------------------------------------
%Call function to load the directories
directories=set_directories(GetFullPath('..'));

%Input directory where the denoised 3d image channels and the segmentation
%of the VE-Cadherin channel can be found
mainDir=directories.mainDir;


%List time-points after MI to be studied. Please note first two folders
%returned by Matlab's "dir" function are '.' and '..' so we ignore them
daysMI_All=dir(mainDir);

%Directory where 3D volume with possible arterioles/venules areas are saved
arteriolesDir=directories.mainDir_Art;

%Output directory for saving the 3D microvasculature after the application
%of the filling pipeline
folderOut=directories.mainDir_Filled;
%Check if folder exists, else create it
if ~exist(folderOut)
    mkdir(folderOut);
end;


%--------------------------------------------------------------------------
%                         Filling Pipeline
%--------------------------------------------------------------------------
for j=3:length(daysMI_All)
    
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    %Input directory
    directoryIn=[mainDir,daysMI,'/'];
    %Find images in the directory
    filenames=dir(directoryIn);
    %Output directory
    dirOut=[folderOut,daysMI,'/'];
    
    %Images files for 1MI
    for i=3:length(filenames)
        %Tissue condition: 'Remote','Infarct','Basal'
        tissue_cat=define_tissueCat(filenames(i).name);
        %Directory for arterioles/venules
        artDirIn=[arteriolesDir,daysMI,'/',tissue_cat,'/'];
        %Output directory for the current image
        folderRaw=[dirOut,tissue_cat,'/',filenames(i).name,'/'];
        %Check whether the folder exist
        if ~exist(folderRaw)
            mkdir(folderRaw)
        end
        %Segmentation refined by Nuclei and Arterioles
        load([directoryIn, filenames(i).name,'/extras/SegmMMT_',filenames(i).name,'.mat']);
        load([artDirIn, filenames(i).name,'/Arterioles_',filenames(i).name,'.mat']);
        %Voxel Size
        [voxelSize,Voxel_volume]=define_voxelSize(filenames(i).name);
        tic
        %Guidance map equals 1 for possible capillaries's regions, equals 2
        %for possible arterioles/venules regions
        Guidance=BWCh2.*(Vfiltered>0)+BWCh2;
        %Fill gaps of the microvasculature
        [CloseImg]=filling_different_SE_3D(Guidance,voxelSize);
        BW=CloseImg>0;
        save([folderRaw,'BW_',filenames(i).name,'.mat'],'BW')
    end
    
end
