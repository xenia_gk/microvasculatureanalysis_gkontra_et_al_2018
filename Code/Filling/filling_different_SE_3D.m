function [CloseImg]=filling_different_SE_3D(Guidance,voxelsize)
%Function for filling with different structural elements depeding on which
%region we are. Precisely, where the vasculature corresponds to
%arterioles/venules then the filling used is which corresponds to the
%smaller diameter found, while the regions corresponds to capilalaries a
%smaller structural element is used. All operations are performed in 3D
%
%Input: 
%                   -Guidance: guidance map, where Guidance=1 indicates a
%                   capillaries area, while 2 indicates a capillaries area
%                   - voxelsize: size of voxel in microns
%
%Output:    
%                   - CloseImg: resuling image after filling with the
%                   different structural elements
%
%Author: Xenia Gkontra, CNIC, 2016



%--------------------------------------------------------------------------
%                   Arterioles region
%--------------------------------------------------------------------------
%Structural element for arterioles
%Smallest arteriole found
r=(20.6/2)/voxelsize(1); %voxels
z_ratio=voxelsize(3)/voxelsize(1); %ratio of voxel along z 
[x,y,z]=meshgrid(-r:r,-r:r,-r/z_ratio:r/z_ratio);
se1 = strel( ( (x/r).^2 + (y/r).^2 + (z/(r/z_ratio)).^2 ) <= 1);
%Closing 
CloseImg1=imclose((Guidance==2),se1);

%--------------------------------------------------------------------------
%               Capillaries region
%--------------------------------------------------------------------------
%Structural element for capillaries
%The sphere should have a diameter equal to the thicker, ie of largest
%diameter, capillary. Using kassab et. al, 1994 Table 2, the diameter is 
%equal to 8.2microns, so radius half
r=(8.2/2)/voxelsize(1); %voxels
z_ratio=voxelsize(3)/voxelsize(1); %ratio of voxel along z 
[x,y,z]=meshgrid(-r:r,-r:r,-r/z_ratio:r/z_ratio);
se2 = strel( ( (x/r).^2 + (y/r).^2 + (z/(r/z_ratio)).^2 ) <= 1);
%Closing
CloseImg2=imclose((Guidance==1),se2);

%--------------------------------------------------------------------------
%                   Unite the 2 regions
%--------------------------------------------------------------------------
CloseImg=double(CloseImg1>0)+2*double(CloseImg2>0);
