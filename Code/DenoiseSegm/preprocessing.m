function [] = preprocessing(dirIn,dirOut,folderExtras,type,denoise,raw)
%Function for reading original tiff images, calling non-local means
%filtering of Buades and saving the results in .mat format as well as .mha
%if desirable
%
%Input:         
%               -dirIn: directory where the original image to be processed
%               is saved in .tif format
%               -dirOut: directory where the results should be saved
%               -denoise: specify whether denoised should be performed or
%               not: 1-> denoised should be performed/0 -> denoising
%               has already been performed just load the denoised image
%               volumes
%               -raw:whether to save or the images in .mha format as well
%
%Output:
%               -Original and denoised images are save in .mat format at
%               Results\daysMI\filename\extras
%               -Original and denoised images are save in .mha format at
%               Results\daysMI\filename\extras
%
%Author: Xenia Gkontra, CNIC

%Folder for saving .raw images
folderRaw=[dirOut,'raw/'];
%Check if folder exists, otherwise create it
if ~exist(folderRaw)
    mkdir(folderRaw);
end

%--------------------------------------------------------------------------
%             STEP 1: Load images 
%--------------------------------------------------------------------------
%Load images
[Channel0, Channel2, Channel3,num_files2] = LoadImageFiles(dirIn);
%Save original images
save([folderExtras,'Original_',type,'.mat'],'Channel0', 'Channel2', 'Channel3');

%--------------------------------------------------------------------------
%        STEP 2: Denoise and save .mat files (and if .mha if asked)
%--------------------------------------------------------------------------

%Preprocess if denoise==1
%Denoising with non-local means filtering
if denoise==1
    %Parameters
    Options.verbose=true;
    Options.blocksize=45;
    %--Channel 0
    D=squeeze(Channel0(:,:,:)); D=single(D); D=D./max(D(:));
    V=NLMF(D,Options);
    Channel0_noisy=D;
    Channel0=V;
    
    %---Channel2
    D=squeeze(Channel2(:,:,:)); D=single(D); D=D./max(D(:));
    V=NLMF(D,Options);
    Channel2_noisy=D;
    Channel2=V;
    
    %--Channel3
    D=squeeze(Channel3(:,:,:)); D=single(D); D=D./max(D(:));
    V=NLMF(D,Options);
    Channel3_noisy=D;
    Channel3=V;
    %Save results
    save([folderExtras,'NL_',type,'.mat'],'Channel3_noisy','Channel3','Channel2_noisy','Channel2','Channel0_noisy','Channel0')
elseif denoise==2
    load([folderExtras,'NL_',type,'.mat']);
end

%Save .mha images
if raw==1
    SCIMAT = scimat_im2scimat(Channel0);
    scirunnrrd = scimat_save([folderRaw,'Original_Ch0_',type,'.mha'],SCIMAT);
    SCIMAT = scimat_im2scimat(Channel2);
    scirunnrrd = scimat_save([folderRaw,'Original_Ch2_',type,'.mha'],SCIMAT);
    SCIMAT = scimat_im2scimat(Channel3);
    scirunnrrd = scimat_save([folderRaw,'Original_Ch3_',type,'.mha'],SCIMAT);
    SCIMAT = scimat_im2scimat(Channel0);
    scirunnrrd = scimat_save([folderRaw,'Denoised_Ch0_',type,'.mha'],SCIMAT);
    SCIMAT = scimat_im2scimat(Channel2);
    scirunnrrd = scimat_save([folderRaw,'Denoised_Ch2_',type,'.mha'],SCIMAT);
    SCIMAT = scimat_im2scimat(Channel3);
    scirunnrrd = scimat_save([folderRaw,'Denoised_Ch3_',type,'.mha'],SCIMAT);
end



