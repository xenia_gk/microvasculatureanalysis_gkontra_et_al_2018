function [Channel0, Channel2, Channel3, num_files1] = LoadImageFiles(dir1)
%LoadImageFiles: Load images from the different channels and save to 3D
%volumes for processing
%
%Input
%           dir1: directory of the image to be processed
%     
%   Output
%           Channel0: 3D volume of Channel0
%           Channel2: 3D volume of Channel2
%           Channel3: 3D volume of Channel3
%           num_files1: number of slices of the 3D volume
%
%NOTE: Images are expected to have 3 channels, the filenames of which are 
%'imageName_z*_ch00' for nuclei, 'imageName_z*_ch02' for the vasculature marker
%(in this work VE-Cadherin) and 'imageName_z*_ch03' for smooth muscle actin
%
%Author: Xenia Gkontra, CNIC


%--------------------------------------------------------------------------
%               STEP 1: Load image names for each channel
%--------------------------------------------------------------------------
%Parts of filenames for each channels
[name1,name2,name3]=set_filenames;
%Channel 0 -> Hoescht (blue):nucleus
if ~isempty(name1)
    filelist0=dir([dir1,name1]);
end
%Channel 2 : vasculature (e.g. VE-Cadherin)
if ~isempty(name2)
    filelist2=dir([dir1,name2]);
end
%Channel 3 : sma
if ~isempty(name3)
    filelist3=dir([dir1,name3]);
end


%--------------------------------------------------------------------------
%                   STEP 2: Preoallocate memory
%--------------------------------------------------------------------------
%Number of images;
num_files1=length(filelist2);
%Read the first image
img1=imread([dir1,filelist2(1).name]);
%Size
[M,N,ch]=size(img1);
Channel0=zeros(M,N,num_files1);
Channel2=zeros(M,N,num_files1);
Channel3=zeros(M,N,num_files1);

%--------------------------------------------------------------------------
%               STEP 3: Save each channel to a 3D volume
%--------------------------------------------------------------------------
for i=1:num_files1
    %------Channel 1: Nuclei
    %Load image file
    ImgCh0=imread([dir1,filelist0(i).name]);
    %Save to the 3d volume
    Channel0(:,:,i)=uint8(ImgCh0);

    %------Channel 2: Vasculature (VE-Cadherin)
    %Load image file
    ImgCh2=imread([dir1,filelist2(i).name]);
    %Save to the 3d volume
    Channel2(:,:,i)=uint8(ImgCh2);
    
    %------Channel 3: SMA
    %Load image file
    ImgCh3=imread([dir1,filelist3(i).name]);
    %Save to the 3d volume
    Channel3(:,:,i)=uint8(ImgCh3);
end

end

