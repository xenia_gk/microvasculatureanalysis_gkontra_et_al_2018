%--------------------------------------------------------------------------
%Main function for denoising of volumes and/or saving the original
%channels and the denoised images if desireable to .mha format for
%visualization (e.g. with ITK-SNAP)
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%%--------------------------------------------------------------------------


clear;
close all;
%--------------------------------------------------------------------------
%                Directories & Filenames
%--------------------------------------------------------------------------
%Call function that sets the directories
directories=set_directories(GetFullPath('..'));

%Directory where the original data are grouped in folders by time-point
mainDir_Original=directories.mainDir_Original;

%List time-points after MI to be studied. Please note first two folders
%listed by dir are '.' and '..' so we ignore them in the next steps
daysMI_All=dir(mainDir_Original);

%Output folder, check if folder exists and if not create it
folderOut=directories.mainDir;
if ~exist(folderOut)
    mkdir(folderOut);
end;

%--------------------------------------------------------------------------
%         Parameters to define what actions should be taken
%--------------------------------------------------------------------------
%Perform denoise: if denoise=1 then denoised is performed, otherwise no
%denoising is performed
denoise=1;
%Save .mha files or not: only if raw=1, .mha files are saved.
raw=2;

%--------------------------------------------------------------------------
%                   Pre-processing 
%--------------------------------------------------------------------------
for j=3:length(daysMI_All)
    
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    %Directory for this particular time-point
    directoryIn=[mainDir_Original,daysMI];
    %Images to be processed
    type_ALL=dir(directoryIn);
    
    %Run for very image of this time-point
    for i=3:length(type_ALL)
        
        %Name of image used for loading and saving results: Check if it is
        %number and convert to string
        if isnumeric(type_ALL(i).name)
            type=num2str(type_ALL(i).name);
        else
            type=type_ALL(i).name;
        end
        %Directory to load the particular image under investigation
        directoryIn_im=[directoryIn,'/',type,'/'];
        %Directory for saving the results
        directoryOut=[folderOut,daysMI,'/',type,'/'];
        %Check if folder exists, otherwise create it
        if ~exist(directoryOut,'dir')
            mkdir(directoryOut);
        end
        %Folder for saving the main results (Original, Denoised image volumes
        %and later segmentations etc)
        folderExtras=[directoryOut,'extras/'];
        %Check if folder exists, otherwise create it
        if ~exist(folderExtras,'dir')
            mkdir(folderExtras);
        end
        
        %Call preprocessing function to load and pre-process the image
        tic
        preprocessing(directoryIn_im,directoryOut,folderExtras,type,denoise,raw);
        toc
    end
    
end
