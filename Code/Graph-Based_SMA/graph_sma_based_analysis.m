function []=graph_sma_based_analysis(mainDir,mainDir_Filled,mainDir_Arterioles,dirOut,imageName,additional)
%Function to load all necessary data, calculate skeleton, and call
%calculate_all_metrics.m function to analyze the microvasculature at the
%segment level based on its graph-based representation,
%as well as the SMA+ coverage, damage of the tissue through myofibroblasts
%estimation and cell counting related information
%Input:
%               - mainDir: Directory where the nuclei and SMA+
%               cells segmentations are saved
%               - mainDir_Filled: Directory where the microvasculature is
%               saved (filled segemented VE-Cadherin channel)
%               - mainDir_Arterioles: Directory where the 3D volume with
%               annotated (on voxels) of regons that might belong to
%               arterioles/venules is saved
%               -imageName: name of the image being processed
%               - additional: whether to calculated additional metrics not
%               presented in Gkontra et al., 2018 (additional=1) regarding the
%               characteristics of vessels covered with sma or not
%
%Output:
%               - A .mat file with the name RXMI6_name.mat is saved in
%               folder dirOut and contains intermediate and final
%               results regarding the segment level metrics, SMA
%               coverage/damage, and nuclei calculations through the call
%               of the function calculate_all_metrics
%                           
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com

tic
folderRaw=dirOut;
%Check whether the folder exist else create it
if ~exist(folderRaw)
    mkdir(folderRaw)
end

%--------------------------------------------------------------------------
%       Part 1: Prepare for calculating the metrics
%       Load sma, microvasculature and arterioles reconstructions
%--------------------------------------------------------------------------
%Load nuclei segmentation
data=load([mainDir, imageName,'/extras/SegmMMT_',imageName,'.mat']);
BWCh0=data.BWCh0;
[Nx,Ny,Nz]=size(BWCh0);
%Load SMA segmentation
data=load([mainDir, imageName,'/extras/SegmSMA_',imageName,'.mat']);
BWCh3=data.BWCh3;
%Load filled volumes
data=load([mainDir_Filled,imageName,'/BW_',imageName,'.mat']);
BWCh2=data.BW;
%Load arterioles
dataArt=load([mainDir_Arterioles,imageName,'/Arterioles_',imageName,'.mat']);

%Voxel size and voxel volume
[voxelSize,Voxel_volume]=define_voxelSize(imageName);
%Convert from nm^3 to microns^3
Voxel_volume=Voxel_volume*10^(-9);
%Calculate tissue volume
Vol_sample=Voxel_volume*Nx*Ny*Nz;

%Areas that might belong to arterioles/venules
Art2=dataArt.Vfiltered;
Art=Art2>0;


%--------------------------------------------------------------------------
%               Part 2: Calculate skeleton & distance transform
%--------------------------------------------------------------------------

%Calculate distance transformation of every non-vascular voxel to the
%closest voxel that belongs to the vasculature
%An extra control whether the distance transform already exists for
%cases where I already have the skeletons from previous runs to avoid
%unecessary calculation
if exist([folderRaw,'/SDF_c_',imageName,'.mat'],'file')
    load([folderRaw,'/SDF_c_',imageName,'.mat']);
else
    SDF1=bwdistsc(1-BWCh2,voxelSize);
    save([folderRaw,'/SDF_c_',imageName,'.mat'],'SDF1')
end

%An extra control of if the skeleton has been created is performed for
%cases where I already have the skeletons from previous runs to avoid
%unecessary calculation
if exist([folderRaw,'/net8_',imageName,'.mat'],'file')
    load([folderRaw,'/net8_',imageName,'.mat']);
    skel=skel_final;
    link3=link;
else
    %Create skeleton
    [link3,node,skel]=improved_skeleton_graph_2(BWCh2,dataArt.Vfiltered,voxelSize,folderRaw,imageName,SDF1);
end
%Distance function on skeleton voxels
SDF=SDF1.*skel;
%Initialize
labelSkel=zeros(size(BWCh2));

%--------------------------------------------------------------------------
%               Step 3: Call function for calculating metrics
%--------------------------------------------------------------------------
calculate_all_metrics(link3,node,BWCh0, BWCh2,BWCh3,Art,SDF,voxelSize,labelSkel,Vol_sample,folderRaw,imageName,additional);

toc