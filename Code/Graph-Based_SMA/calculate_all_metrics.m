function []=calculate_all_metrics(link3,node,BWCh0, BWCh2,BWCh3,Art,SDF,voxelSize,labelSkel,Vol_sample,folderRaw,name,additional)
%Function to calculate metrics that allow characterization of the
%microvasulture at the segment level based on its graph form representation and metrics that
%allow to characterize the SMA+ cells coverage (thickness and percentage of
%covered vessels) and damage level of the tissue.
%
%Input:
%                   - link3: structure conatining information about the segments
%                   detected the improved skeleton
%                   - node: structure conatining information about the nodes
%                   detected on the skeleton of the microvasculature
%                   - BWCh0: 3D binary image volume of the segmented nuclei channel
%                   - BWCh2: 3D binary image volume of the reconstucted microvacculature
%                   - BWCh3: 3D binary image volume of the segmented SMA+ cells channel
%                   - Art: 3D image volume with annotated the regions (on voxels) that
%                   might belong to arterioles/venules
%                   - SDF: 3D distance transform of the voxels on the skeleton
%                   of the microvasculature
%                   - voxelSize: resolution of the image volumes
%                   - labelSkel: an empty 3D image volume of size equal to
%                   the of the image being processed to be used for
%                   reconstructing the skeleton using the link3 and nodes
%                   structure and coloring every segment with a different
%                   color
%                   - Vol_Sample: volume of the tissue in units (here microns^3)
%                   - folderRaw: folder to be used for saving the output
%                   .mat file with intermediate and final results
%                   - name: name of the image being processed
%                   - additional: whether to calculated additional metrics not
%                   presented in Gkontra et al., 2018 (additional=1) regarding the
%                   characteristics of vessels covered with sma or not
%
%Output:
%                   - A .mat file with the name RXMI6_name.mat is saved in
%                   folder folderRaw and contains intermediate and final
%                   results regarding the segment level metrics, SMA
%                   coverage/damage, and nuclei calculations
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com

%--------------------------------------------------------------------------
%           STEP 1: Network metrics
%--------------------------------------------------------------------------
%Calculate tortuosity, mean length, mean-max-min radius for each vessel
%segment
net_props=zeros(length(link3),5);
for k=1:length(link3)
    labelSkel(link3(k).point)=k;
    if ~isempty(link3(k).point)
        [tortuosity,radiiMean,radiiMax,radiiMin,len]=network_Metrics2(link3(k),SDF,voxelSize);
        net_props(k,:)=[tortuosity len radiiMean radiiMax radiiMin];
    end
end

%--------------------------------------------------------------------------
%                   STEP 2: Adjacency related metrics
%--------------------------------------------------------------------------
%Calculate num1=number of nodes, num2=number of links, num3=independent
%vascular networks,num4=end-points, deg=degrees of each node

[num1,num2,num3,num4,num5,num6,num7,num8]=metrics_Adjacency([],link3,node,BWCh2);


%--------------------------------------------------------------------------
%      STEP 3: Metrics based on the graph-based representation
%      of the microavsculature and by assuming that every vessel
%      can be aprocimated as a tube of constant radius
%--------------------------------------------------------------------------
[Vol_mean,L_mean,D_mean,S_mean,Vv,Lv,Sv,R]=metrics_vessel_constant_tube(net_props(:,2),net_props(:,3),Vol_sample);

%--------------------------------------------------------------------------
%          STEP 4: SMA+ cells related metrics & nuclei related
%                   metrics
%--------------------------------------------------------------------------
%Call function for calcualting SMA coverage and number of nuceli of every
%category
[output,num_endothelialCells,nun_Nuclei_SMATouch,nun_Nuclei_fibro,BW_Endothelial,BW_SMA_Touching,BW_Fibro]=SMACoverage(BWCh3,BWCh2,BWCh0,4);

cell_metrics=[num_endothelialCells nun_Nuclei_SMATouch nun_Nuclei_fibro];

%SMA+ related metrics according to region
%Exclude touching SMA that corresponds to arterioles
%Call function to calculate the image that contains only SMA touching
%capillaries
[outputCap]=SMACoverage(BWCh3,BWCh2.*(1-Art),BWCh0,2);
%SMA touchning arterioles
SMA_Arterioles=output.*(1-outputCap);
%SMA Capillaries
SMA_Capillaries=outputCap;
%SMA outside of capilalries and aretrioles
SMA_Fibro=(BWCh3-output);

%Number of sma+ voxels touching the capillaries
sma_Capillaries=sum(SMA_Capillaries(:));
%Number of sma+ voxels touching the arterioles
sma_Arterioles=sum(SMA_Arterioles(:));
%Number of sma+ voxels not associated with vessels
sma_Fibro=sum(SMA_Fibro(:));

c_damage=sma_Fibro/(sma_Capillaries+sma_Fibro);
%Damage index capilalries + large vessels
damage=sma_Fibro/(sma_Capillaries+sma_Fibro+sma_Arterioles);

sma_cap=[sma_Capillaries sma_Arterioles sma_Fibro damage c_damage];

%labels1: labels of vessels with SMA coverage, labels2: labels of vessels
%without SMA coverage

[labels1,labels2,D2_SMA,D2_NoSMA,num_SMA,SMA_thick,labels1_cap,labels2_cap]=extract_labels(BWCh2,labelSkel,output,voxelSize, Art);

%--------------------------------------------------------------------------
%               STEP 5: Copy results to matrices for plotting
%--------------------------------------------------------------------------
%===========        1. Network metrics          ===========================
%1: Number of nodes | 2: Number of links | 3: Number of connected
%components | 4: Number of end-points | 5: Length | 6: Mean
%radius | 7: Max radius | 8: Min radius | 9: Degree of nodes |
% 10: Number of bifurcations | 11: Number of trifurcations |
% 12: Number of higher order connections | 13: Tortuosity | 14: Mean volume
%of the segments | 15: Mean Length of the segments | 16. Mean diameter of
%the segments | 17. Mean surface of the segments | 18. Volume density | 19.
%Length Density | 20. Surface density | 21. Maximum extravascular distance

p_net2=[num1 num2 num3 num4 mean(net_props(:,2)) mean(net_props(:,3)) mean(net_props(:,4)) mean(net_props(:,5)) num5 num6 num7 num8 mean(net_props(:,1)) Vol_mean L_mean D_mean S_mean Vv Lv Sv R];

p_net={net_props(labels1,:) net_props(labels2,:)};

%--------------------------------------------------------------------------
%           STEP 6: Additional metrics not presented in
%        Gkontra et al., 2017 but might be useful in other contexts
%--------------------------------------------------------------------------
if additional==1
    %Characteristics of vessels with SMA coverage
    [Vol_C,L_mean_C,D_mean_C,S_mean_C,Vv_C,Lv_C,Sv_C,R_C]=metrics_Spyros(net_props(labels1,2),net_props(labels1,3),Vol_sample);
    p_SMA_Cov=[Vol_C L_mean_C D_mean_C S_mean_C Vv_C Lv_C Sv_C R_C mean(net_props(labels1,:),1)];
    %Characteristics of vessels without SMA Coverage
    [Vol_U,L_mean_U,D_mean_U,S_mean_U,Vv_U,Lv_U,Sv_U,R_U]=metrics_Spyros(net_props(labels2,2),net_props(labels2,3),Vol_sample);
    p_SMA_UnCov=[Vol_U,L_mean_U,D_mean_U,S_mean_U,Vv_U,Lv_U,Sv_U,R_U mean(net_props(labels2,:),1)];
end
%--------------------------------------------------------------------------
%                   STEP 6: Save results
%--------------------------------------------------------------------------
if additional~=1
    save([folderRaw,'\RXMI6_',name,'.mat'],'p_net2','p_net','sma_cap','cell_metrics','labels1','labels2','labels1_cap','labels2_cap','SMA_thick','output','outputCap','BW_Endothelial','BW_SMA_Touching','BW_Fibro','num_SMA')
else
    save([folderRaw,'\RXMI6_',name,'.mat'],'p_net2','p_net','sma_cap','cell_metrics','labels1','labels2','labels1_cap','labels2_cap','SMA_thick','output','outputCap','BW_Endothelial','BW_SMA_Touching','BW_Fibro','num_SMA','p_SMA_Cov','p_SMA_UnCov')
end
    