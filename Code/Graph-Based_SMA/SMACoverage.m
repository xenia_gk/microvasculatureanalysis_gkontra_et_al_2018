function [output,num_endothelialCells,nun_Nuclei_SMATouch,nun_Nuclei_fibro,BW_Endothelial,BW_SMA_Touching,BW_Fibro]=SMACoverage(BWCh3,BWCh2,BWCh0,method)
%Function to calculate SMA+ coverage the microvessels using the 
%smooth muscle actin (SMA) positive areas that colocalize with microvessels
%or are in touch with them. An option of calculating the number of
%endothelial cells, cma+ cells and fibroblasts as well is provided thought
%the "method" option
%
%Input:   
%                   - BWCh3: Segmentation of SMA+ cells channel
%                   - BWCh2: Segmentation of the vasculature
%                   - BWCh0: Segmentation of the nuclei channel
%                   - method: indicator for calculating number of cells
%                   (method==4) or just calculate the 3D volume with the
%                   vessels touching 
%
%Output:            
%                   - output: image containing SMA+ cells common or in touch with 
%                   microvessels
%                   - num_endothelialCells: number of endothelial cells
%                   - nun_Nuclei_SMATouch: number of SMA+ cells (nuclei) on
%                   the SMA coverage of the vessels, ie in contact with vessels
%                   - nun_Nuclei_fibro: number of fibroblasts  (nuclei)
%                   - BW_Endothelial: 3D image volume with endothelial cells
%                   - BW_SMA_Touching: 3D image volume with SMA+ cells 
%                   - BW_Fibro: 3D image volume with fibroblasts
%
%Author: Xenia Gkontra, CNIC, 2015

[Nx,Ny,Nz]=size(BWCh2);

num_endothelialCells=NaN;
nun_Nuclei_SMATouch=NaN;
nun_Nuclei_fibro=NaN;
BW_Endothelial=zeros(Nx,Ny,Nz);
BW_SMA_Touching=zeros(Nx,Ny,Nz);
BW_Fibro=zeros(Nx,Ny,Nz);
%Create image with common parts between SMA and vessels
Img=BWCh3+2*BWCh2;
Common=(Img==3);

%--------------------------------------------------------------------------
%        Method 2: SMA Coverage=Touching to Vessels SMA/Vessels Area
%--------------------------------------------------------------------------
if method==2|method==4
    output=zeros(size(Img));
    %Keep parts of SMA on or common with the microvasculature
    Img2=Img;
    %Image with voxels with value 1 representing voxels where sma was detected
    %but not a blood vessel according to the corresponding BWCh2, and 3 where
    %voxels are commonly on for both sma and BWCh2
    Img2(Img==2)=0;
    Img3=Img2>0;
    %Find connected components
    labelIm=bwlabeln(Img3);
    props=regionprops(labelIm,'Image','BoundingBox');
    length(props)
    for i=1:length(props)
        %Image containing only this component
        Im=props(i).Image;
        %Bounding box [x y z width]
        box=round(props(i).BoundingBox);
        %Check whether this component includes voxels with value of 3 (this
        %means that the component is adjacent to the vasculature)
        %(both sma and vasculature)
        %  x1=isempty(find(Img2(box(2):box(2)+box(5)-1,box(1):box(1)+box(4)-1,box(3):box(3)+box(6)-1)==2,1));
        x2=isempty(find(Img2(box(2):box(2)+box(5)-1,box(1):box(1)+box(4)-1,box(3):box(3)+box(6)-1)==3,1));
        if ~x2
            output(box(2):box(2)+box(5)-1,box(1):box(1)+box(4)-1,box(3):box(3)+box(6)-1)=output(box(2):box(2)+box(5)-1,box(1):box(1)+box(4)-1,box(3):box(3)+box(6)-1)+Im;
        end
    end
    %Ensure that all values on output are set to 1
    output=output>0;
end

%----------------------------------------------------------------------
%               Calculate Myofibroblast index
%           & Coverage caluclated using surface
%----------------------------------------------------------------------
if method==4
    [num_endothelialCells,nun_Nuclei_SMATouch,nun_Nuclei_fibro,BW_Endothelial,BW_SMA_Touching,BW_Fibro]=nuclei_related_metrics(BWCh0,BWCh2,BWCh3,output);
end


