function [A2,node,link,skel2]=cleanSkeleton(skel,thr,voxelSize,Nx,Ny,Nz,SDF)
%Function to clean (prune) skeleton according to either global or local
%theshold
%Input:
%               - skel: Initial skeleton
%               - thr: Threshodl to be used. If local threshold is to be used
%               the thr is a scalar, otherwise the thr is a 3D map 
%               to guide the pruning procedure (local threshold defined
%               according to which region the segments belong to, ie
%               arterioles/venules or capillaries
%               - voxelSize: image resolution
%               - Nx,Ny,Nz: size of the volume being processed
%               -SDF: distance transform 
%Output:
%               - A2:  adjacency matrix 
%               - node: structure with information about the nodes found
%               - link: structure with informationr egarding the links
%               found
%               - skel2: skeleton after pruning
%
%Author: Xenia Gkontra, JHU, 2015

% convert skeleton to graph structure
if isscalar(thr)
    [A,node,link] = Skel2Graph3D(skel,thr); %If global threshold was defined-same for all voxels
else
    [A,node,link] = Skel2Graph3D_v3(skel,thr,SDF,voxelSize); %If local threshold was defined
end

% convert graph structure back to (cleaned) skeleton
skel2 = Graph2Skel3D(node,link,Nx,Ny,Nz);

%Convert skeleton to graph structure without any more pruning
[A2,node,link] = Skel2Graph3D_v3(skel2,zeros(size(SDF)),SDF,voxelSize);

