function [tortuosity,radiiMean,radiiMax,radiiMin,len]=network_Metrics2(link,SDF,voxelSize)
%Function to calculate the radius, tortuosity and length of the vessel
%segments as calcuated form the skeleton of the microvasculature
%
%Input: 
%                      - link3: structure conatining information about
%                      the segments detected the improved skeleton
%                      - SDF: 3D distance transform of the voxels on the skeleton
%                      of the microvasculature
%                      - voxelSize: resolution of the image volumes
%
%Output:
%                       - tortuosity: array of length equal to the number
%                       of segments found in the skeleton that conatins the
%                       tortuosity of every vessel segment
%                       - radiiMean: array of length equal to the number
%                       of segments found in the skeleton that conatins the
%                       radius of every vessel segment. In this case the
%                       radius for each segment is calculated as the mean of all on voxels
%                       on the skeleton of the particular segment
%                       - radiiMax: array of length equal to the number
%                       of segments found in the skeleton that conatins the
%                       radius of every vessel segment. In this case the
%                       radius for each segment is calculated as the maximum of all on voxels
%                       on the skeleton of the particular segment
%                       - radiiMin: array of length equal to the number
%                       of segments found in the skeleton that conatins the
%                       radius of every vessel segment. In this case the
%                       radius for each segment is calculated as the min all on voxels
%                       on the skeleton of the particular segment
%                       - len: array of length equal to the number
%                       of segments found in the skeleton that conatins the
%                       length of every vessel segment
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com

%--------------------------------------------------------------------------
%               Metrics based on link structure
%--------------------------------------------------------------------------


[rows,cols,slices]=size(SDF);
%Calculate for each link the tortuosity and mean radii
%Calculate tortuosity
[x1,y1,z1]=ind2sub([rows,cols,slices],link.point(1));
[x2,y2,z2]=ind2sub([rows,cols,slices],link.point(end));
clear point_set
point_set=zeros(length(link.point),3);
parfor j=1:length(link.point)
    [x3,y3,z3]=ind2sub([rows,cols,slices],link.point(j));
    point_set(j,:)=[x3 y3 z3];
end
L=zeros(size(point_set,1)-1,1);
parfor j=1:size(point_set,1)-1
    L(j) = sqrt(((point_set(j+1,1)-point_set(j,1))*voxelSize(1))^2 + ((point_set(j+1,2)-point_set(j,2))*voxelSize(2))^2 ...
        + ((point_set(j+1,3)-point_set(j,3))*voxelSize(3))^2);
end

len=sum(L);
tortuosity=len/sqrt(((x1-x2)*voxelSize(1)).^2+((y1-y2)*voxelSize(2)).^2+((z1-z2)*voxelSize(3)).^2);
%Calculate radii
radiiMean=mean([SDF(link.point)]);
radiiMax=max([SDF(link.point)]);
radiiMin=min([SDF(link.point)]);



%save([directoryOut,'network',type,'.mat'],'tortuosity','radiiMin','radiiMax','radiiMean','len')