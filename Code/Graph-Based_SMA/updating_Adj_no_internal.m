function [node3,link3]=updating_Adj_no_internal(node2,link2)
%Function to delete internal nodes
%Input:         node2: structure with nodes including internal nodes
%               link2: structure with edges
%Output:        node2: structre with nodes after eliminating internal nodes
%               link3: structure with edges after eliminating internal nodes
%Author:  Xenia Gkontra, JHU, 2015             



%merging linksnodes that are internal nodes (a small branch was found there and it got pruned)
clear node3 link3 link_new indx idx;
%All nodes that are branching (connecting to at least two links)
temp=arrayfun(@(x)(find(length(x.links)>=3)),node2,'UniformOutput',false);
indBr=find(cellfun(@(y)~isempty(y),temp));
%All nodes that are internal
temp=arrayfun(@(x)(find(length(x.links)==2)),node2,'UniformOutput',false);
indIn=find(cellfun(@(y)~isempty(y),temp));
%All nodes that correspond to an end point
temp=arrayfun(@(x)(find(length(x.links)==1)),node2,'UniformOutput',false);
indEn=find(cellfun(@(y)~isempty(y),temp));
%For visualization
%{
skel_copy3=skel2;
for i=1:length(x2)
skel_copy3([node2(x2(i)).idx])=3;
end
%}
sizLinks=size(link2);
%Merge the links that are separated by an internal node, this way cycle
%detection will be really fast
link_new=link2;
nodes_new=node2;
for i=1:length(indIn)
    
    %   Position in the new matrix to save the new segment
    pos=size(link_new,2)+1;
    %Internal nodes that form the
    l1=node2(indIn(i)).links(1);
    l2=node2(indIn(i)).links(2);
    %Create new link
    %All nodes connected to the specified link
    temp=[link2(l1).n1 link2(l1).n2 link2(l2).n1 link2(l2).n2];
    %Keep the other two nodes to which the liks are connected
    n=temp(temp~=indIn(i));
    point=[link2(l1).point link2(l2).point node2(indIn(i)).idx'];
    link_new=[link_new struct('n1',n(1),'n2',n(2),'point',point)'];
    %Set this link to empty
    link_new(l1).n1=[];
    link_new(l1).n2=[];
    link_new(l1).point=[];
    link_new(l1).n1=[];
    link_new(l1).n2=[];
    link_new(l1).point=[];
    link_new(l2).n1=[];
    link_new(l2).n2=[];
    link_new(l2).point=[];
    link_new(l2).n1=[];
    link_new(l2).n2=[];
    link_new(l2).point=[];
    %Find other nodes that are connected to these to link and give them the
    %new link as reference
    temp=arrayfun(@(x)find(x.links==l1|x.links==l2),node2,'UniformOutput',false);
    [indR,indC]=find(~cellfun('isempty',temp));
    for j=1:length(indR)
        nodes_new(indC(j)).links(indR(j))=pos;
    end
    %Set values empty of internal node
    nodes_new(indIn(i)).links=[];
    nodes_new(indIn(i)).conn=[];
    
    
end
%Copy to new matrices that exclude the links, nodes that aren't in use
indx=find(~cellfun('isempty',(arrayfun(@(x)find(~isempty(x.n1)),link_new,'UniformOutput',false))));
%Initialize struct
clear link3
link3(length(indx))=struct();
%unique nodes:
[un]=unique([[link_new.n1]';[link_new.n2]']);
%ignore 0
ref=zeros(length(nodes_new),1);
ref(un(1:end)')=[1:length(un(1:end))]';
%keep only that nodes that are unique
node3=nodes_new(un(1:end)');
clear temp

for i=1:length(indx)
    if link_new(indx(i)).n1>0
        %Start node
        link3(i).n1=ref((link_new(indx(i)).n1));
        n1=link3(i).n1;
        %Links corresponding to the start node
        temp=find([node3(n1).links]==indx(i));
        node3(n1).links(temp)=i;
        %Update links in connectivity: node.conn
        temp1=find(node3(n1).conn>0);
        node3(n1).conn(temp1)=ref(node3(n1).conn(temp1));
    end
    if link_new(indx(i)).n2>0
        %end-node
        link3(i).n2=ref(link_new(indx(i)).n2);
        n2=link3(i).n2;
      %Links corresponding to the end node
        temp=find([node3(n2).links]==indx(i));
        node3(n2).links(temp)=i;
        %Update links in connectivity: node.conn
        temp1=find(node3(n2).conn>0);
        node3(n2).conn(temp1)=ref(node3(n2).conn(temp1));
        
    end
    link3(i).point=link_new(indx(i)).point;
    
    
    %=struct('n1',link(indx(i)).n1,'n2',link(indx(i)).n2,'point',link(indx(i)).point');
end

%last segment
l=length(link3);
for i=1:length(node3)
    temp=find(node3(i).links>l);
    node3(i).links(temp)=[];
    node3(i).conn(temp)=0;
end




%Size of adjacency matrix=size of
%Create adjacency matrix
A=zeros(length(node3),length(node3));
for i=1:length(node3)
    idx1=find(node3(i).conn>0);
    idx2=find(node3(i).links>0);
    idx=intersect(idx1,idx2);
    for j=1:length(idx)
        if(i==link3(node3(i).links(idx(j))).n1)
            %    A(i,link3(node3(i).links(idx(j))).n2)=length(link3(node3(i).links(idx(j))).point);
            %    A(link3(node3(i).links(idx(j))).n2,i)=length(link3(node3(i).links(idx(j))).point);
            A(i,link3(node3(i).links(idx(j))).n2)= A(i,link3(node3(i).links(idx(j))).n2)+1;
            A(link3(node3(i).links(idx(j))).n2,i)= A(link3(node3(i).links(idx(j))).n2,i)+1;
        end;
        if(i==link3(node3(i).links(idx(j))).n2)
            %   A(i,link3(node3(i).links(idx(j))).n1)=length(link3(node3(i).links(idx(j))).point);
            %   A(link3(node3(i).links(idx(j))).n1,i)=length(link3(node3(i).links(idx(j))).point);
            A(i,link3(node3(i).links(idx(j))).n1)=A(i,link3(node3(i).links(idx(j))).n1)+1;
            A(link3(node3(i).links(idx(j))).n1,i)=A(link3(node3(i).links(idx(j))).n1,i)+1;
        end;
    end;
end;

%Correct multi-edges


%Find cycles


%Keep cycles that are possible VE-Cadherin gaps and not vessels
%cross-sections-> keep only regions not intersecting with SMA
%For visualization purdposes
%{
skel_copy3=skel2;
for i=1:length(x2)
    skel_copy3([node2(x2(i)).idx])=3;
end
%Find
%}
