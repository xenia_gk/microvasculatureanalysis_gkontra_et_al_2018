function [seg_length]=segment_length(seg,voxelSize,Nx,Ny,Nz)
%Calculate length of the segment, works for both isotropic and 
%anisotropic voxel size
%
%Input:         
%           -seg: indexes of voxels on the segment
%           -voxelSize: size of the voxel
%
%Output:
%           - length of the segment
%
%Author: Xenia Gkontra, CNIC, 2016

segment(:,1)=seg;
%First point on the segment
j=1;
[x,y,z]=ind2sub([Nx,Ny,Nz],seg(j,1));
%x-coordinate for the specific point j of segment i
segment(j,2)=x;
%y-coordinate for the specific point j of segment i
segment(j,3)=y;
%z-coordinate for the specific point j of segment i
segment(j,4)=z;

for j=2:size(segment,1)
    [x,y,z]=ind2sub([Nx,Ny,Nz],segment(j,1));
    %x-coordinate for the specific point j of segment i
    segment(j,2)=x;
    %y-coordinate for the specific point j of segment i
    segment(j,3)=y;
    %z-coordinate for the specific point j of segment i
    segment(j,4)=z;
    L(j-1,1) = sqrt(((segment(j-1,2)-segment(j,2))*voxelSize(1))^2 + ((segment(j-1,3)-segment(j,3))*voxelSize(2))^2 ...
        + ((segment(j-1,4)-segment(j,4))*voxelSize(3))^2);
end

seg_length=sum(L);