function [skel3,bubbles2]=replace_bubbles_skeleton(node,link,Nx,Ny,Nz,outputFolder,name)
%Replace bubbles or multi-point node points presented in the skeleton
%Input:            
%                   node: structure with nodes
%                   link: structure containing links
%                   Nx, Ny,Nz: size of the volume under investigation (rows
%                   ,columns, slices)
%
%               
%Output:            skel3: skeleton without bubbles
%                   bubbles2: image containing the links that replaced
%                   bubbles
%Xenia Gkontra, JHU, 2015


%Find the bubbles
b=find(~cellfun('isempty',arrayfun(@(x)find(length(x.idx)>1),node,'UniformOutput',false)));

%Create image with the bubbles - labeled, each bubble is set to a value
bubbles=zeros(Nx,Ny,Nz);
bubbles2=zeros(Nx,Ny,Nz);
%541
for i=1:length(b)
    bubbles(node(b(i)).idx)=i;
end
%labeled the image
labeled=bubbles;
props=regionprops(bubbles,'Centroid');
bubbles=bubbles>0;

%labeled=bwlabeln(bubbles);
%Find the centroid
%props=regionprops(labeled,'Centroid');
R = imref3d([Nx Ny Nz]);
for i=1:length(b)
    %Convert centroid to index
    Centroid=[props(i).Centroid(2) props(i).Centroid(1) props(i).Centroid(3)];
    [xIntrinsic,yIntrinsic,zIntrinsic] = worldToIntrinsic(R,props(i).Centroid(2),props(i).Centroid(1),props(i).Centroid(3));
    indexC=sub2ind([Nx,Ny,Nz],xIntrinsic,yIntrinsic,zIntrinsic);
    %Find the links connected to each bubble
    linkConnected=node(b(i)).links;
    for j=1:length(linkConnected)
        %Find closest point of each link to the centroid
        points=link(linkConnected(j)).point;
        %Convert index to coordinates for the first and last segment point
        [pointFirst(1,1),pointFirst(1,2),pointFirst(1,3)]=ind2sub([Nx,Ny,Nz],points(1));
        [pointLast(1,1),pointLast(1,2),pointLast(1,3)]=ind2sub([Nx,Ny,Nz],points(end));
        %Calculate distances
        dif=[pdist2(pointFirst,Centroid) pdist2(pointLast,Centroid)];
        %Connect closest point of the link to the centroid
        [dist,pos]=find(dif==min(dif),1);
        if pos==1
            point1=pointFirst;
        else
            point1=pointLast;
        end
        t=0:(1/floor(dif(pos))):1;
        length(t)
        C=(repmat(point1,length(t),1)'+(Centroid-point1)'*t)';
        
        %Convert C to index
        indexC2=[];
        length(C)
        for k=1:size(C,1)
            %  [xIntrinsic,yIntrinsic,zIntrinsic] = worldToIntrinsic(R,C(k,1),C(k,2),C(k,3));
            indexC2=sub2ind([Nx,Ny,Nz],round(C(k,1)),round(C(k,2)),round(C(k,3)));
            bubbles2(indexC2)=1;
        end
        %
        
    end
end

skel3=zeros(Nx,Ny,Nz);
for i=1:length(link)
    for j=1:length(link(i).point)
        skel3(link(i).point(j))=1;
    end
end

%Visualize to check whether everything was done correctly
if exist(outputFolder)
  %  SCIMAT = scimat_im2scimat(double(skel3)+2*double(bubbles2));
  %  scirunnrrd = scimat_save([outputFolder,'bubbles_',name,'.mha'],SCIMAT);
end