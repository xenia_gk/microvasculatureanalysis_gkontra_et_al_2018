function [labels1,labels2,D2_SMA,D2_NoSMA,num_SMA,SMA_thick,labels1_cap,labels2_cap]=extract_labels(BWCh2,labelSkel,output,voxelSize,Art)
%Function to calculate the thickness of the SMA+ Coverage eof the vessels.
%During the run of this functions the segments that are covered with SMA
%those that are not are calculated and returned by the functions for future
%use as well as the segmented that belong to capillaries only and are or
%are not covered with SMA+ cells.
%
%Input: 
%                     - BWCh2: 3D binary voluem of the microvasculature
%                     - labelSkel: labeled skeleton (every segment of the
%                     skeleton is annotated with a different color)
%                     - output:3D binary volume with the SMA+ cells
%                     coverage of the vessels 
%                     - voxelSize: resolution of teh volume
%                     - Art: 3D image volume with annotated the regions (on voxels) that
%                     might belong to arterioles/venules
%
%Output:
%                    - labels1: labels of the vessel segments that are
%                    covered with SMA
%                    - labels2: labels of the vessel segments that are
%                    not covered with SMA
%                    - D2_SMA: 3D labeled volume that contains only the
%                    vessels that are in contact with SMA+ cells, ie thay
%                    have s SMA+ covreage
%                    - D2_NoSMA: 3D labeled volume that contains only the
%                    vessels that are not in contact with SMA+ cells, ie
%                    they do not have SMA+ coverage
%                    - num_SMA: arrays with number of SMA+ cells parts that form the
%                    coverage for every vessels. The size of the area is equal to
%                    the number or vessels covered with SMA.
%                    - SMA_thick: array with the thickeness of the coverage
%                    of every image
%                    - labels1_cap: labels of the vessel segments that
%                    belong the capillaries regions (as indicated by the
%                    %Art 3D volume) and which are covered with SMA
%                    - labels2_cap: labels of the vessel segments that
%                    belong the capillaries regions (as indicated by the
%                    %Art 3D volume) and which are not covered with SMA
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com

%--------------------------------------------------------------------------
%          STEP 1: Decompose volume with vessels into 2:
%         one with vessels with SMA coverage and one with
%                   those lacking SMA coverage
%--------------------------------------------------------------------------

[~,IDX] = bwdist(labelSkel);
%Replace the minimum distance with the label of the voxel to which the minimum distance was calculated
D2=labelSkel(IDX).*BWCh2;
%Create image with SMA coverage
SMA_D2=double(D2).*output;
%Properties
propsSMA=regionprops(SMA_D2,'Area');
u1=unique(D2);
u2=unique(SMA_D2);
%Find the label of vessels with SMA coverage
values=u1(ismember(u1,u2));
%Create label matrix that contains only the vessels with sma
D2_SMA=D2;
D2_SMA(~ismember(D2,values))=0;
%%Create label matrix that contains only the vessels without sma coverage
D2_NoSMA=D2;
D2_NoSMA(ismember(D2,values))=0;
%x=imSurface(D2_SMA);
propsVessel=regionprops(D2_SMA,'Area');

%--------------------------------------------------------------------------
%           STEP 2: Calculate characteristics of vessels
%         with SMA coverage and of those without SMA coverage
%--------------------------------------------------------------------------
%Labels for vessels with SMA
labels1 = unique(D2_SMA);
labels1(labels1 == 0) = [];
%Labels for vessels without SMA
labels2 = unique(D2_NoSMA);
labels2(labels2 == 0) = [];

%--------------------------------------------------------------------------
%         STEP 3: Find labels corresponding to capillaries
%--------------------------------------------------------------------------

%Replace the minimum distance with the label of the voxel to which the minimum distance was calculated
D3=labelSkel(IDX).*BWCh2.*Art;
%Labels corresponding to capillaries
values_cap=unique(D3);
%Find labels in label1 that correspond to capillaries
labels1_cap=ismember(labels1,values_cap);
%Find labels in label2 that correspond to capillaries
labels2_cap=ismember(labels2,values_cap);


%--------------------------------------------------------------------------
%           STEP 4: Calculate thickness
%--------------------------------------------------------------------------
%Distance to closest vessel
SDF1=bwdistsc(BWCh2,voxelSize);
SMA_D2_2=labelSkel(IDX).*output;
values(find(values==0))=[];


%For each vessel, extract each corresponding parts of SMA coverage
parfor i=1:length(values)
    %Image with all parts of SMA that cover vessel with label values(i)
    temp2=double(SMA_D2_2==values(i));
    %Distances for each piece
    temp1=SDF1.*(temp2);
    
    props=regionprops(temp2,'PixelIdxList');
    %Number of pieces of SMA on top of the vessel
    num_SMA(i)=length(props);
    %Maximum distance of each piece to the vessel: it represents the
    %thickness of the SMA coverage
    temp=[];
    for j=1:length(props)
        temp(j)=max([temp1(props(j).PixelIdxList)]);
    end
    %Mean value of the maximum coverage
    SMA_thick(i)=mean(temp);
end


