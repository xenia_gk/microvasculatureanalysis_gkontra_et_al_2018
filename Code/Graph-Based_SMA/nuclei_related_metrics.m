function [num_endothelialCells,nun_Nuclei_SMATouch,nun_Nuclei_fibro,BW_Endothelial,BW_SMA_Touching,BW_Fibro]=nuclei_related_metrics(BWCh0,BW,BWCh3,output)
%Funtion to calculate number of endothelial cells, SMA+ cells in contact with 
%vessels and myofibrobalsts. The calculation is based on nuclei counting on the surface of
%the vessels for endothelial cells, the SMA+ cells that belong to the SMA+
%coverage of the vessels for SMA+ cells in contact with vessels, and 
%SMA+ cells not in contact with vessels for the myofibroblasts repectively.
%
%Input:
%                   BWCh0: Segmentation of the nuclei channel
%                   BW: Segmentation of the vasculature
%                   BWCh3: Segmentation of SMA+ cells channel
%                   - output: image containing SMA+ cells common or in touch with 
%                   microvessels
%
%Output:
%                   - num_endothelialCells: number of endothelial cells
%                   - nun_Nuclei_SMATouch: number of SMA+ cells (nuclei) on
%                   the SMA coverage of the vessels, ie in contact with vessels
%                   - nun_Nuclei_fibro: number of fibroblasts  (nuclei)
%                   - BW_Endothelial: 3D image volume with endothelial cells
%                   - BW_SMA_Touching: 3D image volume with SMA+ cells 
%                   - BW_Fibro: 3D image volume with fibroblasts
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com

[Nx,Ny,Nz]=size(BWCh0);
%--------------------------------------------------------------------------
%                Vessel surface
%--------------------------------------------------------------------------
h=ones(3,3,3);
h(2,2,2)=0;
V1=convn(BW,h,'same').*BW;
vessel_surface=double(V1<25&V1>0);

%--------------------------------------------------------------------------
%               Segment Nuclei
%--------------------------------------------------------------------------
%With watershed
se = strel('disk', 2);
bw = imopen(BWCh0, se);
D = -bwdist(~bw);
mask = imextendedmin(D,2);
D2 = imimposemin(D,mask);
Ld2 = watershed(D2);
bw3 = bw;
bw3(Ld2 == 0) = 0;
%Label image
labelWater=bwlabeln(bw3);
Nuclei=labelWater;
%--------------------------------------------------------------------------
%               Common between nuclei and vessel surface
%--------------------------------------------------------------------------
%Nuclei that that touch vessel surface (ie endothelial)
endothelialCells=vessel_surface.*Nuclei;
%Total number of endothelial cells
labels_Endo=unique(endothelialCells);
num_endothelialCells=length(labels_Endo)-1;
%Create an image that contains the endothelial cells
BW_Endothelial=Nuclei;
BW_Endothelial(~ismember(BW_Endothelial,labels_Endo))=0;

%--------------------------------------------------------------------------
%               Common between sma touching area and vessel surface
%--------------------------------------------------------------------------
%SMA+ cell not touching the vessels, ie possible fibroblasts
fibroblasts=BWCh3.*(1-output);
%Nuclei that belong to SMA+ cells that touch a vessel
Nuclei_SMA=output.*Nuclei;
labels_SMATouch=unique(Nuclei_SMA);
nun_Nuclei_SMATouch=length(labels_SMATouch)-1;
%Create volume that contains all touching nuclei
BW_SMA_Touching=Nuclei;
BW_SMA_Touching(~ismember(BW_SMA_Touching,labels_SMATouch))=0;

%Nuceli that belong to SMA+ cell that could be fibrosblasts (do not touch a
%vessel)
Nuclei_fibro=fibroblasts.*Nuclei;
nun_Nuclei_fibro=length(unique(Nuclei_fibro))-1;
labels_fibro=unique(Nuclei_fibro);
%Create volume with fibroblasts nuclei
BW_Fibro=Nuclei;
BW_Fibro(~ismember(BW_Fibro,labels_fibro))=0;

