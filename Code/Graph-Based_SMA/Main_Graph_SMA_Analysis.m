%--------------------------------------------------------------------------
%Main function for calculating metrics describing the microvasculature at
%the segment level and sma coverage related metrics
%
%Output:
%               - .mat files during the call through the call of by 
%               graph_sma_based_analysis
%
%Note! Remember to adapt pruning of the skeleton to your needs if necessary
%in function improved_skeleton_graph_2.m
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clc
clear;

%Additional metrics not presented in Gkontra et al., 2017, that which might
%be useful in other contexts
additional=0;
%--------------------------------------------------------------------------
%                   Input & Output Directories
%--------------------------------------------------------------------------
%Call function to load the directories
directories=set_directories(GetFullPath('..'));

%Directory to save the results
dirOut=directories.mainDir_Segment;
%Directory where the VE-Cadherin segmentations after the application of the
%filling algorithm can be found
dirFilled=directories.mainDir_Filled;
%Directory where 3D volume with possible arterioles/venules areas are saved
dirArt=directories.mainDir_Art;
%Input directory where the segmentations of SMA and nuclei channels are
%saved
dirIn=directories.mainDir;
%List time-points after MI to be studied. Please note first two folders
%returned by Matlab's "dir" function are '.' and '..' so we ignore them
daysMI_All=dir(dirIn);
%--------------------------------------------------------------------------
%                     Calculate parameters
%--------------------------------------------------------------------------

for j=3:length(daysMI_All)
    %Time-point under investigation
    daysMI=daysMI_All(j).name;
    filelist=dir([dirIn,daysMI]);
    
    for i=3:length(filelist)
        %Name of image used for loading and saving results
        type=filelist(i).name;
        tissue_cat=define_tissueCat(type);
        %Directory for saving the results
        directoryOut=[dirOut,daysMI,'/',tissue_cat,'/',type,'/'];
        %Input directories for the current image
        directoryIn=[dirIn,daysMI,'/'];
        directoryFilled=[dirFilled,daysMI,'/',tissue_cat,'/'];
        directoryArt=[dirArt,daysMI,'/',tissue_cat,'/'];
        graph_sma_based_analysis(directoryIn,directoryFilled,directoryArt,directoryOut,type,additional);
        
    end
end
