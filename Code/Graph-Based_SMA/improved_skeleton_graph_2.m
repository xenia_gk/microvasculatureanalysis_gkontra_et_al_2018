function [link,node,skel_final]=improved_skeleton_graph_2(BW,Vfiltered,voxelSize,outputFolder,name,SDF)
%Function for calculting an imrpoved versions of the skeleton and 
%the network graph of the skeleton based on modifications and additions on 
%Kollmannsberger's functions (%Kerschnitzki, Kollmannsberger et al.,
% "Architecture of the osteocyte network correlates with bone material quality."
% Journal of Bone and Mineral Research, 28(8):1837-1845, 2013.)
%In order to improve the skeleton calcuted by Kollmannsberger's functions a
%local pruning approach is added. The local pruning is based on the
%knowledge of the type of vessels, ie arterioles/venules or capilalries.
%For capillaries a stricter threshold is used while for larger vessels a
%less strict. Bubbles on the skeleton are also removed and the stucture
%nodes is modified so as not to include branch points whose branch vessels
%have been pruned.
%Input:
%               - BW: 3D Binary volume (containing the microvasculature) on 
%               which the skeletonization should be applied
%               - Vfiltered: 3D volume map where  on voxels indicate possible
%               arterioles/venules regions
%               - voxelSize: resolution in microns
%               - outputFolder: folder where the results should be saved
%               - SDF: distance transform of vascular voxels of BW
%Output: 
%               - link: structure conatining information about the segments
%               detected the improved skeleton 
%               (see Kollmannsberger's folder for more details on the
%               structure format)
%               - node: structure conatining information about the nodes
%               detected on the improved skeleton 
%               (see Kollmannsberger's folder for more details on the
%               structure format)
%               - skel_final: final skeleton
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com


%Image size
[Nx,Ny,Nz]=size(BW);
Vfiltered=Vfiltered>0;

%--------------------------------------------------------------------------
%                   Part 1: Calculate skeleton & Prune
%--------------------------------------------------------------------------
%Create skeleton
skel_final=Skeleton3D(BW);

%Create volume that locally indicates threshold to be used for the pruning
%of the skeleton
S1=double(zeros(Nx,Ny,Nz));
S2=double(zeros(Nx,Ny,Nz));
%In arteriole/venules region use a looser threshold (1.5 times the diameter
%of the smallest arteriole/venule of order 1 found in the pig heart
%according to Kassab's publications)and a stricker for capillaries (1.5 times the diameter
%of the smallest capillary of order 1 found in the pig heart according to
%Kassab's publications)
%Express in microns, 3D local pruning guidance map
S1(Vfiltered==0)=4.1*1.5;
S1=S1.*double(BW);
S2(Vfiltered>0)=8.26*1.5; 
S=S1+S2;
%First pruning
[~,~,~,skel_art2]=cleanSkeleton(skel_final,S,voxelSize,Nx,Ny,Nz,SDF);
%Second pruning
S1(Vfiltered==0)=4.1*1.5;
S1=S1.*double(BW);
S2(Vfiltered>0)=8.26*1.5; 
S=S1+S2;
[A2,node,link,skel_final2]=cleanSkeleton(skel_art2,S,voxelSize,Nx,Ny,Nz,SDF);

% convert skeleton to graph structure
[~,node,link] = Skel2Graph3D(skel_final2,0);

%--------------------------------------------------------------------------
%                   Part 2: Further Improve skeleton
%--------------------------------------------------------------------------
%1. Erase skeleton bubbles
[skel_NoBubbles,node_NoBubbles,link_NoBubbles,skel_vis]=erase_skeleton_bubbles(node,link,Nx,Ny,Nz,outputFolder,name);
save([outputFolder,'/NoBubblesSkel2_',name,'.mat'],'skel_NoBubbles','node_NoBubbles','link_NoBubbles')

%2. Add end-points in node/link structures
[node_NoBubbles,link_NoBubbles]=add_end_points(node_NoBubbles,link_NoBubbles,Nx,Ny,Nz);

%3. Delete the internal nodes
[node_NoInt,link_NoInt]=updating_Adj_no_internal(node_NoBubbles,link_NoBubbles);
%Final Skeleton
node=node_NoInt;
link=link_NoInt;
skel_final = Graph2Skel3D(node,link,Nx,Ny,Nz);

%Uncomment for visualization
%{
node=node_NoInt;
link=link_NoInt;
%Reconstruct skeleton
skel_final=zeros(Nx,Ny,Nz);
for i=1:length(link)
    skel_final(link(i).point)=1;
end
for i=1:length(node)
    skel_final(node(i).idx)=2;
end
%}
%--------------------------------------------------------------------------
%                  Part 3: Save Results
%--------------------------------------------------------------------------

save([outputFolder,'/net8_',name,'.mat'],'A2','node','link','skel_final','BW')

