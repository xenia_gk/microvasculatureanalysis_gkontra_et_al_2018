function [Vol,L_mean,D_mean,S_mean,Vv,Lv,Sv,R]=metrics_vessel_constant_tube(hist_L,hist_R,Vol_sample)
%Function to calculate the Volume, length desnity etc based on the aproximation 
%of every vessel as a tube of constant radius
%
%Input:
%                   -hist_L: array with the length of every segment of the
%                   microvasculature
%                   -hist_R: array with the radius of every segment of 
%                   the microvasculature 
%                   -Vol_sample: volume of tissue, ie the volume of the
%                   image being processed
%
%Output:
%                   -Vol: Volume of the microvasculature
%                   -L_mean: mean lenght of the segments
%                   -D_mean: mean diameter of the segments
%                   -S_mean: mean surface 
%                   -Vv: volume density
%                   -Lv: length density
%                   -Sv: surface density
%                   -R: extravascular distance
%
%Author: Xenia Gkontra,  based on Stamatelos et. al., 2014
%

%Diameter
hist_d=2*hist_R;

%Surface area
for i = 1:length(hist_d)
    surf_L(i) = hist_L(i)*pi*hist_d(i);
end

%---------------        Volume      ---------------------------------------
Vol_full = pi*hist_d.^2./4.*hist_L;

%--------------       Volume Density            ---------------------------
Vv = sum(Vol_full)/(Vol_sample); 

%--------------       Length Density            ---------------------------
Lv = sum(hist_L)/Vol_sample;

%--------------       Surface Density          ----------------------------
Sv = sum(surf_L)/Vol_sample;

%--------------      Mean Extravascular Distance      ---------------------
R = sqrt(1/(pi*Lv)); 

%--------------      Mean Length     --------------------------------------
L_mean = mean(hist_L);

%--------------      Mean Diameter   --------------------------------------
D_mean = mean(hist_d);

%---------------        Surface Area   ------------------------------------
S_mean=mean(surf_L);

%----------------      Mean Volume          -------------------------------
Vol=mean(Vol_full);