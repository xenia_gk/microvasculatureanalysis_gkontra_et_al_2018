function [num1,num2,num3,num4,num5,num6,num7,num8]=metrics_Adjacency(A,link,node,BW)

%--------------------------------------------------------------------------
%                   Metrics based on adjacency matrix
%--------------------------------------------------------------------------
[Nx,Ny,Nz]=size(BW);
if isempty(A)
    numbreOfNodes=length(unique([link(:).n1 link(:).n2]));
    A=zeros(numbreOfNodes,numbreOfNodes);
    %Initialize adjacency matrix
    for i=1:length(link)
        A(link(i).n1,link(i).n2)=1;
    end
end

%Find degrees - Column 9
[deg,indeg,outdeg]=degrees(A>0);

%Number of branching points
num1=length(find(arrayfun(@(x)length(x.links),node)>1));
%Number of links
num2=length(link);
%Number of independent vascular networks
label=bwlabeln(BW);
num3=length(unique(label))-1;

%Number of end-points
%num4=length(find(arrayfun(@(x)length(x.links),node)==1));
%Nodes that are not on border along x-axis
x_NotBorder=~cellfun('isempty',(arrayfun(@(x)find(round(x.comx)>9&round(x.comx)<(Nx-8)),node,'UniformOutput',0)));
%Nodes that are not on border along y-axis
y_NotBorder=~cellfun('isempty',(arrayfun(@(x)find(round(x.comy)>9&round(x.comy)<(Ny-8)),node,'UniformOutput',0)));
%Nodes that are not on border along z-axis
z_NotBorder=~cellfun('isempty',(arrayfun(@(x)find(round(x.comz)>3&round(x.comz)<(Nz-2)),node,'UniformOutput',0)));
%Total number of end nodes
endNodes=~cellfun('isempty',(arrayfun(@(x)find(length(x.links)==1),node,'UniformOutput',0)));
%End-nodes that are not in Borders
%notBorder=find(x_NotBorder==1&y_NotBorder==1&z_NotBorder==1&endNodes==1);
num4=length(find(x_NotBorder==1&y_NotBorder==1&z_NotBorder==1&endNodes==1));

%Uncomment if you want to calculate end-nodes taht are on borders
%Border=(find((x_NotBorder~=1|y_NotBorder~=1|z_NotBorder~=1)&endNodes==1))
%Degrees
num5=mean(deg(:));
%Bifurcations
nodes_2=~cellfun('isempty',(arrayfun(@(x)find(length(x.links)==3),node,'UniformOutput',0)));
num6=length(find(nodes_2==1));
%Trifurcations
nodes_3=~cellfun('isempty',(arrayfun(@(x)find(length(x.links)==4),node,'UniformOutput',0)));
num7=length(find(nodes_3==1));
%Higher order nodes
nodes_4=~cellfun('isempty',(arrayfun(@(x)find(length(x.links)>4),node,'UniformOutput',0)));
num8=length(find(nodes_4==1));

