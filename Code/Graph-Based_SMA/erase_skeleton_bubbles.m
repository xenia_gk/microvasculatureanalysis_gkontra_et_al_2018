function [skel_NoBubbles,node_NoBubbles,link_NoBubbles,skel_vis]=erase_skeleton_bubbles(node,link,Nx,Ny,Nz,outputFolder,name)
%Function for repeatevily call replace_bubbles_skeleton
%to erase bubbles and nodes and replacing them with
%one-voxel nodes (sometimes it might be more than one-point but never
%bubbles) until no more skeleton bubbles are present or the procedure has been repeated
%more than 5 times
%
%Input:
%               node: structure containing all branching nodes
%               link: structure containing edges of the skeleton
%               Nx,Ny,Nz: dimensions of skeleton volume
%Output:
%               skel_NoBubbles: resulting skeleton with no Bubbles
%               node_NoBubbles: stucture of nodes after erasing bubbles
%               link_NoBubbles: stucture of edges after erasing bubbles
%               skel_vis: resulting skeleton for visualization purposes (2
%               is a node, 1 is an edge)
%
%Author: Xenia Gkontra, JHU, 2015


%--------------------------------------------------------------------------
%                   Erase skeleton bubbles
%--------------------------------------------------------------------------
count=0;
b=find(~cellfun('isempty',arrayfun(@(x)find(length(x.idx)>1),node,'UniformOutput',false)));
while(~isempty(b)&&count<10)
    [skel3,bubbles2]=replace_bubbles_skeleton(node,link,Nx,Ny,Nz,outputFolder,name);
    skel_NoBubbles=skel3+bubbles2;
    skel_NoBubbles=skel_NoBubbles>0;
    % convert graph structure back to (cleaned) skeleton
    [A,node_NoBubbles,link_NoBubbles] = Skel2Graph3D(skel_NoBubbles,0);
    %Recreate skeleton for node and links
    skel_vis=zeros(Nx,Ny,Nz);
    for i=1:length(link_NoBubbles)
        skel_vis(link_NoBubbles(i).point)=1;
    end
    for i=1:length(node_NoBubbles)
        skel_vis(node_NoBubbles(i).idx)=2;
    end
    node=node_NoBubbles;
    link=link_NoBubbles;
    %Cound number of nodes that consist of more than one voxel
    b=find(~cellfun('isempty',arrayfun(@(x)find(length(x.idx)>1),node,'UniformOutput',false)));
    %Count how many times the procedure is repeated
    count=count+1;
end