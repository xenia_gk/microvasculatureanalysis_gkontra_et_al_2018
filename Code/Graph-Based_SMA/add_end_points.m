function [node_NoBubbles,link_NoBubbles]=add_end_points(node_NoBubbles,link_NoBubbles,Nx,Ny,Nz)
%Function to find and add the end-points at the structure node
%Input:
%                   node_NoBubbles: initial structrue with nodes that
%                   conatins only the branching nodes
%                   link_NoBubbles: structure conating all link information
%                   Nx,Ny,Nz: dimensions of skeleton volume
%
%Output:
%                   node_NoBubbles: all end-nodes are adding at the end of
%                   the intial node_NoBubbles structure
%                   link_NoBubbles: structure conating all edge
%                   information, reference to end-nodes index instead of
%                   -1 that appeared in the initial link_NoBubbles
%                   structrue is added
%
%Author: Xenia Gkontra, JHU, 2015

%End-points
indxEnd=find([link_NoBubbles.n2]==0);
%Number of nodes (branching or artifacts internal) found
br=length(node_NoBubbles);
for i=1:length(indxEnd)
    link_NoBubbles(indxEnd(i)).n2=br+i;
    node_NoBubbles(br+i).idx= link_NoBubbles(indxEnd(i)).point(end);
    node_NoBubbles(br+i).links=indxEnd(i);
    node_NoBubbles(br+i).conn=-1;
    [node_NoBubbles(br+i).comy,node_NoBubbles(br+i).comx,node_NoBubbles(br+i).comz]=ind2sub([Nx,Ny,Nz],node_NoBubbles(br+i).idx);
end
%End-points
indxEnd=find([link_NoBubbles.n1]==0);
%Number of nodes (branching or artifacts internal) found
br=length(node_NoBubbles);
for i=1:length(indxEnd)
    link_NoBubbles(indxEnd(i)).n2=br+i;
    node_NoBubbles(br+i).idx= link_NoBubbles(indxEnd(i)).point(end);
    node_NoBubbles(br+i).links=indxEnd(i);
    node_NoBubbles(br+i).conn=-1;
    [node_NoBubbles(br+i).comy,node_NoBubbles(br+i).comx,node_NoBubbles(br+i).comz]=ind2sub([Nx,Ny,Nz],node_NoBubbles(br+i).idx);
end