%--------------------------------------------------------------------------
%Run image analysis pipeline (Steps 1-3) presented in Gkontra et al., 2017
%
%Additionally, calculate statistics and create comparative plots of the 
%different conditions per metric (Step 4), perform classification (Step 5)
%and additional analysis of efficiency in oxygen supply (krogh equivalent
%cylinders, extravascular distances histograms, define best fitting 
%super-ellipsoids to describe capillary supply regions)(Step 6)
%as in Gkontra et al., 2018
%
%Author: Xenia Gkontra, CNIC, xenia.gkontra@gmail.com
%--------------------------------------------------------------------------

clear;
close all;

%Add folders with programs used to run the code
addpath(genpath('../Programs/GetFullPath_20161216/'))
addpath(genpath(GetFullPath('../Programs/')))

%Add functions called by various sub-steps of the analysis pipeline, ie
%define voxelSize, define region to which the tissue used for the every
%image being analyzed belongs to (remote,infarct,basal)

addpath(genpath(GetFullPath('../Library_common_functions/')))

%--------------------------------------------------------------------------
%               STEP 1: Apply filling pipeline to 
%            recontruct the complete microvasculature
%--------------------------------------------------------------------------

%Step 1.a: Denoising
run('../DenoiseSegm/Main_Preprocessing.m');

%Step 1.b: Segmentation of VE-Cadherin Channel & Refinement by Nuclei
run('../MMT/Main_Segmentation.m');

%Step 1.c: Segmentation of SMA channel & identification of possible arterioles/venules areas 
run('../SMA/Main_SMA_Arterioles.m')

%Step 1.d: Filling of the gaps
run('../Filling/Main_Filling_Pipeline.m')

%--------------------------------------------------------------------------
%            STEP 2: Calculate metrics at structure level
%           Fractal-Based Metrics & Minkowski-based Metrics
%--------------------------------------------------------------------------

%Step 2.a: Calculate Fractal Dimension & lacunarity
run('../Fractal-Based/Fd_Lacunarity/Main_Fd_Lacu.m')

%Step 2.b: Calculate Succolarity
run('../Fractal-Based/Succolarity/Main_succo.m')

%Step 2.c: Calculate additional metrics characterising the microvasculature
%at structure level & gather all metrics in matrices according to tissue 
%category
run('../Structure_level_Metrics/Main_metrics_1.m')

%--------------------------------------------------------------------------
%               STEP 3: Calculate metrics at segment level, 
%          metrics related to SMA+ cells and traditional 2D metrics 
%           indicating oxygen diffusion efficiency
%--------------------------------------------------------------------------

%Step 3.a: Calculate metrics at segment level, sma+ related metrics and
%metrics regarding nuclei
run('../Graph-Based_SMA/Main_Graph_SMA_Analysis.m')

%Step 3.b: analysis of metrics traditionally related to oxygen-diffusion 
%distances, i.e. capillary density, and inter-capillary distance calculated
%from 2D slices of the 3D volume
run('../Efficiency/Metrics2D/Main_2D_Efficiency.m')

%Step 3.c: Gather and calculate additional metrics at the segment level
%extracted from the graph-based representation of the microvasculature as
%well as metrics related to SMA+ cells, nuclei and efficiency in oxygen 
%diffusion (3D extravascular distances, 2D metrics, diffusion distance)
run('../Graph_SMA_Oxygen_Metrics/Main_metrics_2.m')


%--------------------------------------------------------------------------
%               STEP 4: Calculate statistics & Create Plots
%--------------------------------------------------------------------------

%Step 4.a: Calculate statistics and create comparative plots per metric at
%structure level
run('../Statistics_and_Plots/Main_statistics_plot(1)')

%%Step 4.b: Calculate statistics and create comparative plots per metric at
%segment level, sma+ related metrics, efficiency in oxygen delivery
run('../Statistics_and_Plots/Main_statistics_plot(2)')

%--------------------------------------------------------------------------
%           STEP 5: Join results & classification
%--------------------------------------------------------------------------

%Step 5.a: Gather in common matrices all metrics
run('../Classification/Main_join_results.m')

%Step 5.b: Run classification
run('../Classification/Main_classification.m')

%--------------------------------------------------------------------------
%           STEP 6: Additional regarding capillary supply regions
%--------------------------------------------------------------------------

%Step 6.a: Create Krogh equivalent cylinders & corresponding relative
%frequency distributions of extravascular distances
run('../Efficiency/Krogh_cylinder/Main_krogh_equivalent_per_condition.m');

%Step 6.b: Create 3D bar plot of frequency histograms for all subjects (and 
%frequency histograms per volume)
run('../Efficiency/3DExtravascularDiffusionDistances/Main_ExtravascularDistanceHistograms(1,50,[],[])');

%Step 6.c: Super-ellipsoids
run('../Efficiency/Superellipsoids/Main_fit_3D_shapes_per_tissue.m')