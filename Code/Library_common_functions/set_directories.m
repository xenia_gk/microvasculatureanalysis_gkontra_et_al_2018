function [directories]=set_directories(directory)
%Function to specify the paths where the original data and the results from
%the different modules are saved. The function is used by all modules to
%load results from other modules or the original data
%
%Output:
%
%               - directory: relative level of the path of the module that  
%               called set_directories with respect to the "Code" folder that 
%               contains all modules' code 
%               - directories: struct that contains the following fields:
%                   * mainDir_Original: path to original data
%                   * mainDir: path to denoised data and segmentations of the
%                   * channels
%                   * mainDir_Filled: path to microvasculature after filling
%                   gaps
%                   * mainDir_Art: path to possible arterioles/venules region
%                   * mainDir_Fractals: path to fractal-based metrics
%                   * mainDir_Structure_Results: path to gathered results for
%                   all "structure" level metrics
%                   * mainDir_Segment: path to graph-based representation of
%                   the microvasculature and related metrics, as well as sma+
%                   cells information
%                   * mainDir_2D: path to 2D metrics
%                   * mainDir_Segment_Results: path to gathered results for all
%                   "segment" level metrics, sma+ cells related metrics and
%                   diffusion efficiency
%
%Author: Xenia Gkontra


%--------------------------------------------------------------------------
%                       Original data
%--------------------------------------------------------------------------
%Path to the original image data 
directories.mainDir_Original='../../Dataset/';

%--------------------------------------------------------------------------
%                    Code modules 
%--------------------------------------------------------------------------

%Path to the denoised images and segmentations of
%microvasculature's, SMA and nuclei channel will be 
mainDir=[directory,'/DenoiseSegm/Results/'];
if ~exist(mainDir)
    mkdir(mainDir);
end
directories.mainDir=mainDir;

%Path to the microvasculature reconstruction after
%applying the filling pipeline
mainDir_Filled=[directory,'/Filling/Results/'];
if ~exist(mainDir_Filled)
    mkdir(mainDir_Filled);
end
directories.mainDir_Filled=mainDir_Filled;

%Path to possible arterioles/venules regions 
mainDir_Art=[directory,'/SMA/Arterioles/'];
if ~exist(mainDir_Art)
    mkdir(mainDir_Art);
end
directories.mainDir_Art=mainDir_Art;

%Path to fractal-based metrics 
mainDir_Fractals=[directory,'/Fractal-Based/Results_Fractals/'];
if ~exist(mainDir_Fractals)
    mkdir(mainDir_Fractals);
end
directories.mainDir_Fractals=mainDir_Fractals;

%Path to .mat file that contains matrices per tissue category with the
%results for every sample and every structure-level metric (fractals,
%minkowski)
mainDir_Structure_Results=[directory,'/Structure_level_Metrics/Results/'];
if ~exist(mainDir_Structure_Results)
    mkdir(mainDir_Structure_Results);
end
directories.mainDir_Structure_Results=mainDir_Structure_Results;

%Path to skeletonized microvasculature, related metrics and sma+
%information to be enriched with additional metrics by module
%"Graph_SMA_Oxygen_Metrics" 
mainDir_Segment=[directory,'/Graph-Based_SMA/Results/'];
if ~exist(mainDir_Segment)
    mkdir(mainDir_Segment);
end
directories.mainDir_Segment=mainDir_Segment;

%Path to 2D-based metrics (capillary density and intercapillary distance)
mainDir_2D=[directory,'/Efficiency/Metrics2D/Results/'];
if ~exist(mainDir_2D)
    mkdir(mainDir_2D);
end
directories.mainDir_2D=mainDir_2D;

%Path to .mat file that contains for every sample (grouped by tissue
%category) at the segment level extracted from the graph-based
%representation of the microvasculature as well as metrics related to SMA+ 
%cells, nuclei and efficiency in oxygen diffusion 
mainDir_Segment_Results=[directory,'/Graph_SMA_Oxygen_Metrics/Results/'];
if ~exist(mainDir_Segment)
    mkdir(mainDir_Segment);
end
directories.mainDir_Segment_Results=mainDir_Segment_Results;

%Folder name to save results inside each module
directories.folderOut='Results/';
