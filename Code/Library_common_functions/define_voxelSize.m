function [voxelSize,Voxel_volume]=define_voxelSize(filename)
%Return voxel size & voxel volume
%Here the voxel size is outputed dirrectly from the microscope but it can
%be calculated as voxelSize=physicalSize/numberofvoxels (microns/voxel)
%Input:
%           -filename: name of image volume to be processed
%Output:
%           -voxelSize: voxel size (width, height, depth) in microns
%           -Voxel_volume: volume of voxel in nm^3
%
%Author: Xenia Gkontra, CNIC

if isempty(find(ismember({'20','21','22','23','24','25','26','99'}, filename), 1))
    voxelSize=[0.379*4,0.379*4,1.007];
elseif ~isempty(find(ismember({'20','21','22','23','24','25','26'},filename), 1))
    voxelSize=[0.314*4,0.314*4,1.007];
elseif ~isempty(find(ismember({'99'}, filename), 1))
    voxelSize=[0.379*4,0.379*4,0.9651];
    
end
Voxel_volume=voxelSize(1)*voxelSize(2)*voxelSize(3)*(10^9);%nm^3