function [tissueCat]=define_tissueCat(filename)
%Function to define whether an image image belongs to infarcted or remote
%tissue category
%Input:
%           - filename: name of image volume under investigation
%Output:
%           - tissueCat: tissue catergory, can be 'infarct' or 'remote'
%
%Author: Xenia Gkontra, CNIC, 2016

%Filenames for infarcted volumes
Infarct={'57','68','4','5','34','35','36','55','56','10','11','12','25','26','27','49','50','51','16','17','18','19','20','21','43','44','45','89','90','91','92','93','94','95','96','97',...
    '111','112','113','115','116','117','118','119','120','128','131','132','133','134','135','136','137','138'};
%Filanames for volumes from remote zone
Remote={'1','2','3','37','38','39','52','53','54','7','8','9','31','32','33','46','47','48','13','14','15','22','23','24','28','29','30','79','80','81','82','83','84','85','86','87',...
    '99','121','122','123','124','125','127','129','130','139','101','102','103','104','105','107','140','141'
    };

if ~isempty(find(ismember(Infarct,filename), 1))
    tissueCat='Infarct';
elseif ~isempty(find(ismember(Remote,filename), 1))
    tissueCat='Remote';
else
    tissueCat='';
end