function [name1,name2,name3]=set_filenames
%Function to define the part of the image filenames that is caracteristic
%for each channel. Three channels are assumed
%
%Output:
%               - name1: Part of the filename that defines that the image 
%                 corresponds to Channel 1 (here nuclei)
%               - name2: Part of the filename that defines that the image 
%                 corresponds to Channel 2 (here VE-Cadherin)
%               - name3: Part of the filename that defines that the image 
%                 corresponds to Channel 3 (here SMA)
%
%Author: Xenia Gkontra

name1='*_ch00.tif';
name2='*_ch02.tif';
name3='*_ch03.tif';