function [Jaccard,Dice,rfp,rfn, precision,recall,f1]=sevaluate(m,o)
% gets label matrix for one tissue in segmented and ground truth 
% and returns the similarity indices
% m is a tissue in gold truth
% o is the same tissue in segmented image
% rfp false pasitive ratio
% rfn false negative ratio
%Modified, Xenia Gkontra to add precision, recall adn F1 measure
m=m(:);
o=o(:);
common=sum(m & o); 
union=sum(m | o); 
cm=sum(m); % the number of voxels in m
co=sum(o); % the number of voxels in o
Jaccard=common/union;
Dice=(2*common)/(cm+co);

%False positive
fp=co-common;
%False negative
fn=cm-common;
%True postive
tp=common;

rfp=fp/cm;
rfn=fn/cm;

%Precision
precision=tp/(tp+fp);
%Recall
recall=tp/(tp+fn);
%F1 measure
f1=2*((precision*recall)/(precision+recall));