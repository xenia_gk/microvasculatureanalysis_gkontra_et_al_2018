function [po]=createCircle(po,centerX,centerY,centerZ,radius)
%function to create a circle 

imageSizeX=size(po,1);
imageSizeY=size(po,2);
[x,y] = meshgrid(1:imageSizeX, 1:imageSizeY);
circlePixels = (x - centerY).^2 + (y - centerX).^2 <= radius.^2;
imshow(circlePixels) ;
size(po)
size(circlePixels)
for i=0:0
po(:,:,centerZ+i)=permute(circlePixels,[2 1]);
end