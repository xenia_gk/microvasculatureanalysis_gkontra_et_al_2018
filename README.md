-------------------------------------------------------------

Code for the analysis (and recognition) of microvascular networks, relations with sma+ cells as well as analysis regarding the diffusion efficiency and adequate geometric models of the capillary supply region 

Version 1.0.0

Author: Polyxeni (Xenia) Gkontra, CNIC, xenia.gkontra@gmail.com

-------------------------------------------------------------


General information
-------------------------------------------------------------
The code included in this file is the code used to perform all
analysis presented in Gkontra et al., 2018. 

The code is organized in different modules, but also a script for running all pipeline at once (Code/Run_all_pipeline/run_pipeline.m) is provided. 

For modifications and re-sharing of this code please see the license.txt file. 

If you use this code or part of it in your work, please cite our original publication:

Gkontra et al., “Deciphering microvascular changes after myocardial infarction through 3D fully automated image analysis”, Scientific Reports, 8(1):1854, 2018

An executable version of this code is under development and soon to be released


Prerequisites
--------------------------------------------------------------
In order to run this code or part of it you will need

1.  Matlab (Mathworks) installed with the following toolboxes
	- Image Processing Toolbox™,
	- Parallel Computing Toolbox™,
	- Optimization Toolbox™,
	- Statistics and Machine Learning Toolbox™,  
	- Bioinformatics Toolbox™, 
	- Global Optimization Toolbox™ (is additionally required for running the super-ellipsoids part)
	
2. Open source programs/codes from earlier works. They are all included along with their licenses in Code/Programs/. When any of them has been modified, it is mentioned.


Installation 
--------------------------------------------------------------
Unzip the provided MicrovasculatureAnalysis_Gkontra_et_al_2018.zip file. 

If you are not running the run_pipeline.m, but you prefer to run modules separately you will first have to add to the Matlab path the Programs folder & Library_common_functions. You can do this either by Matlab's interface or when entering the folder of the module you would like to use by typing the following in the command window:

addpath(genpath('../Programs/GetFullPath_20161216/'))

addpath(genpath(GetFullPath('../Programs/')))

addpath(genpath(GetFullPath('../Library_common_functions/')))


Folder structure & Contents
------------------------------------------------------------
The structure of the folders should be kept as it is or you should change the directories in the code (see Library_common_functions). As it is, Dataset and Code folder should be in the same path.

-Dataset: contains the original images organized by time-point under investigation 

-Code: contains in separate folder the code for every module/step. In brief,

    - DenoiseSegm: Denoising of 3D image volumes using the non-local means filter (Buades et al., CVPR, 2005).
 	
    - MMT: Segmentation of VE-Cadherin and nuclei channels using the multi-scale multilevel thresholding algorithm (Gkontra et al., MICCAI, 2015). The VE-Cadherin segmentation is refined using information from the nuclei channel.
 	
    - SMA: Segmentation of SMA+ channel and extraction of possible arterioles/venules regions.
 	
    - Filling:  Application of the filling algorithm on top of the VE-Cadherin segmentation to reconstruct regions of the microvasculature not stained by the VE-Cadherin.
 	
    - Fractal-Based: Includes subfolders Fd_Lacunarity and Succolarity. The former contains the code for calculating fractal dimension and lacunarity, while the latter the corresponding code for calculating succolarity. Please note that depending on the image size this might be a very time-consuming process.
 	
    - Structure_level_Metrics: Calculate and gather parameters at structure level (Fractal-Based Metrics & Minkowski-based Metrics) using calculations of the previous step (see Fractal-Based folder)
 	
    - Graph-Based_SMA: Calculation of segment level parameters, SMA+ cells and nuclei related properties
    
    - Efficiency: Contains subfolders (i) Metrics2D with the code for calculating capillary density and intercapillary distance, (ii) 3DExtravascularDiffusionDistances with the code for the calculation of extravascular diffusion distances histograms, (iii) Krogh_cylinder with the code for creating equivalent krogh cylinder models per tissue category and their respective diffusion histograms, (iv) Superellipsoids with the code for identifying super-ellipsoid shapes that can best fit the frequency histograms of extravascular distances observed in each tissue category 
 	
    - Graph_SMA_Oxygen_Metrics: Gather previously calculated metrics (see Graph-Based_SMA & Metrics2D  folders) at the segment level extracted from the graph-based representation of the microvasculature as well as metrics related to SMA+ cells, nuclei and efficiency in oxygen diffusion (3D extravascular distances, 2D metrics, diffusion distance) and calculate additional metrics
 	
    - Statistics_and_Plots: Calculate statistics and create comparative plots per metric 
 	
    - Classification: Joining all metrics in common matrices and performing classification using 9-fold cross validation and different classifiers
 	
    - Library_common_functions: Functions for defining for every image of the dataset the tissue condition (“Infarct” or “Remote” for time-points 1 “1MI”, “3MI”,”7MI” and “” for ”Basal”), voxel size and the subject to which the tissue used to acquire the image was obtained. Additional functions, for setting the paths with the results of the modules and defining the filename for the different channels 
 	
    - Programs: Folder containing additional open-source programs/codes along with their licenses necessary to run the whole pipeline
	
    - Run_all_pipeline: Run all pipeline at once


Instructions for running
--------------------------------------------------------------

You can run all pipeline (including statistics calculation, classification etc) by calling [Code/Run_all_pipeline/run_pipeline.m](https://bitbucket.org/xenia_gk/microvasculatureanalysis_gkontra_et_al_2018/src/9da352bd2032050c746827a04722b8968fc79af7/Code/Run_all_pipeline/?at=master)

Otherwise, you can run each module/step separately by calling the main script for each module (Main_*.m of corresponding subfolder).

Please see [Doucmentation.pdf](https://bitbucket.org/xenia_gk/microvasculatureanalysis_gkontra_et_al_2018/src/a36ee17841359893c7244e251a1ad0c1d772bde0/Documentation_Final.pdf?at=master&fileviewer=file-view-default) for a detailed description of the various modules/steps.

Comments & Bug report
------------------------------------------------------------
Any comments, corrections or suggestions are highly welcome.
Please contact xenia.gkontra@gmail.com.


